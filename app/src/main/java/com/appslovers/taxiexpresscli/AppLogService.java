package com.appslovers.taxiexpresscli;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Inflater;

/**
 * Created by javierquiroz on 22/08/16.
 */
public class AppLogService extends Service {

    private WindowManager windowManager;
    private ImageView chatHead;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    View MiLayout;
    WebView wv;
    WindowManager.LayoutParams params_opener, params_layout;
    NotificationManager NM;

    @Override
    public void onCreate() {
        super.onCreate();

        NM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        startForeground(1, new Notification());
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        MiLayout = LayoutInflater.from(getApplicationContext()).inflate(R.layout.console, null);
        MiLayout.findViewById(R.id.btnCerrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MiLayout != null) {
                    isTap = true;
                    windowManager.removeView(MiLayout);
                }
            }
        });
        MiLayout.findViewById(R.id.btnCrash).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(Environment.getExternalStorageDirectory(), "crash.html");
                if (file.exists()) {
                    String s = file.getAbsolutePath();
                    Log.i("fullpath", ">" + s);
                    wv.loadUrl("file://" + s);
                }
            }
        });


        wv = (WebView) MiLayout.findViewById(R.id.miWebView);
        wv.setBackgroundColor(Color.TRANSPARENT);
        wv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        //wv.loadUrl("file:///android_res/raw/assurance.html");
        wv.getSettings().setSupportZoom(true);//habilita el zoom
        wv.getSettings().setBuiltInZoomControls(true);//necesario para habilitar zoom
        wv.getSettings().setLoadWithOverviewMode(true);//centra el contenido
        wv.getSettings().setDisplayZoomControls(false);//hide buttons zoom
        wv.getSettings().setUseWideViewPort(true);//double tap zoom


        //float scale = 90 * WV.getScale();
        //WV.setInitialScale((int)scale );

        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });

        chatHead = new ImageView(this);
        chatHead.setImageResource(R.drawable.launcher);

        params_layout = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,//|WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                PixelFormat.TRANSLUCENT);


        params_opener = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params_opener.gravity = Gravity.TOP | Gravity.LEFT;
        params_opener.x = 0;
        params_opener.y = 100;

        chatHead.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params_opener.x;
                        initialY = params_opener.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        return true;
                    case MotionEvent.ACTION_UP:

                        if (isTap) {


                            Log.d("TAP", ">>>>>>>>TRUE");
                            windowManager.addView(MiLayout, params_layout);
                            isTap = false;


                            String s = extractLogToFileAndWeb(false, getApplicationContext()).getAbsolutePath();
                            Log.i("fullpath", ">" + s);
                            wv.loadUrl("file://" + s);
                        } else {
                            Log.d("TAP", ">>>>>>FALSE");
                        }


                        return true;
                    case MotionEvent.ACTION_MOVE:

                        params_opener.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params_opener.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHead, params_opener);

                        return true;

                }
                return false;
            }
        });


        windowManager.addView(chatHead, params_opener);
    }

    boolean isTap = true;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatHead != null) windowManager.removeView(chatHead);
    }

    final static String Etag = "<font color=\"#F36A67\">";
    final static String Dtag = "<font color=\"#3486AE\">";
    final static String Wtag = "<font color=\"#B5BB48\">";
    final static String Itag = "<font color=\"white\">";
    final static String closetag = "</font>";

    final String cerrar = "</font>";

    public static File extractLogToFileAndWeb(boolean b, Context context) {
        //set a file
        Date datum = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        String fullName = df.format(datum) + "appLog.html";
        if (b)
            fullName = "crash.html";

        File file = new File(Environment.getExternalStorageDirectory(), fullName);

        //clears a file
        if (file.exists()) {
            file.delete();
        }


        //write log to file
        int pid = android.os.Process.myPid();
        try {
            String command = String.format("logcat -d -v threadtime *:*");
            //String command = String.format("logcat -v brief threadtime *:*");
            Process process = Runtime.getRuntime().exec(command);

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder result = new StringBuilder();
            String currentLine = null;

            result.append("<html>");
            result.append("<head>");
            result.append("<head>");
            result.append("</head>");
            result.append("<body text=\"white\">");
            result.append("<pre>");

            if (b) {
                result.append("CRASH!!!!!\n");
                result.append("CRASH!!!!!\n");
                result.append("CRASH!!!!!\n");
                result.append("CRASH!!!!!\n");
                result.append("CRASH!!!!!\n");
                result.append("CRASH!!!!!\n");
            }

            while ((currentLine = reader.readLine()) != null) {
                if (currentLine != null && currentLine.contains(String.valueOf(pid))) {
                    String[] splits = currentLine.split(" ");
                    if (splits != null && splits.length >= 5) {
                        switch (splits[4]) {
                            case "D"://azul
                                result.append(Dtag + currentLine + closetag);
                                break;
                            case "E"://rojo
                                result.append(Etag + currentLine + closetag);
                                break;
                            case "I"://GRIS
                                result.append(Itag + currentLine + closetag);
                                break;
                            case "W"://amarillo
                                result.append(Wtag + currentLine + closetag);
                                break;
                        }

                    } else {
                        result.append(currentLine);
                    }

                    // Log.i("logcat",""+currentLine);
                    result.append("\n");
                }
            }
            result.append("</pre>");
            result.append("</body>");
            result.append("</html>");
            FileWriter out = new FileWriter(file);
            out.write(result.toString());
            out.close();

            //Runtime.getRuntime().exec("logcat -d -v time -f "+file.getAbsolutePath());
        } catch (IOException e) {
            //Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }


        //clear the log
        try {
            Runtime.getRuntime().exec("logcat -c");
        } catch (IOException e) {
            Toast.makeText(context, "logcat" + e.toString(), Toast.LENGTH_SHORT).show();
        }

        return file;
    }
}


