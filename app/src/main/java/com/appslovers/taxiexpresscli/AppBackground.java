package com.appslovers.taxiexpresscli;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.appslovers.taxiexpresscli.data.ISocket;
import com.appslovers.taxiexpresscli.data.InnerActions;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyGps;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Notificaciones;
import com.appslovers.taxiexpresscli.util.SocketIo;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.util.SocketIoBridge;
import com.github.nkzawa.socketio.client.Ack;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import pe.com.visanet.lib.VisaNetConfigurationContext;
import pe.com.visanet.lib.VisaNetPaymentActivity;
import pe.com.visanet.lib.VisaNetPaymentInfo;

/**
 * Created by javierquiroz on 20/05/16.
 */
public class AppBackground extends Service {

    AppBinder AppConexion = new AppBinder();
    NotificationManager NM;

    public static final int ZOOM_GOOGLEMAPS = 16;

    @Override
    public IBinder onBind(Intent intent) {
        AppConexion.setApp(this);

        Log.d("AppBackground", "onBind");
        return AppConexion;
    }

    @Override
    public void onRebind(Intent intent) {

        AppConexion.setApp(this);
        Log.d("AppBackground", "onReBind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {

        //AppConexion.setApp(null);
        AppConexion.setRoot(null);
        Log.d("AppBackground", "onUnBind");
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            if (intent.getAction() != null) {
                doIntentInside(intent);
            }
        }
        return START_NOT_STICKY;
    }


    public void doIntentInside(Intent i) {

        switch (InnerActions.valueOf(i.getAction())) {
            case notact_updateestado:
                JsonPack.RespEstadoCliente rec = (JsonPack.RespEstadoCliente) i.getSerializableExtra("rec");
                if (rec != null) {
                    Log.d("notact_updateestado", "REC OK");
                    ServicioActual = rec.Servicio;
                    ConductorActual = rec.Conductor;
                    UnidadActual = rec.Unidad;
                    //if(ServicioActual!=null && ServicioActual.getEstado()>0)
                    if (ServicioActual != null && ServicioActual.getEstado() > 0 && ServicioActual.getEstado() < 6) // este es provicional
                    {
                        Log.d("Servicio actual", ">" + ServicioActual.getEstado());
                        if (!isNodeJsConnected()) {
                            NodeJsTurnOn();
                        }
                    } else {
                        Log.e("Serbvicio actual", "NULL");
                    }

                } else {
                    Log.e("notact_updateestado", "REC null");
                }

                break;
            case notact_salirservicio:
                NM.cancelAll();
                stopForeground(true);
                stopSelf();
                android.os.Process.killProcess(android.os.Process.myPid());
                break;

            case notact_detenernode:
                NodeJsTurnOff();
                break;

            case notact_encendernode:
                NodeJsTurnOn();
                break;

            case notact_cancelar_encendernode:
                NodeJsCancelTurnOn();
                break;
            case notact_cancelar_detenernode:
                NodeJsCancelTurnOff();
                break;

            case notact_aceptarSolicitud:
                int ida = i.getIntExtra("notiid", -1);
                if (ida > -1)
                    NM.cancel(ida);

                JsonPack.NuevaSolicitud ns = (JsonPack.NuevaSolicitud) i.getSerializableExtra("solicitud");
                AceptarServicioConductor(ns);
                break;

            case notact_rechazarSolicitud:
                int idr = i.getIntExtra("notiid", -1);
                if (idr > -1)
                    NM.cancel(idr);
                break;

            case push_conductorllego:

                if (isoc != null) {
                    //encender el nodejs
                    isoc.ConductorLLego(i.getStringExtra("json"));
                    NodeJsTurnOff();
                    NodeJsTurnOn();
                }

                break;

            case push_incioservicio:
                if (isoc != null) {
                    //encender el nodejs
                    String json = i.getStringExtra("json");
                    isoc.InicioServicio(json);
                    NodeJsTurnOff();
                    NodeJsTurnOn();
                }
                break;

            case push_finalizarserviciocliente:
                if (isoc != null) {
                    //encender el nodejs
                    isoc.FinalizarServicioCliente((JsonPack.RespFinalizarServ) i.getSerializableExtra("json"));

                    NodeJsTurnOff();
                    NodeJsTurnOn();
                }
                break;
            case notif_solicitudpago:
                isoc.enviarSolicitudPago(i.getStringExtra("solicitud"));
                NM.cancelAll();
                break;
        }
    }

    public void detenerServicio() {
        NM.cancelAll();
        stopForeground(true);
        stopSelf();
    }

    public boolean isNodeJsConnected() {
        if (SI != null) {
            if (SI.SOCKET != null) {
                if (SI.SOCKET.connected()) {
                    return true;
                }
            }
        }
        return false;

    }

    //SPUser Session;
    JsonPack.ResponseLogin Session;

    public void NodeJsTurnOn() {

        Session = new SPUser(getApplicationContext()).getResponseLogin();

        //startForeground(1, Notificaciones.getNotificacionDetener(getApplicationContext()));
        startForeground(1, Notificaciones.getNotificacionEncendiendoDeteniendo(getApplicationContext(), true));
        SI.Conectar();
    }

    public void NodeJsCancelTurnOn() {
        Log.i("appbg", ">>>>>>>>>> cancel turn on");
        SI.Desconectar();
    }

    public void NodeJsCancelTurnOff() {
        Log.i("appbg", ">>>>>>>>>> cancel turn off");
    }

    public void NodeJsTurnOff() {
        //SI.SOCKET.disconnect();
        //SI.SOCKET.close();
        SI.Desconectar();
        startForeground(1, Notificaciones.getNotificacionEncendiendoDeteniendo(getApplicationContext(), false));
        //startForeground(1, Notificaciones.getNotificacionEncender(getApplicationContext()));

    }

    public void AceptarServicioConductor(JsonPack.NuevaSolicitud ns) {

        JsonPack.SolicitudRespuesta aceptaTaxi = new JsonPack.SolicitudRespuesta();
        aceptaTaxi.conductor_id = Session.cliente_id;
        aceptaTaxi.servicio_id = ns.ServicioId;
        Log.d("Envio nueva sol", ">" + ns.Envio);
        if (ns.Envio == 0) {
            aceptaTaxi.socketCli = null;
        } else {
            aceptaTaxi.socketCli = ns.socketCliente;
        }

        SI.emitData("AceptarSolicitudConductor", new Gson().toJson(aceptaTaxi));
    }

    public void EnviarCancelarServicio() {

        JsonPack.RespCancelarServicio cs = new JsonPack.RespCancelarServicio();
        cs.conductor_id = ConductorActual.conductor_id;
        cs.servicio_id = ServicioActual.IdServicio;

        SI.emitData("CancelarService", new Gson().toJson(cs));
    }

    public void EnviarLlegadaRecogo() {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.cliente_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("LLegadaConductor", new Gson().toJson(recojo), new Ack() {
            @Override
            public void call(Object... args) {
                Log.i("EnviarLlegadaRecogo", ">" + SocketIo.getOneString(args));
            }
        });
    }


    public void EnviarLlegadaRecogo(Ack a) {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.cliente_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("LLegadaConductor", new Gson().toJson(recojo), a);
    }

    public void EnviarIniciarServicio() {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.cliente_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("IniciarServicio", new Gson().toJson(recojo), new Ack() {
            @Override
            public void call(Object... args) {
                Log.i("EnviarLlegadaRecogo", ">" + SocketIo.getOneString(args));
            }
        });
    }

    public void EnviarAlerta() {
        MYGPS.turnOn(true);
    }

    public void solicitarServicio(JsonPack.Servicio serv, SocketIo.CallBack a) {
        /*
        JsonPack.LlegoRecojo recojo=new JsonPack.LlegoRecojo();
        recojo.IdConductor=Session.cliente_id;
        recojo.IdServicio=ServicioActual.IdServicio;*/

        JsonPack.Solicitud sol = serv.getSolicitud();
        int tipo = ((AppForeGround) getApplication()).flag_tipotarifa;
        sol.kilometraje = "" + (tipo == 2 ? 1 : 2);

        SI.emitDataCallback("getListConductorGeo", new Gson().toJson(sol), a);
    }


    public void SelecionarConductor(JsonPack.SolicitudResponse sr, JsonPack.ConductorLista conductorLista, SocketIo.CallBack a) {

        int quitardelista = -1;
        try {
            quitardelista = Integer.parseInt(conductorLista.IdConductor);
        } catch (NumberFormatException e) {
            Log.e("nfe", ">" + e.getMessage());
        }
        if (quitardelista > -1) {
            sr.idsCond.remove(new Integer(quitardelista));

        }

        JsonPack.SelectionarConductor sc = new JsonPack.SelectionarConductor();
        sc.idarray = sr.idsCond;
        sc.idservicio = sr.Servicio;
        sc.idconductor = conductorLista.IdConductor;
        sc.tipo = "1";

        SI.emitDataCallback("SeleccionConductor", new Gson().toJson(sc), a);

    }

    public void finalizarConTarjeta(VisaNetPaymentInfo paymentInfo,String idServicio){
        Log.e("FIN TARJETA", "finalizarConTarjeta: PAYMENTINFO" );
        JsonPack.PaymentInfo info=new JsonPack.PaymentInfo();
        info.info=paymentInfo;
        info.idServicio=idServicio;
        SI.emitData("finalizarConTarjeta",new Gson().toJson(info));
        Log.e("AppBackground", "finalizarConTarjeta: "+ new Gson().toJson(info));
    }

    public void EnviarIniciarServicio(SocketIo.CallBack a) {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.cliente_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("IniciarServicio", new Gson().toJson(recojo), a);
    }

    public void EnviarFinalizarServicio() {

        JsonPack.LlegoRecojo recojo = new JsonPack.LlegoRecojo();
        recojo.IdConductor = Session.cliente_id;
        recojo.IdServicio = ServicioActual.IdServicio;

        SI.emitDataCallback("FinalizarServicio", new Gson().toJson(recojo), new Ack() {
            @Override
            public void call(Object... args) {
                Log.i("EnviarLlegadaRecogo", ">" + SocketIo.getOneString(args));
            }
        });
    }


    public void CalificarCliente(float calificacion, String mensaje) {
        new MyRequest<JsonPack.ResponseLogin>(Urls.ws_calificacion, MyRequest.HttpRequestType.GET) {

            @Override
            public void onSucces(ResponseWork rw, boolean isActive) {
            }
        }
                .putParams("tipo", "1")
                .putParams("servicio_id", ServicioActual.IdServicio)
                .putParams("cliente_id", ServicioActual.IdCliente)
                .putParams("conductor_id", ServicioActual.IdConductor)
                .putParams("calificacion", "" + calificacion)
                .putParams("mensaje", mensaje)
                .send();
    }

    //MyGps MYGPS;
    SocketIo SI;
    ISocket isoc;

    SocketIoBridge SOIB;
    SocketIoBridge.AuxiliarBridge AXB;

    public void setAuxiliarBridge(SocketIoBridge.AuxiliarBridge axb) {
        AXB = axb;
    }

    public void setSocketIoBridge(SocketIoBridge s) {
        SOIB = s;
        if (SOIB != null) {

            Log.i("setSocketIoBridge", "SocketIoBridge OK");
            /*
            if(TEMP_NS!=null)
            {
                SOIB.nuevaSolicitudConductor(TEMP_NS);
                TEMP_NS=null;
            }*/
            if (Solicitudes != null && Solicitudes.size() > 0) {
                Log.i("setSocketIoBridge", "Hay Solicitudes");
                enviarNotificaciones();
            } else {
                Log.i("setSocketIoBridge", "NO hay solicitudes");
            }
        } else {
            Log.i("setSocketIoBridge", "Null SocketIoBridge");
        }
    }

    ArrayList<JsonPack.NuevaSolicitud> Solicitudes;

    public void enviarNotificaciones() {
        for (JsonPack.NuevaSolicitud ns : Solicitudes) {
            if (SOIB != null) {
                SOIB.nuevaSolicitudConductor(ns);
            }
        }
        Solicitudes.clear();
        NM.cancel(notidpp);
    }


    int notidpp = 10;

    public JsonPack.Servicio getServicioActual() {
        if (ServicioActual == null) {
            Log.e("getServicioActual", "es null");
        } else {
            Log.i("getServicioActual", "es OK");
        }
        return ServicioActual;
    }

    public JsonPack.Conductor getConductorActual() {
        if (ConductorActual == null) {
            Log.e("getConductorActual", "es null");
        } else {
            Log.i("getConductorActual", "es OK");
        }
        return ConductorActual;
    }

    public JsonPack.Vehiculo getUnidadActual() {
        if (UnidadActual == null) {
            Log.e("getUnidadActual", "es null");
        } else {
            Log.i("getUnidadActual", "es OK");
        }
        return UnidadActual;
    }

    public void playAudio(int i) {
        switch (i) {
            case 1:
                try {
                    audio1.prepare();
                } catch (IllegalStateException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                } catch (IOException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                }
                audio1.start();
                break;
            case 2:
                try {
                    audio2.prepare();
                } catch (IllegalStateException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                } catch (IOException e) {
                    Log.e(AppBackground.class.getSimpleName(), "audio1>" + e.getMessage());
                }
                audio2.start();
                break;
        }

    }

    PowerManager.WakeLock wl;

    public JsonPack.Conductor ConductorActual;
    public JsonPack.Servicio ServicioActual;
    public JsonPack.Vehiculo UnidadActual;
    MediaPlayer audio1, audio2;
    MyGps MYGPS;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(AppBackground.class.getSimpleName(), "onCreste Servicio");

        //startForeground(1, Notificaciones.getNotificacionEncender(getApplicationContext()));
        audio1 = MediaPlayer.create(getApplicationContext(), R.raw.audio1);
        audio2 = MediaPlayer.create(getApplicationContext(), R.raw.audio2);

        NM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        SOIB = new SocketIoBridge();
        isoc = new ISocket() {

            @Override
            public void SolicitudApis(JsonPack.ApiKeys s) {
                SOIB.SolicitudApis(s);

                Log.e("keys", "" + s.foursquare_key);
            }

            @Override
            public void SolicitudAceptar(JsonPack.ConductorLista solicitud) {
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.SolicitudAceptar(solicitud);
                else
                    Log.e(SocketIo.class.getSimpleName(), "connect null bridge ");
            }

            @Override
            public void ConductorLLego(String solicitud) {

                Log.d("ConductorLLego", ">" + solicitud);
                ServicioActual.Estado = "2";
                playAudio(1);
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.ConductorLLego(solicitud);
                else {
                    JsonPack.RespConductorLlego rcl = null;
                    try {
                        rcl = new Gson().fromJson(solicitud, JsonPack.RespConductorLlego.class);
                    } catch (JsonParseException e) {
                        Log.e("parseex", ">" + e.getMessage());
                    }
                    if (rcl != null && ServicioActual != null) {
                        ServicioActual.FechaRecojo = rcl.FechaRecojo;
                    }
                }
            }


            @Override
            public void InicioServicio(String solicitud) {

                Log.d("InicioServicio", ">" + solicitud);
                //{"FechaInicio":"2016-7-26 18:13:43"}

                //ServicioActual.Estado = "3";
                //if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                //SOIB.InicioServicio(solicitud);

                if (solicitud.equalsIgnoreCase("reserva")) {

                    //ServicioActual.Estado="3";
                    //NM.notify(notidpp, Notificaciones.getNotificacionReservaEnCamino(getApplicationContext()));
                    Intent i = new Intent(getApplicationContext(), RootSplash.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    Log.d("AppBG", "ServicioCancelado: " + solicitud);

                }

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.InicioServicio(solicitud);
                else {
                    NM.notify(notidpp, Notificaciones.getNotificacionReservaEnCamino(getApplicationContext()));
                    Intent i = new Intent(getApplicationContext(), RootSplash.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    Log.d("AppBG", "ServicioCancelado: " + solicitud);
                }
                ServicioActual.Estado = "3";

            }

            @Override
            public void FinalizarServicioCliente(JsonPack.RespFinalizarServ s) {

                Log.d("FinalizarServicioClie", ">" + s);
                ServicioActual.Estado = "6";
                ((AppForeGround) getApplication()).flag_califico = 1;
                playAudio(2);
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.FinalizarServicioCliente(s);
            }

            @Override
            public void enviarSolicitudPago(String s) {

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.enviarSolicitudPago(s);
                else {
                    NM.notify(notidpp, Notificaciones.getNotificacionSolicitudPago(getApplicationContext(), s));
                    Intent i = new Intent(getApplicationContext(), RootSplash.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }


            }


            @Override
            public void RefreshCliente(String s) {

                Log.d("RefreshCliente", ">" + s);

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.RefreshCliente(s);
                else {
                    LatLng newPos = null;
                    try {
                        newPos = new Gson().fromJson(s, JsonPack.RespRefreshConductor.class).getLL();
                    } catch (JsonParseException e) {
                        Log.e("jsonex", ">" + e.getMessage());
                    }
                    if (newPos != null) {
                        ConductorActual.conductor_lat = newPos.latitude;
                        ConductorActual.conductor_lng = newPos.longitude;
                    }
                    //{"LatCondutor":-8.1090517,"LngConductor":-79.0215}
                }
            }

            @Override
            public void ConductorAsignadoaReserva(String s) {
                SOIB.ConductorAsignadoaReserva(s);
            }

            @Override
            public void connect(String s) {
                JsonPack.UpdateClienteSocket ucs = new JsonPack.UpdateClienteSocket();
                ucs.idCliente = Session.cliente_id;
                ucs.token = "0";
                ucs.tipo = "2";
                ucs.socketCliente = "/#" + SI.SOCKET.id();

                SI.emitData("setUpdateSocketCliente", new Gson().toJson(ucs));

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.connect(s);
                else
                    Log.e(SocketIo.class.getSimpleName(), "connect null bridge ");

                startForeground(1, Notificaciones.getNotificacionDetener(getApplicationContext()));
            }

            @Override
            public void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns) {
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null)) {
                    SOIB.nuevaSolicitudConductor(ns);
                } else {
                    if (Solicitudes == null)
                        Solicitudes = new ArrayList<>();

                    Solicitudes.add(ns);
                    if (AXB != null && (AppConexion != null && AppConexion.getRoot() != null)) {
                        AXB.onNuevasSolicitudes(Solicitudes.size());
                    } else {
                        //NM.cancel(notidpp);
                        NM.notify(notidpp, Notificaciones.getNotificacionSolicitud(getApplicationContext(), Solicitudes.size()));
                    }
                }
            }

            @Override
            public void ServicioCaducado(String idservicio) {

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.ServicioCaducado(idservicio);
                else
                    Log.e(SocketIo.class.getSimpleName(), "ServicioCaducadonull bridge ");

            }

            @Override
            public void ServicioCancelado(JsonPack.CancelarServicio s) {
                Log.e("Cliente", "Servicio cancelado por el taxista");
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null)) {
                    SOIB.ServicioCancelado(s);
                } else {
                    NM.notify(notidpp, Notificaciones.getNotificationCancelarServicio(getApplicationContext()));
                    Log.e(SocketIo.class.getSimpleName(), "ConductorSeleccionado null bridge ");
                }
            }

            @Override
            public void ConductorSeleccionado(JsonPack.Servicio s) {
                ServicioActual = s;
                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.ConductorSeleccionado(s);
                else
                    Log.e(SocketIo.class.getSimpleName(), "ConductorSeleccionado null bridge ");
            }

            @Override
            public void reconnect(String s) {
                Log.d("socket", ">reconnect");
            }

            @Override
            public void disconnect(String s) {
                Log.d("socket", ">disconnect :p");

                if (SOIB != null && (AppConexion != null && AppConexion.getRoot() != null))
                    SOIB.disconnect(s);
                else
                    Log.e(SocketIo.class.getSimpleName(), "disconnect null bridge ");


                Log.i("disconnect", "-----------------before noti");
                //startForeground(1, Notificaciones.getNotificacionEncender(getApplicationContext()));
                Log.i("disconnect", "-----------------after noti");
                //SI.SOCKET.close();
            }

            @Override
            public void connect_error(String s) {
                Log.d("socket", ">connect_error");
            }

            @Override
            public void connect_tiemout(String s) {
                Log.d("socket", ">connect_tiemout");
            }

        };

        SI = new SocketIo(isoc, Urls.servidornodejssocket);


        MYGPS = new MyGps(getApplicationContext(), new MyGps.IMyGps() {

            @Override
            public void onLocation(Location l) {

                /*
                JsonPack.UpdateGeoConductor ucg=new JsonPack.UpdateGeoConductor();
                ucg.idCliente=Session.cliente_id;
                ucg.latitud=l.getLatitude();
                ucg.longitud=l.getLongitude();
                SI.emitData("setUpdateGeoConductor", new Gson().toJson(ucg));*/
                MYGPS.turnOff();
                JsonPack.AlertaWeb A = new JsonPack.AlertaWeb();
                if (MYGPS != null && MYGPS.lastKnownLocation != null) {
                    A.latitud = MYGPS.lastKnownLocation.getLatitude();
                    A.longitud = MYGPS.lastKnownLocation.getLongitude();

                    if (getServicioActual() != null) {
                        A.tipo = "3";
                        A.codigo = getServicioActual().IdServicio;
                        A.flag = 1;
                    } else {
                        A.codigo = new SPUser(getApplicationContext()).getResponseLogin().cliente_id;
                        A.tipo = "1";
                        //A.flag=2;
                    }
                    SI.emitData("nuevaAlertaWeb", new Gson().toJson(A));
                } else {
                    Log.e("EviarAlerta", "Sin coordaenadas");
                }


            }

            @Override
            public void onOldLocation(Location l) {

            }

            @Override
            public void onEstaApagado() {

            }

            @Override
            public void onFueApagado() {

            }
        });


    }


    @Override
    public void onDestroy() {

        if (wl != null) wl.release();
        super.onDestroy();

    }



}
