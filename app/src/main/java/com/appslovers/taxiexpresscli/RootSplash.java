package com.appslovers.taxiexpresscli;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.data.InnerActions;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyAppBackground;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.data.SqlitePack;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.auth.RootLoginReg;
import com.appslovers.taxiexpresscli.body.RootBody;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.internal.ObjectConstructor;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by javierquiroz on 20/05/16.
 */
public class RootSplash extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rootsplash);
    }

    SPUser sp;

    CountDownTimer CDT = new CountDownTimer(1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            if (logueado)
                startActivity(new Intent(getApplicationContext(), RootBody.class));
            else
                startActivity(new Intent(getApplicationContext(), RootLoginReg.class));

            finish();
        }
    };

    boolean logueado;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    public void initComonProces() {
        sp = new SPUser(getApplicationContext());
        //createOrUpdateShortcut();
        if (sp.getResponseLogin() != null) {
            logueado = true;
            JsonPack.ModCount mc = ((AppForeGround) getApplication()).getDb().getCountMod();
            //syncZonas.putParams("mod", mc.mMod).putParams("tot", mc.mTot).send();
            syncZonas.putParams("fecha",sp.getDate()).putParams("id",sp.getLastId()).send();
                    //putParams("mod", "0").putParams("tot", "0").send();
        } else {
            CDT.start();
        }


    }

    String[] permisos = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
            , android.Manifest.permission.CALL_PHONE
            , android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onStart() {
        super.onStart();

        boolean flagsolicitar = false;
        for (String s : permisos) {
            if (ContextCompat.checkSelfPermission(RootSplash.this, s) != PackageManager.PERMISSION_GRANTED) {
                flagsolicitar = true;
                break;
            }
        }
        if (flagsolicitar) {
            ActivityCompat.requestPermissions(RootSplash.this, permisos, 100);
        } else {
            initComonProces();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                boolean flagsolicitar = false;
                for (int p : grantResults) {
                    if (p != PackageManager.PERMISSION_GRANTED) {
                        flagsolicitar = true;
                        break;
                    }
                }

                if (!flagsolicitar) {
                    initComonProces();
                } else {
                    Toast.makeText(getApplicationContext(), "Por favor acepte los permisos necesarios", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(RootSplash.this, permisos, 100);
                }
            }
            break;
        }
    }


    MyRequest syncZonas = new MyRequest(null, Urls.ws_lista_zonas, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            //ArrayList<SqlitePack.Zona> zonas = null;
            ArrayList<JsonPack.ZonaSQL> zonas = null;
            JsonPack.SynchronizeZonesResponse respuesta=new Gson().fromJson(rw.ResponseBody, JsonPack.SynchronizeZonesResponse.class);
            try {
                Log.i("response", ">" + rw.ResponseBody);
                //zonas = new Gson().fromJson(rw.ResponseBody, new TypeToken<ArrayList<SqlitePack.Zona>>() {}.getType());
                zonas = respuesta.list;
            } catch (JsonParseException e) {
                Log.e("jsonparse", ">" + e.getMessage());
                System.out.println("ERROR JsonParseException  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ");
            }

            if (zonas != null) {
                sp.setDate(respuesta.fecha);
                sp.setLastId(respuesta.end_id);
                ((AppForeGround) getApplication()).getDb().fillZonas(zonas);
                Log.i("after fillzone", "------------->>>>>>>>>>>>>>>>>");
                ArrayList<JsonPack.Zona> ZZ = ((AppForeGround) getApplication()).getDb().getZonas();
                Log.e("zonas", ZZ.size() + " Zonas registradas en SQLite");
            }
            syncTipoServicio.putParams("d", "d").send();
        }

        @Override
        public void onFailedRequestForeground(ResponseWork rw) {
            syncTipoServicio.putParams("d", "d").send();
        }
    };

    MyRequest syncTipoServicio = new MyRequest(null, Urls.ws_tipo_servicio, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {
            ArrayList<JsonPack.TipoServicio> tipos = null;

            try {
                tipos = new Gson().fromJson(rw.ResponseBody, new TypeToken<ArrayList<JsonPack.TipoServicio>>() {
                }.getType());
            } catch (JsonParseException e) {
                Log.e("jsonparse", ">" + e.getMessage());
            }

            if (tipos != null) {
                new SPUser(getApplicationContext()).setTiposServico(tipos);
            }

            if (sp.getResponseLogin().cliente_id != null)
                getEstadoCliente.putParams("codigo", sp.getResponseLogin().cliente_id).putParams("flag", "2").send();
            else
                CDT.start();

        }

        @Override
        public void onFailedRequestForeground(ResponseWork rw) {

            if (sp.getResponseLogin().cliente_id != null)
                getEstadoCliente.putParams("codigo", sp.getResponseLogin().cliente_id).putParams("flag", "2").send();
            else
                CDT.start();
        }
    };

    MyRequest<JsonPack.RespEstadoCliente> getEstadoCliente = new MyRequest<JsonPack.RespEstadoCliente>(null, Urls.estado, MyRequest.HttpRequestType.POST) {

        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespEstadoCliente object) {
            Log.i("Calf cond", ">" + object.CalificacionCliente);
            Log.i(" cliente objeto", " " + object.Conductor);
            Log.i(" servicio objeto", " " + object.Servicio);

            ((AppForeGround) getApplication()).flag_califico = object.CalificacionCliente;
            ((AppForeGround) getApplication()).flag_tipotarifa = object.TipoTarifa;
            Intent servici = new Intent(getApplicationContext(), AppBackground.class);
            servici.setAction(InnerActions.notact_updateestado.toString());
            servici.putExtra("rec", object);
            startService(servici);

            /*RECUPERANDO API KEYS*/
            Urls.FOURSQUARE_CLIENT_ID = object.foursquare_key;
            Urls.FOURSQUARE_CLIENT_SECRET = object.foursquare_secret;
            Urls.GOOGLE_KEY = object.google_android;
            Urls.HERE_APP_ID = object.here_key;
            Urls.HERE_APP_CODE = object.here_secret;
            CDT.start();
        }
    };


    @Override
    protected void onStop() {

        if (CDT != null)
            CDT.cancel();

        super.onStop();
    }

    public void createOrUpdateShortcut() {
        if (!sp.checkInstalationShotCut()) {
            Intent HomeScreenShortCut = new Intent(getApplicationContext(), RootSplash.class);
            HomeScreenShortCut.setAction(Intent.ACTION_MAIN);
            HomeScreenShortCut.putExtra("duplicate", false);

            Intent addIntent = new Intent();
            addIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            addIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, HomeScreenShortCut);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.launcher));
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent);
        }
    }

}
