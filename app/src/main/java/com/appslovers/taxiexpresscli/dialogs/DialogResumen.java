package com.appslovers.taxiexpresscli.dialogs;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.body.FragSolicitudDestino;
import com.appslovers.taxiexpresscli.util.Tools;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogResumen extends MyDialog implements View.OnClickListener {
    View ViewRoot;

    TextView tvDirOri, tvDirDes, tvRefOri, tvRefDes, tvTarifa, tvTipoPago, tvTipoServ;
    TextView tvLabelTarifa;
    ArrayList<JsonPack.TipoServicio> tiposServices;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot = inflater.inflate(R.layout.dialog_resumen, null);
        ViewRoot.findViewById(R.id.btnConfirmar).setOnClickListener(this);
        tvLabelTarifa = (TextView) ViewRoot.findViewById(R.id.tvLabelTarifa);
        tvDirOri = (TextView) ViewRoot.findViewById(R.id.tvDirOri);
        tvDirDes = (TextView) ViewRoot.findViewById(R.id.tvDirDes);
        tvRefOri = (TextView) ViewRoot.findViewById(R.id.tvRefOri);
        tvRefDes = (TextView) ViewRoot.findViewById(R.id.tvRefDes);
        tvTipoPago = (TextView) ViewRoot.findViewById(R.id.tvTipoPago);
        tvTipoServ = (TextView) ViewRoot.findViewById(R.id.tvTipoServ);
        tvTarifa = (TextView) ViewRoot.findViewById(R.id.tvTarifa);
        if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 2) {
            tvLabelTarifa.setText(getString(R.string.uctarifa) + " APROXIMADA");
        } else if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 1) {
            tvLabelTarifa.setText(getString(R.string.uctarifa));
        }
        tiposServices = new SPUser(getActivity()).getTiposServicio();

        return ViewRoot;
    }


    JsonPack.Servicio SERVICIO;

    public void setServicio(JsonPack.Servicio s) {
        SERVICIO = s;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (SERVICIO != null) {
            tvDirOri.setText(SERVICIO.OrigenDireccion);
            tvRefOri.setText(SERVICIO.OrigenReferencia);
            tvDirDes.setText(SERVICIO.DestinoDireccion);
            tvRefDes.setText(SERVICIO.DestinoReferencia);

            for (JsonPack.TipoServicio ts : tiposServices) {
                if (ts.id == Integer.parseInt(SERVICIO.TipoServicio)) {
                    tvTipoServ.setText(ts.nombre);
                    break;
                }
            }

            if (SERVICIO.Tarifa != null)
                tvTarifa.setText("S/. " + Tools.formatFloat(Tools.getFloat(SERVICIO.Tarifa)));
            else
                tvTarifa.setText("S/. 0.00");

            tvTipoPago.setText(FragSolicitudDestino.getTipoPago(SERVICIO.TipoPago));

        }
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }

    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        if (SERVICIO.Tarifa != null) {
            if (Double.parseDouble(SERVICIO.Tarifa.trim()) != 0.0d) {
                dismiss();
                getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());
            } else {
                Toast.makeText(getActivity(), "Esta ruta no se encuentra disponible", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
