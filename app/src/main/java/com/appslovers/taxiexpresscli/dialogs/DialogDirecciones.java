package com.appslovers.taxiexpresscli.dialogs;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.IMyAdapter;
import com.appslovers.taxiexpresscli.data.IMyViewHolder;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.util.MyGps;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.body.RootBody;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogDirecciones extends MyDialog implements IMyViewHolder {


    View ViewRoot;
    RecyclerView rvLista;
    EditText etDireccion;
    ArrayList<JsonPack.DirLug> Direcciones;
    LinearLayoutManager llm;
    MyAdapter<DialogDirecciones.DireccionesViewHolder, JsonPack.DirLug> mAdapter;
    MyGps<RootBody> GPS;
    ProgressBar pbBuscando;
    InputMethodManager IMM;
    ImageView iconView;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        IMM = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        ViewRoot = inflater.inflate(R.layout.dialog_direcciones, null);

        pbBuscando = (ProgressBar) ViewRoot.findViewById(R.id.pbBuscando);


        ViewRoot.findViewById(R.id.ivBuscar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //IMM.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                IMM.hideSoftInputFromWindow(etDireccion.getWindowToken(), 0);

                pbBuscando.setVisibility(View.VISIBLE);

                Direcciones.clear();
                mAdapter.notifyDataSetChanged();

                getJsonHere();
                getJsonGoogleGeoCode();
                getJsonFoursquare();
            }

        });
        etDireccion = (EditText) ViewRoot.findViewById(R.id.etDireccion);
        etDireccion.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    //IMM.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    IMM.hideSoftInputFromWindow(etDireccion.getWindowToken(), 0);

                    pbBuscando.setVisibility(View.VISIBLE);

                    Direcciones.clear();
                    mAdapter.notifyDataSetChanged();

                    getJsonHere();
                    getJsonGoogleGeoCode();
                    getJsonFoursquare();
                    //Direcciones.clear();
                    //mAdapter.notifyItemRangeChanged(0,0);
                    return true;
                }

                return false;

            }

        });

        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lista);

        Direcciones = new ArrayList<>();

        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);
        mAdapter = new MyAdapter<>(Direcciones, this, R.layout.dialog_direcciones_item);
        rvLista.setAdapter(mAdapter);
        GPS = new MyGps<RootBody>((RootBody) getActivity(), new MyGps.IMyGps() {
            @Override
            public void onLocation(Location l) {

                //Toast.makeText(getActivity(), "Location captures > "+l.getLatitude()+","+l.getLongitude(), Toast.LENGTH_SHORT).show();
                MyLocation = l;
                if (MyLocation != null) {
                    Log.i("onLocation got it", ">" + l.getLongitude() + "," + l.getLatitude());

                    if (switchjsonhere) {
                        getJsonHere();
                        getJsonGoogleGeoCode();
                        getJsonFoursquare();
                    }

                    GPS.turnOff();
                } else {
                    Log.e("onLocation", "> es null");
                }


            }

            @Override
            public void onOldLocation(Location l) {
                MyLocation = l;
                if (MyLocation != null) {
                    Log.i("onoldlocation got it", ">" + l.getLongitude() + "," + l.getLatitude());
                    if (switchjsonhere) {
                        getJsonHere();
                        getJsonGoogleGeoCode();
                        getJsonFoursquare();
                    }


                    GPS.turnOff();
                } else {
                    Log.e("onOldLocation", "> es null");
                }

            }

            @Override
            public void onEstaApagado() {


            }

            @Override
            public void onFueApagado() {

            }

        });

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        }

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        }


        return ViewRoot;
    }

    public String textoBusqueda = "";
    public String hintBusqueda = "";

    public void setTexoBusqueda(String s) {
        textoBusqueda = s;
    }

    @Override
    public void onStart() {
        super.onStart();

        contador_req = 0;
        if (textoBusqueda != null)
            etDireccion.setText(textoBusqueda);
        if (hintBusqueda != null)
            etDireccion.setHint(hintBusqueda);


        //etDireccion.selectAll();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                etDireccion.selectAll();
            }
        });
    }

    Location MyLocation;

    @Override
    public void onResume() {
        super.onResume();
        GPS.turnOn(true);
    }


    @Override
    public void onStop() {

        if (GPS != null)
            GPS.turnOff();

        super.onStop();
    }


    public boolean switchjsonhere = false;


    int contador_req = 0;

    public void getJsonHere() {
        switchjsonhere = true;

        if (MyLocation != null) {


            new MyRequest<JsonPack.Here>(this, Urls.here_geocode, MyRequest.HttpRequestType.GET) {
                @Override

                public void onParseSuccesForeground(ResponseWork rw, JsonPack.Here object) {

                    contador_req++;


                    Direcciones.addAll(object.getDirLug());
                    mAdapter.notifyItemRangeChanged(Direcciones.size(), object.getDirLug().size());
                    //getJsonGoogleGeoCode();
                    if (contador_req >= 3) {
                        pbBuscando.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailedRequest(ResponseWork rw, boolean isActive) {
                    contador_req++;
                    if (contador_req >= 3) {
                        pbBuscando.setVisibility(View.GONE);
                    }
                }
            }
                    .putParams("searchtext", etDireccion.getText().toString() + " " + getResources().getString(R.string.searchdir_city))
                    .putParams("gen", "9")
                    .putParams("app_id", Urls.HERE_APP_ID)
                    .putParams("app_code", Urls.HERE_APP_CODE)
                    .putParams("prox", MyLocation.getLatitude() + "," + MyLocation.getLongitude() + ",1000")
                    .putParams("language", "es")
                    .putParams("city", getResources().getString(R.string.searchdir_city))
                    .putParams("country", getResources().getString(R.string.searchdir_country))
                    //.putParams("prox", "-12.046374,-77.0427934,50000")
                    .send();
        }


    }

    public void getJsonGoogleGeoCode() {
        if (MyLocation != null) {
            new MyRequest<JsonPack.GoogleGeoCode>(this, Urls.google_geocode, MyRequest.HttpRequestType.GET) {

                @Override
                public void onParseSuccesForeground(ResponseWork rw, JsonPack.GoogleGeoCode object) {

                    contador_req++;
                    Direcciones.addAll(object.getDirLug());
                    mAdapter.notifyItemRangeChanged(Direcciones.size(), object.getDirLug().size());
                    //getJsonFoursquare();

                    if (contador_req >= 3) {
                        pbBuscando.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailedRequest(ResponseWork rw, boolean isActive) {
                    contador_req++;
                    if (contador_req >= 3) {
                        pbBuscando.setVisibility(View.GONE);
                    }
                }
            }
                    .putParams("address", etDireccion.getText().toString() + " " + getResources().getString(R.string.searchdir_country))
                    .putParams("components", "country:pe|administrative_area_level_1=" + getString(R.string.searchdir_city))
                    .putParams("bounds", getString(R.string.searchdir_google_bounds))
                    .putParams("language", "es")
                    .putParams("sensor", "false")
                    .send();
        }


    }

    public void getJsonFoursquare() {
        if (MyLocation != null) {
            new MyRequest<JsonPack.Foursquare>(this, Urls.foursquare_geocode, MyRequest.HttpRequestType.GET) {

                @Override
                public void onParseSuccesForeground(ResponseWork rw, JsonPack.Foursquare object) {

                    contador_req++;
                    Direcciones.addAll(object.getDirLug());
                    mAdapter.notifyItemRangeChanged(Direcciones.size(), object.getDirLug().size());

                    if (contador_req >= 3) {
                        pbBuscando.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailedRequest(ResponseWork rw, boolean isActive) {
                    contador_req++;
                    if (contador_req >= 3) {
                        pbBuscando.setVisibility(View.GONE);
                    }
                }
            }
                    .putParams("client_id", Urls.FOURSQUARE_CLIENT_ID)
                    .putParams("client_secret", Urls.FOURSQUARE_CLIENT_SECRET)
                    .putParams("v", "20130815")
                    //.putParams("ll","-12.04664447,-77.04233408")
                    .putParams("ll", MyLocation.getLatitude() + "," + MyLocation.getLongitude())
                    //.putParams("query", etDireccion.getText().toString() + " " + getString(R.string.searchdir_city))
                    .putParams("query", etDireccion.getText().toString())
                    .putParams("llAcc", "1000.0")
                    // .putParams("near", "LIMA,PE")
                    .putParams("limit", "20")
                    .send();
        }

    }

    @Override
    public boolean isMatchParent() {
        return true;
    }


    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public int getMargins() {
        return 15;
    }


    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new DireccionesViewHolder(v);
    }

    public class DireccionesViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<DireccionesViewHolder>, View.OnClickListener {


        TextView tvTexto;

        public DireccionesViewHolder(View v) {
            super(v);
            tvTexto = (TextView) v.findViewById(R.id.tvDireccion);
            tvTexto.setOnClickListener(this);
            iconView = (ImageView) v.findViewById(R.id.imageView22);

        }


        @Override
        public void bindView(DireccionesViewHolder holder, int position) {

            holder.tvTexto.setText(Direcciones.get(position).name);
            switch (Direcciones.get(position).from) {
                case 1:
                    //holder.tvTexto.setTextColor(Color.rgb(102, 15, 130));
                    break;

                case 2:
                    //holder.tvTexto.setTextColor(Color.rgb(0, 0, 0));
                    break;
                case 3:
                    //holder.tvTexto.setTextColor(Color.BLUE);
                    //holder.tvTexto.setText(Direcciones.get(position).direccion);

                    holder.tvTexto.setText(Direcciones.get(position).name + "\n" + Direcciones.get(position).direccion);
                    ((AppForeGround) getActivity()
                            .getApplication())
                            .getPicasso()
                            .load(Direcciones.get(position).iconURL)
                            .into(iconView);
                    break;
            }


        }

        @Override
        public void onClick(View v) {

            //Toast.makeText(getActivity(), ">" + getLayoutPosition(), Toast.LENGTH_SHORT).show();

            Intent i = new Intent();
            i.putExtra("dir", Direcciones.get(getLayoutPosition()));
            getTargetFragment().onActivityResult(getTargetRequestCode(), 200, i);
            dismiss();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 100:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Toast.makeText(getActivity(),"PERMISO CONSEDIDO 1",Toast.LENGTH_SHORT).show();

                } else {

                    //Toast.makeText(getActivity(),"PERMISO DENEGADO 1",Toast.LENGTH_SHORT).show();
                }

                break;

            case 200:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Toast.makeText(getActivity(),"PERMISO CONSEDIDO 2",Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    //Toast.makeText(getActivity(),"PERMISO DENEGADO 2",Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                break;

        }

        // other 'case' lines to check for other
        // permissions this app might request

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
