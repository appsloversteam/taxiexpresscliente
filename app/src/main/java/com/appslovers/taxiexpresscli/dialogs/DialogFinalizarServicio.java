package com.appslovers.taxiexpresscli.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.Tools;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogFinalizarServicio extends MyDialog implements View.OnClickListener {

    View ViewRoot;

    TextView etTarifa, etAdicionales, etTotal;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot = inflater.inflate(R.layout.dialog_finalizarserv, null);
        etTarifa = (TextView) ViewRoot.findViewById(R.id.etTarifa);
        etAdicionales = (TextView) ViewRoot.findViewById(R.id.etAdicionales);
        etTotal = (TextView) ViewRoot.findViewById(R.id.etTotal);

        ViewRoot.findViewById(R.id.btnAceptar).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnRechazar).setOnClickListener(this);

        return ViewRoot;
    }


    public JsonPack.RespFinalizarServ FS;

    @Override
    public void onResume() {
        super.onResume();

        if (FS != null) {
            etTarifa.setText("S/. " + Tools.formatFloat(FS.Tarifa));
            etAdicionales.setText("S/. " + Tools.formatFloat(FS.Espera + FS.Paradas + FS.Parqueos + FS.Peajes));
            etTotal.setText("S/." + Tools.formatFloat(FS.Tarifa + FS.Espera + FS.Paradas + FS.Parqueos + FS.Peajes));
        }
    }

    @Override
    public boolean releaseOnBack() {
        return false;
    }

    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }

    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {
        dismiss();
        getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());
    }
}
