package com.appslovers.taxiexpresscli.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.auth.RootLoginReg;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogRegistroEnProceso extends MyDialog {


    View ViewRoot;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_registroenproceso,null);
        ViewRoot.findViewById(R.id.tvCerrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SPUser.logout(getActivity());
                getActivity().finish();
                getActivity().startActivity(new Intent(getActivity(), RootLoginReg.class));
            }
        });
        return ViewRoot;
    }


    @Override
    public boolean releaseOnBack() {
        return true;
    }

    @Override
    public boolean dismissOnTouchOut() {
        return false;
    }

    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }
}
