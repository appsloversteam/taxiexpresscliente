package com.appslovers.taxiexpresscli.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.appslovers.taxiexpresscli.R;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogEnviarNotificacion extends MyDialog implements View.OnClickListener{


    View ViewRoot;

    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_enviarnotificacion,null);
        ViewRoot.findViewById(R.id.btnEnviarAlerta).setOnClickListener(this);

        return ViewRoot;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }


    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        dismiss();
        getTargetFragment().onActivityResult(getTargetRequestCode(),200,new Intent());

    }
}
