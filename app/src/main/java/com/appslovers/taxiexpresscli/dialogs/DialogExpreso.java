package com.appslovers.taxiexpresscli.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.R;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogExpreso extends MyDialog implements View.OnClickListener{


    View ViewRoot;

    CheckBox rbNoConfirmar;
    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_expreso,null);

        rbNoConfirmar=(CheckBox) ViewRoot.findViewById(R.id.rbNoConfirmar);
        ViewRoot.findViewById(R.id.btnSi).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnNo).setOnClickListener(this);

        return ViewRoot;
    }

    SPUser SPCLI;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();
        SPCLI=new SPUser(getActivity());
        if(SPCLI.getExpreso())
        {
            rbNoConfirmar.setChecked(false);
        }
        else
        {
            rbNoConfirmar.setChecked(true);
        }
    }

    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }

    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnSi:

                if(rbNoConfirmar.isChecked())
                {
                   SPCLI.setExpreso(false);
                }
                dismiss();
                getTargetFragment().onActivityResult(getTargetRequestCode(),200,new Intent());
                break;
            case R.id.btnNo:
                dismiss();
                break;
        }


    }
}
