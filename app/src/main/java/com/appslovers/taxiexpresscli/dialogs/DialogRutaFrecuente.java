package com.appslovers.taxiexpresscli.dialogs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.R;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 26/05/16.
 */
public class DialogRutaFrecuente extends MyDialog implements View.OnClickListener{


    View ViewRoot;

    EditText etNombre;

    ArrayList<JsonPack.TipoServicio> tiposServices;
    @Override
    public View onGetCreateView(LayoutInflater inflater) {

        ViewRoot =inflater.inflate(R.layout.dialog_rutafrec,null);
        ViewRoot.findViewById(R.id.btnAceptar).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnRechazar).setOnClickListener(this);
       etNombre=(EditText) ViewRoot.findViewById(R.id.etNombre);


        tiposServices= new SPUser(getActivity()).getTiposServicio();

        return ViewRoot;
    }


    JsonPack.Servicio SERVICIO;
    public void setServicio(JsonPack.Servicio s)
    {
        SERVICIO=s;
    }


    public String Nombre;
    @Override
    public void onResume() {
        super.onResume();

        if(SERVICIO!=null)
        {


        }
    }

    @Override
    public boolean isMatchParent() {
        return false;
    }

    @Override
    public boolean isCloseVisible() {
        return false;
    }

    @Override
    public int getMargins() {
        return 15;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnAceptar:

                dismiss();
                Nombre=etNombre.getText().toString();
                getTargetFragment().onActivityResult(getTargetRequestCode(),200,new Intent());
                break;

            case R.id.btnRechazar:
                dismiss();
                break;

        }


    }
}
