package com.appslovers.taxiexpresscli.data;

import android.support.v7.widget.RecyclerView;

/**
 * Created by javierquiroz on 1/06/16.
 */
public interface IMyAdapter<VHH extends RecyclerView.ViewHolder> {

    void bindView(VHH holder, int position);

}
