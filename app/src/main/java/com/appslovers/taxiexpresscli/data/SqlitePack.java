package com.appslovers.taxiexpresscli.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by javierquiroz on 10/06/16.
 */
public class SqlitePack implements Serializable{

    public static class Zona implements Serializable
    {

        //public String lugares_id,lugar_descripcion,lugar_lat,lugar_lon,lugar_coordpolygono,lugar_fecharegistro,lugar_fechaupdate,lugar_estado,lugar_mod,lugar_update;


        public long _id;
        public String id;
        public String nombre;
        public Coordenada centro;
        public ArrayList<Coordenada> coordenadas;
        public double centrolatitud;
        public double centrolongitud;
        public long mod;
        public long estado;
    }

    public static class Coordenada implements Serializable
    {
        public long _id;
        public String _idzona;
        public double latitud;
        public double longitud;
    }
}
