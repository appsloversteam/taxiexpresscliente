package com.appslovers.taxiexpresscli.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by javierquiroz on 10/06/16.
 */
public class MyDataBase extends SQLiteOpenHelper {

    final static int VERSION = 2;
    final static String NOMBRE = "taxidb";

    Context CON;
    SQLiteDatabase RDB;
    SQLiteDatabase WDB;

    public MyDataBase(Context context) {
        super(context, NOMBRE, null, VERSION);
        CON = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        builddb(SqlitePack.class, db, true);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        builddb(SqlitePack.class, db, false);
        onCreate(db);
    }


    public JsonPack.ModCount getCountMod() {

        if (RDB == null) RDB = getReadableDatabase();


        JsonPack.ModCount MC = new JsonPack.ModCount();
        Cursor c = RDB.rawQuery("SELECT count(_id), max( CAST( mod AS NUMBER) ) FROM zona", null);
        if (c != null) {
            if (c.moveToFirst()) {
                MC.mTot = c.getString(0);
                MC.mMod = c.getString(1) == null ? "0" : c.getString(1);
            }
        }
        return MC;
    }

    public void fillZonas(ArrayList<JsonPack.ZonaSQL> Zonas) {


        WDBready();

        /*LIMPIANDO TABLAS...*/
        //WDB.execSQL("DELETE FROM zona");
        //WDB.execSQL("DELETE FROM coordenada");

        String sqlquery = "INSERT OR REPLACE INTO zona" +
                " (id, nombre,centrolatitud, centrolongitud,mod,estado) VALUES" +
                " (?,?,?,?,?,?)";

        Log.i("query", sqlquery);


        WDB.beginTransaction();

        for (JsonPack.ZonaSQL Z : Zonas) {
            Z.coordenadas=new Gson().fromJson(Z.lugar_coordpolygono, ArrayList.class);
            SQLiteStatement SQLS = WDB.compileStatement(sqlquery);
            SQLS.clearBindings();
            SQLS.bindString(1, Z.lugares_id);///Log.i("Z.id", Z.id);
            SQLS.bindString(2, Z.lugar_descripcion);//Log.i("Z.nombre",Z.nombre);
            SQLS.bindDouble(3, Z.lugar_lat!= null ? Double.parseDouble(Z.lugar_lat) : 0.0d);
            SQLS.bindDouble(4, Z.lugar_lon != null ? Double.parseDouble(Z.lugar_lon) : 0.0d);
            SQLS.bindLong(5, 0);
            SQLS.bindLong(6, Long.parseLong(Z.lugar_estado));
            SQLS.execute();
            SQLS.close();

        }
        WDB.setTransactionSuccessful();
        WDB.endTransaction();


        String sqlquery2 = "INSERT INTO coordenada" +
                " (_idzona, latitud,longitud) VALUES" +
                " (?,?,?)";

        Log.i("query2", sqlquery2);


        WDB.beginTransaction();

        for (JsonPack.ZonaSQL Z : Zonas) {

            //if (Z.estado == 1) {
                WDB.execSQL("DELETE FROM coordenada WHERE _idzona = " + Z.lugares_id);
                for (String C : Z.coordenadas) {
                    JsonPack.Coordenada coord= new JsonPack.Coordenada();
                    coord.parseCoordinates(C);
                    SQLiteStatement SQLS2 = WDB.compileStatement(sqlquery2);
                    SQLS2.clearBindings();
                    SQLS2.bindString(1, Z.lugares_id);//Log.i("Z.id>" + Z.id, ">" + Z.nombre);
                    SQLS2.bindDouble(2, coord != null ? coord.latitud : 0.0d);
                    SQLS2.bindDouble(3, coord != null ? coord.longitud: 0.0d);
                    SQLS2.execute();
                    SQLS2.close();

                }
           // }*/
        }
        WDB.setTransactionSuccessful();
        WDB.endTransaction();

    }

    public ArrayList<JsonPack.Zona> getZonas() {
        RDBready();
        ArrayList<JsonPack.Zona> Zonas = null;
        String sqlquery = "SELECT id, nombre,centrolatitud, centrolongitud FROM zona WHERE estado = 1";
        Cursor cur = RDB.rawQuery(sqlquery, null);

        Log.i("ZONAS", ">" + cur.getCount());

        if (cur != null) {

            //int _id=cur.getColumnIndex("_id");
            int id = cur.getColumnIndex("id");
            int nombre = cur.getColumnIndex("nombre");
            int latitud = cur.getColumnIndex("centrolatitud");
            int longitud = cur.getColumnIndex("centrolongitud");


            //Log.d("cur1 ",">"+cur.getCount());
            Zonas = new ArrayList<>();
            if (cur.moveToFirst()) {

                do {

                    JsonPack.Zona Z = new JsonPack.Zona();
                    Z.id = cur.getString(id);
                    Z.nombre = cur.getString(nombre);
                    JsonPack.Coordenada cor = new JsonPack.Coordenada();//podria comentarse
                    cor.latitud = cur.getDouble(latitud);
                    cor.longitud = cur.getDouble(longitud);

                    Z.ltlncentro = new LatLng(cor.latitud, cor.longitud);

                    Z.centro = cor;//podria comentarse

                    String sqlquery2 = "SELECT  latitud, longitud FROM coordenada WHERE _idzona =  " + Z.id;
                    Cursor cur2 = RDB.rawQuery(sqlquery2, null);

                    if (cur2 != null) {
                        //Log.d("cur2 ", ">" + cur2.getCount());
                        ArrayList<JsonPack.Coordenada> Coordendas = new ArrayList<>();//se podria comentar
                        int lat = cur2.getColumnIndex("latitud");
                        int lon = cur2.getColumnIndex("longitud");
                        if (cur2.moveToFirst()) {

                            do {

                                JsonPack.Coordenada corinside = new JsonPack.Coordenada();
                                corinside.latitud = cur2.getDouble(lat);
                                corinside.longitud = cur2.getDouble(lon);

                                Z.getLtns().add(corinside.generar());

                                Coordendas.add(corinside);//se podria comentar
                            } while (cur2.moveToNext());

                            Z.coordenadas = Coordendas;
                        }
                        cur2.close();
                    } else {
                        //Log.d("cur2 ",">null");
                    }

                    Zonas.add(Z);


                } while (cur.moveToNext());

            }
            cur.close();
        } else {
            //Log.d("cur1 ",">null");
        }

        return Zonas;
    }


    private void RDBready() {
        if (RDB == null)
            RDB = getReadableDatabase();
    }

    private void WDBready() {
        if (WDB == null)
            WDB = getWritableDatabase();
    }


    public static void builddb(Class clase, SQLiteDatabase db, boolean create) {

        for (Class CC : clase.getClasses()) {
            if (create) {
                String tabla = "CREATE TABLE IF NOT EXISTS " + CC.getSimpleName().toLowerCase() + "(";
                String campos = "";

                int k = 0;
                Field[] fields = CC.getFields();

                for (Field F : fields) {
                    boolean flag = true;
                    k++;
                    String tipo = " TEXT";

                    if (F.getType() == String.class) {
                        tipo = " TEXT";
                    } else if (F.getType() == int.class || F.getType() == long.class) {
                        tipo = " INTEGER";
                    } else if (F.getType() == float.class || F.getType() == double.class) {
                        tipo = " REAL";
                    } else {
                        flag = false;
                    }
                    if (flag) {

                        if (F.getName().equals("_id")) {
                            campos += " _id INTEGER PRIMARY KEY AUTOINCREMENT";
                        } else {
                            campos += " " + F.getName() + "" + tipo;
                        }

                    }
                    if (k == fields.length) {
                        if (!flag) {
                            campos = campos.substring(1, campos.length() - 1);
                        }
                        campos += " )";
                    } else {
                        if (flag)
                            campos += " ,";
                    }
                }
                Log.v("Generando...", "" + tabla + campos);
                db.execSQL(tabla + campos);

            } else {
                String tabla = "DROP TABLE IF EXISTS " + CC.getSimpleName();
                db.execSQL(tabla);
            }
        }

    }
}
