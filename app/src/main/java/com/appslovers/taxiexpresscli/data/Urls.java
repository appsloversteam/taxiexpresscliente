package com.appslovers.taxiexpresscli.data;

/**
 * Created by javierquiroz on 15/07/16.
 */
public class Urls {





    public  static String HERE_APP_ID = "T2apR9nUv5uZLGD4euEt";
    public  static String HERE_APP_CODE = "O-S7lCFdk4kpibVVYaANog";
    public static String FOURSQUARE_CLIENT_ID = "SFHUDNIS5SHIOP41IDVIFK10JEIGIUHVDYLBDQJU2GUVDJ5E";
    public static String FOURSQUARE_CLIENT_SECRET = "J3CG5DDVUFO0SBGWZNXFXB2FZRBEF0A53TTBXMFH1ROZHKWJ";
    public static String GOOGLE_KEY = "AIzaSyBSdHr2_3zoO9C8-5jSWoABt5IU5z9lRsc";

    private final static String servidor = "http://taxiexpress.tutaxidispatch.com";
    //private final static String servidor="http://192.168.1.22";
    private final static String puerto = "3000";
    //private final static String puerto = "3004";

    private final static String servidornodejs = servidor + ":" + puerto + "/";
    public final static String servidornodejssocket = servidor + ":" + puerto;
    private final static String servidorphp = servidor + "/ws/cliente/";
    //private final static String servidorphp = servidor + ":8081/TaxiExpress/ws/cliente/";


    public final static String servidorarchivos = servidor + "/BL/";

    public final static String estado = servidornodejs + "w_recuperar_estado";
    public final static String updateTokenAndroid = servidornodejs + "w_token_android";

    public final static String ws_recuperar_password = servidorphp + "ws_recuperar_password.php";
    public final static String ws_lista_beneficio = servidorphp + "ws_lista_beneficio.php";
    public final static String ws_lista_zonas = servidorphp + "ws_lista_zonas.php";
    public final static String ws_canjear_beneficios = servidorphp + "ws_canjear_beneficio.php";
    public final static String ws_registro = servidorphp + "ws_registro.php";
    public final static String ws_lista_reservas = servidorphp + "ws_lista_reservas.php";
    public final static String ws_historial_servicios = servidorphp + "ws_historial_servicios.php";
    public final static String ws_login = servidorphp + "ws_login.php";
    public final static String ws_consultar_tarifa = servidorphp + "ws_consultar_tarifa.php";
    public final static String ws_calificacion = servidorphp + "ws_calificacion.php";
    public final static String w_vales=servidorphp+"ws_listar_vales.php";
    public final static String ws_save_vale_general=servidornodejs+"w_save_vale_general";

    public final static String ws_tipo_servicio = servidorphp + "ws_tipo_servicio.php";
    public final static String ws_obtener_puntos = servidorphp + "ws_obtener_puntos.php";

    //public final static String ws_agregar_reserva=servidornodejs+"w_agregar_reserva";
    public final static String ws_agregar_reserva = servidorphp + "ws_agregar_reserva.php";
    public final static String ws_guardar_datos_seguridad = servidorphp + "ws_guardar_datos_seguridad.php";
    public final static String ws_obtener_datos_seguridad = servidorphp + "ws_obtener_datos_seguridad.php";

    public final static String ws_agregar_ruta_frecuente = servidorphp + "ws_agregar_ruta_frecuente.php";
    public final static String ws_mostrar_ruta_frecuente = servidorphp + "ws_mostrar_ruta_frecuente.php";
    public final static String ws_eliminar_ruta_frecuente = servidorphp + "ws_eliminar_ruta_frecuente.php";


    //Url Apis
    public final static String here_geocode = "http://geocoder.cit.api.here.com/6.2/geocode.json";
    public final static String foursquare_geocode = "https://api.foursquare.com/v2/venues/search";
    public final static String google_geocode = "http://maps.google.com/maps/api/geocode/json";
    public final static String google_inversegeocode = "https://maps.googleapis.com/maps/api/geocode/json";
    public final static String google_distancematrix = "https://maps.googleapis.com/maps/api/distancematrix/json";


}
