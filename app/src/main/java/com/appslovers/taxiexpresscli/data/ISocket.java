package com.appslovers.taxiexpresscli.data;

import pe.com.visanet.lib.VisaNetPaymentInfo;

/**
 * Created by javierquiroz on 3/06/16.
 */
public interface  ISocket
{
    void connect(String s);
    void reconnect(String s);
    void disconnect(String s);
    void connect_error(String s);
    void connect_tiemout(String s);

    void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns);
    void ConductorSeleccionado(JsonPack.Servicio s);
    void ServicioCaducado(String idservicio);
    void ServicioCancelado(JsonPack.CancelarServicio s);
    void SolicitudAceptar(JsonPack.ConductorLista solicitud);
    void SolicitudApis(JsonPack.ApiKeys s);
    void ConductorLLego(String solicitud);
    void InicioServicio(String solicitud);
    void FinalizarServicioCliente(JsonPack.RespFinalizarServ s);

    //void finalizarConTarjeta(VisaNetPaymentInfo paymentInfo);
    void enviarSolicitudPago(String s);

    void RefreshCliente(String s);
    void ConductorAsignadoaReserva(String s);
}