package com.appslovers.taxiexpresscli.data;

/**
 * Created by javierquiroz on 23/06/16.
 */
public enum InnerActions {

    notact_updateestado
    ,notact_salirservicio
    ,notact_encendernode
    ,notact_detenernode
    ,notact_cancelar_detenernode
    ,notact_cancelar_encendernode
    ,notact_detenerservicio
    ,notact_aceptarSolicitud
    ,notact_rechazarSolicitud
    ,notact_showSolicitud
    ,push_conductorllego
    ,push_incioservicio
    ,push_finalizarserviciocliente
    ,notact_cancelarservicio
    ,notif_solicitudpago
}
