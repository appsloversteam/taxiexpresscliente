package com.appslovers.taxiexpresscli.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 9/06/16.
 */
public class SPUser {
    private static final String RESPONSELOGIN = "RESPONSELOGIN";
    private static final String TIPOSSERIVCIO = "TIPOS";
    private static final String RECIENTES = "RECIENTES";
    private static final String EXPRESO = "EXPRESO";

    private static final String SESIONES = "SESIONES";
    private static final String ICONO_HOMESCREEN = "ICONO_HOME";
    private static final String TOKEN = "TOKEN";
    private static final String DATE="DATE";
    private static final String ULT_ID="ID";

    private static final String SERVICIO="SERVICIO";

    private static final String CODIGO_VALE="TICKET_CODE";

    private static final String TARJETA="TARJETA";

    public void setToken(String token) {
        SP.edit().putString(TOKEN, token).apply();
    }

    public String getToken() {
        return SP.getString(TOKEN, null);
    }

    SharedPreferences SP;

    public SPUser(Context c) {
        if (c != null)
            SP = c.getSharedPreferences(SPUser.class.getSimpleName(), Context.MODE_MULTI_PROCESS);
    }

    public static void logout(Context c) {
        c.getSharedPreferences(SPUser.class.getSimpleName(), Context.MODE_MULTI_PROCESS).edit()
                .remove(RESPONSELOGIN)
                .remove(TIPOSSERIVCIO)
                .remove(RECIENTES)
                .remove(EXPRESO)
                //.clear()
                .apply();
    }

    public void setTiposServico(ArrayList<JsonPack.TipoServicio> tipos) {
        SP.edit().putString(TIPOSSERIVCIO, new Gson().toJson(tipos)).apply();
    }

    public ArrayList<JsonPack.TipoServicio> getTiposServicio() {
        ArrayList<JsonPack.TipoServicio> lista = null;

        String s = SP.getString(TIPOSSERIVCIO, null);
        if (s != null) {
            lista = new Gson().fromJson(s, new TypeToken<ArrayList<JsonPack.TipoServicio>>() {
            }.getType());
        }

        return lista;
    }

    public void setResponseLogin(JsonPack.ResponseLogin rl) {
        SP.edit().putString(RESPONSELOGIN, new Gson().toJson(rl)).apply();
    }


    public JsonPack.ResponseLogin getResponseLogin() {
        JsonPack.ResponseLogin rl = null;

        String s = SP.getString(RESPONSELOGIN, null);
        //Log.v("getResponseLogin",">"+s);
        if (s != null) {
            rl = new Gson().fromJson(s, JsonPack.ResponseLogin.class);
        }

        return rl;
    }

    public void addReciente(JsonPack.DirLug dl) {
        if (dl != null) {

            ArrayList<JsonPack.DirLug> dls = getRecientes();

            dls.add(dl);

            SP.edit().putString(RECIENTES, new Gson().toJson(dls)).apply();
            Log.d("addReciente", ">" + dl.direccion + " , " + dl.name);
        } else {
            Log.e("addReciente", ">" + dl.direccion + " , " + dl.name);
        }
    }

    public ArrayList<JsonPack.DirLug> getRecientes() {
        ArrayList<JsonPack.DirLug> lista = new ArrayList<>();

        String s = SP.getString(RECIENTES, null);
        if (s != null) {
            lista = new Gson().fromJson(s, new TypeToken<ArrayList<JsonPack.DirLug>>() {
            }.getType());
            Log.e("getRecientes", ">" + lista.size());
        } else {
            Log.e("getRecientes", "null");
        }

        return lista;
    }

    public void setExpreso(boolean b) {
        SP.edit().putBoolean(EXPRESO, b).apply();
    }

    public boolean getExpreso() {
        return SP.getBoolean(EXPRESO, true);
    }

    public JsonPack.MisSesiones getUsuarios() {
        String s = SP.getString(SESIONES, null);
        if (s != null) {
            return new Gson().fromJson(s, JsonPack.MisSesiones.class);
        }
        return new JsonPack.MisSesiones();
    }

    public void setUsuarios(JsonPack.MisSesiones ms) {
        if (ms != null) {
            SP.edit().putString(SESIONES, new Gson().toJson(ms)).apply();
        }
    }

    public void setDate(String date){
        SP.edit().putString(DATE,date).apply();
    }

    public String getDate(){
        return SP.getString(DATE,null);
    }

    public boolean checkInstalationShotCut() {
        boolean v = SP.getBoolean(ICONO_HOMESCREEN, false);
        if (!v) {
            SP.edit().putBoolean(ICONO_HOMESCREEN, true).apply();

        }

        return v;
    }


    public void setLastId(String lastId) {
        SP.edit().putString(ULT_ID,lastId).apply();
    }

    public String getLastId(){
        return SP.getString(ULT_ID,null);
    }


    public void setServicio(JsonPack.Servicio servicio){
        if (servicio!=null){
            SP.edit().putString(SERVICIO, new Gson().toJson(servicio)).apply();
        }
    }

    public JsonPack.Servicio getServicio(){
        return new Gson().fromJson(SP.getString(SERVICIO, null),JsonPack.Servicio.class);
    }

    public void setCodigoVale(String codigoVale){
        SP.edit().putString(CODIGO_VALE,codigoVale).apply();
    }

    public String getCodigoVale(){
        return SP.getString(CODIGO_VALE,null);
    }


    public void setTarjeta(JsonPack.CreditCard card) {
        SP.edit().putString(TARJETA,new Gson().toJson(card)).apply();
    }

    public JsonPack.CreditCard getCreditCard(){
        return new Gson().fromJson(SP.getString(TARJETA,null), JsonPack.CreditCard.class);
    }
}
