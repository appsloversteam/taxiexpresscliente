package com.appslovers.taxiexpresscli.data;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.io.Serializable;
import java.util.ArrayList;

import pe.com.visanet.lib.VisaNetPaymentInfo;

/**
 * Created by javierquiroz on 30/05/16.
 */
public class JsonPack {

    public static class ModCount {
        public String mMod;
        public String mTot;

    }

    public static class FbResponse implements Serializable {
        public String id;
        public String name;
        public String last_name;
        public String email;
        public String gender;
        public String birthday;
    }

    public static class MisSesiones {
        public ArrayList<String> usuarios;

        public String[] getUsurarios() {
            if (usuarios != null)
                return usuarios.toArray(new String[usuarios.size()]);
            else
                return new String[]{};
        }
    }

    public static class RespRutas {
        public boolean status;
        public ArrayList<RutaFrec> result;
    }

    public static class RutaFrec {
        public String cliente_id;
        public String ruta_nombre;
        public double latitud_origen;
        public String nombre_origen;
        public String nombre_destino;
        public double longitud_origen;
        public double latitud_destino;
        public double longitud_destino;
        public String id_origen;
        public String id_destino;
        public String tiposervicio;
        public String origen;
        public String destino;
        public String ref_origen;
        public String ref_destino;
        public String tarifa;
        public String rutasid;
    }

    public static class ServicioHist {
        public String origen;
        public String direccion_origen;
        public String destino;
        public String direccion_destino;
        public String fecha_inicio;
        public String fecha_fin;
        public String tarifa;
        public String hora;
        public String fecha;
    }

    public static class ResponseHistorial {
        public boolean status;
        public ArrayList<ServicioHist> result;
    }

    public static class NuevaReserva {
        public String cliente_id;
        public String fecha;
        public String id_origen;
        public String origen;
        public double latitud_origen;
        public double longitud_origen;
        public String direccion_origen;
        public String referencia_origen;
        public String id_destino;
        public String destino;
        public double latitud_destino;
        public double longitud_destino;
        public String direccion_destino;
        public String referencia_destino;
        public String tiposervicio_id;
        public String tipopago_id;
        public String tarifa;
    }

    public static class RespReservas {
        public boolean status;
        public int hora_anticipada;
        public ArrayList<Reserva> result;
    }

    public static class Reserva {
        public String cliente;
        public String cliente_nombre;
        public String reserva_id;
        public String cliente_id;
        public String conductor_id;
        public String reserva_fecha;
        public String origen_id;
        public String lat_origen;
        public String lon_origen;
        public String dir_origen;
        public String destino_id;
        public String lat_destino;
        public String lon_destino;
        public String dir_destino;
        public String tiposervicio_id;
        public String tipo_pago;
        public String tarifa;
        public String estado;
        public String fecha;
        public String hora;
        public String Fecha;
        public String Hora;
    }

    public static class Direccion {

    }

    public static class ResponsePromociones {
        public boolean status;
        public ArrayList<Promocion> result;
    }

    public static class Promocion {
        public String promo_id;
        public String promo_imagen;
        public int promo_puntos;
        public String promo_nombre;
        public String promo_finicio;
        public String promo_fvigencia;
        public String promo_cantidad;
        public String promo_estado;
        public String promo_tipo;
        public int promo_stock;
    }

    public static class TipoServicio {
        public int id;
        public String nombre;
    }

    public static class RespPuntos {
        public boolean status;
        public int puntos;
    }

    public static class ResponseLogin extends ResponseGeneric {
        public String nombre;
        public String apellido;
        public float calificacion;
        public String fecha_brevete;
        public String cliente_id;
        public String unidad_id;
        public String celular;
        public String empresa_id;
        public int tipo_id;
        public String tipo_nombre;
        public int conductor_status;
        public String email;

        /*API KEYS*/
        public String foursquare_key;
        public String foursquare_secret;
        public String google_android;
        public String here_key;
        public String here_secret;

    }

    public static class ResponseRegister {
        public boolean success;
        public String mensaje;
        public String cliente_id;
        public String nombre;
        public String apellido;
        public String correo;
        public String celular;
        public String calificacion;
    }

    public static class ResponseGeneric {
        public boolean status;
        public String mensaje;
    }

    public static class Testnode {
        public String nombre;
    }

    public static class Zona implements Serializable {
        public String id;
        public String nombre;
        public Coordenada centro;
        public ArrayList<Coordenada> coordenadas;

        public LatLng ltlncentro;
        public ArrayList<LatLng> ltlns;

        public ArrayList<LatLng> getLtns() {
            if (ltlns == null)
                ltlns = new ArrayList<>();
            return ltlns;
        }
    }

    public static class Coordenada implements Serializable {
        public double latitud;
        public double longitud;

        public Coordenada parseCoordinates(String coordString){
            coordString=coordString.replace('(',' ').replace(')',' ').trim();
            String[] latLng=coordString.split(",");
            this.latitud=Double.parseDouble(latLng[0]);
            this.longitud=Double.parseDouble(latLng[1]);
            return this;
        }

        public LatLng generar() {
            return new LatLng(latitud, longitud);
        }

        public boolean isInsidePolygon(ArrayList<LatLng> bounds, LatLng point) {
            return PolyUtil.containsLocation(point, bounds, true);
        }
    }

    public static class ResponseTrarifa {
        public float tarifa;
        public boolean status;
    }

    public interface IDirLug {
        public ArrayList<DirLug> getDirLug();
    }

    public static class DirLug implements Serializable {
        public int from;
        public double lat;
        public double lon;
        public String name;
        public String direccion;
        public String iconURL;
    }

    //----------------------------- json direcciones here

    public static class Here implements Serializable, IDirLug {
        public HereResponse Response;
        ArrayList<DirLug> dirslugs;

        @Override
        public ArrayList<DirLug> getDirLug() {

            if (dirslugs == null)
                dirslugs = new ArrayList<>();

            if (dirslugs.size() == 0) {

                if (Response.View.size() > 0) {
                    for (HereResult hr : Response.View.get(0).Result) {
                        DirLug dl = new DirLug();
                        dl.from = 1;
                        //dl.name = hr.Location.Address.Label;
                        dl.name = hr.Location.Address.Street;
                        dl.lat = hr.Location.NavigationPosition.get(0).Latitude;
                        dl.lon = hr.Location.NavigationPosition.get(0).Longitude;
                        dirslugs.add(dl);

                    }
                }
            }
            return dirslugs;
        }
    }

    public static class HereResponse implements Serializable {
        public HereMetaInfo MetaInfo;
        public ArrayList<HereView> View;

    }

    public static class HereMetaInfo implements Serializable {
        public String Timestamp;
    }

    //item
    public static class HereView implements Serializable {
        public String _type;
        public int ViewId;
        public ArrayList<HereResult> Result;
    }

    public static class HereResult implements Serializable {
        public float Relevance;
        public float Distance;
        public String MatchLevel;
        public HereLocation Location;

    }

    public static class HereLocation implements Serializable {
        public String LocationId;
        public String LocationType;
        public ArrayList<HereNavigationPosition> NavigationPosition;
        public HereAddress Address;

    }

    public static class HereNavigationPosition {
        public double Latitude;
        public double Longitude;
    }

    public static class HereAddress {
        public String Label;
        public String Country;
        public String State;
        public String County;
        public String City;
        public String Street;
        public String HouseNumber;
        public String PostalCode;

    }

    public static class SolicitudPago{
        public CostosAdicionales costosAdicionales;
        public String clienteId;
        public String tarifa;
    }

    public static class CostosAdicionales implements Serializable {
        public float Paradas;
        public float Parqueos;
        public float Peajes;
        public float Espera;
        public String idServicio;


        public float getTotal() {
            return Paradas + Parqueos + Peajes + Espera;
        }
    }


    //---------------- json google reverse geocode

    /*
    public static class GoogleRevGeocode implements Serializable
    {

        public ArrayList<GoogleRevGeocodeResult> results;
        public String status;
    }

    public static class GoogleRevGeocodeResult
    {
        public ArrayList<GoogleRevGeocodeAddressComponent> address_components;
        public String formatted_address;
        public GoogleRevGeocodeGeometry geometry;
        public ArrayList<String> types;
    }

    public static class GoogleRevGeocodeAddressComponent
    {
        public static String long_name;
        public static String short_name;
        public static  ArrayList<String> types;
    }

    public static class GoogleRevGeocodeGeometry
    {
        public GoogleRevGeocodeLocation location;
    }
    public static class GoogleRevGeocodeLocation
    {
        public double lat;
        public double lon;
    }*/

    //----------------------------- json direcciones Google


    public static class GoogleGeoCode implements Serializable, IDirLug {

        public String status;
        public ArrayList<GoogleGeoCodeResult> results;
        ArrayList<DirLug> dirslugs;

        @Override
        public ArrayList<DirLug> getDirLug() {
            if (dirslugs == null)
                dirslugs = new ArrayList<>();

            if (dirslugs.size() == 0) {
                for (GoogleGeoCodeResult g : results) {
                    DirLug dl = new DirLug();
                    dl.from = 2;

                    if (g.address_components != null) {

                        String snum = "";
                        String snom = "";
                        String urbdis = "";
                        for (JsonPack.GoogleGeoCodeAddressComponent ac : g.address_components) {
                            if (ac.types != null) {
                                if (ac.types.size() > 0) {
                                    for (String tipo : ac.types) {

                                        if (tipo.contains("street_number")) {
                                            snum = ac.short_name;
                                        }

                                        if (ac.types.get(0).contains("route")) {
                                            snom = ac.long_name;
                                        }

                                        if (ac.types.get(0).contains("locality")) {
                                            urbdis = ac.short_name;
                                        }

                                    }

                                }
                            }

                        }

                        dl.name = (snom + " " + snum + " " + urbdis).trim();
                    } else {
                        dl.name = g.formatted_address;
                    }


                    dl.lat = g.geometry.location.lat;
                    dl.lon = g.geometry.location.lng;
                    dirslugs.add(dl);
                }
            }
            return dirslugs;
        }
    }

    public static class GoogleGeoCodeResult implements Serializable {

        public ArrayList<GoogleGeoCodeAddressComponent> address_components;
        public String formatted_address;
        public GoogleGeoCodeGeometry geometry;

    }

    public static class GoogleGeoCodeComponents {
        public String long_name;
        public String short_name;
        public ArrayList<String> types;
    }

    public static class GoogleGeoCodeGeometry implements Serializable {
        public GoogleGeoCodeLocation location;
    }

    public static class GoogleGeoCodeLocation implements Serializable {
        public double lat;
        public double lng;
        public ArrayList<GoogleGeoCodeAddressComponent> address_components;
    }

    public static class GoogleGeoCodeAddressComponent {
        public String long_name;
        public String short_name;
        public ArrayList<String> types;

    }

    //---- json Google distance matrix

    public static class GoogleDistanceMatrixResult {
        public ArrayList<String> destination_addresses;
        public ArrayList<String> origin_addresses;
        public ArrayList<GoogleDmRow> rows;
        public String status;
    }

    public static class GoogleDmRow {
        public ArrayList<GoogleDmData> elements;
    }

    public static class GoogleDmData {
        public GoogleDitance distance;
        public GoogleDuration duration;
    }

    public static class GoogleDitance {
        public String text;
        public long value;
    }

    public static class GoogleDuration {
        public String text;
        public long value;
    }

    //------------------ json foursaquere

/*
    public static class Foursquare implements Serializable, IDirLug {
        public FoursquareMeta meta;
        public FoursquareResponse response;
        ArrayList<DirLug> dirslugs;

        @Override
        public ArrayList<DirLug> getDirLug() {

            if (dirslugs == null)
                dirslugs = new ArrayList<>();

            if (dirslugs.size() == 0) {
                for (FoursquareVenue v : response.venues) {
                    if (v.name != null && v.location.address != null) {
                        try {
                            DirLug dl = new DirLug();
                            dl.from = 3;
                            dl.name = v.name;
                            //dl.direccion = v.location.address;//v.location.formattedAddress.get(0);
                            dl.direccion = v.location.formattedAddress.get(0);
                            dl.lat = v.location.lat;
                            dl.lon = v.location.lng;
                            dl.iconURL = v.categories.get(0).icon.prefix + "32" + v.categories.get(0).icon.suffix;
                            dirslugs.add(dl);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return dirslugs;
        }
    }
    */

    public static class Foursquare implements Serializable, IDirLug {
        public FoursquareMeta meta;
        public FoursquareResponse response;
        ArrayList dirslugs;

        @Override
        public ArrayList getDirLug() {
            if (dirslugs == null) dirslugs = new ArrayList<>();
            if (dirslugs.size() == 0) {
                for (FoursquareVenue v : response.venues) {
                    if (v.location.address != null && v.location.city != null) {
                        DirLug dl = new DirLug();
                        dl.from = 3;
                        dl.name = v.name;
                        dl.direccion = v.location.address + ", " + v.location.city;
                        dl.lat = v.location.lat;
                        dl.lon = v.location.lng;

                        if (!v.categories.isEmpty())
                            dl.iconURL = v.categories.get(0).icon.prefix + "32" + v.categories.get(0).icon.suffix;

                        dirslugs.add(dl);
                    }
                }
            }

            return dirslugs;
        }
    }

    public static class FoursquareResponse implements Serializable {
        public ArrayList<FoursquareVenue> venues;
    }

    public static class FoursquareMeta implements Serializable {
        public int code;
        public String requestId;
    }

    public static class FoursquareVenue implements Serializable {
        public String id;
        public String name;

        public FoursquareLocation location;
        public String url;
        public ArrayList<FoursquareCategory> categories;
        public boolean verified;
    }

    public static class FoursquareLocation implements Serializable {
        public String address;
        public String crossStreet;
        public double lat;
        public double lng;
        public String postalCode;
        public String cc;
        public String city;
        public String state;
        public String country;
        ArrayList<String> formattedAddress;

    }

    public static class FoursquareCategory {
        public String id;
        public String name;
        public FoursquareIcon icon;
    }

    public static class FoursquareIcon {
        public String prefix;
        public String suffix;
    }

    //------------------ Node JS

    public static class Alerta {
        public String idConductor;
        public double latitud;
        public double longitud;
        public String nombre;
        public String apellido;
    }


    public static class RespFinalizarServ implements Serializable {
        public String FechaFin;
        public float Paradas;
        public float Parqueos;
        public float Peajes;
        public float Espera;
        public float Tarifa;
        public String TipoPago;
        public String Servicioid;
    }

    public static class SolicitudRespuesta {
        public String servicio_id;
        public String conductor_id;
        public String socketCli;
    }

    public static class NuevaSolicitud implements Serializable {

        public String IdCliente;
        public double latOrig;
        public double latDest;
        public double lngOrig;
        public double lngDest;
        public String Origen;
        public String OrigenNombre;
        public String DestinoNombre;
        public String Destino;
        public String dirOrigen;
        public String dirDestino;
        public String refOrigen;
        public String refDestino;
        public String tipoServicio;
        public String ServicioId;
        public float tarifa;
        public String socketCliente;
        public int Envio;

    }

    public static class ConductorLista implements Serializable {
        public String IdConductor;
        public String NombreConductor;
        public String ApelliConductor;
        public String Placa;
        public String Color;
        public String Marca;
        public String Modelo;
        public String Viaje;
        public double Latitud;
        public double Longitud;
        public String Foto;
        public float Calificacion;
        public String ServicioId;
        public String Caracteristicas;
    }

    public static class Conductor implements Serializable {
        public String conductor_id;
        public String unidad_id;
        public String conductor_nombre;
        public String conductor_apellido;
        public String conductor_correo;
        public String conductor_password;
        public String conductor_dni;
        public String conductor_celular;
        public String conductor_ciudad;
        public String conductor_distrito;
        public String conductor_licencia;
        public String conductor_catlicencia;
        public String conductor_fechabrevete;
        public String conductor_foto;
        public String conductor_status;
        public float conductor_calificacion;
        public String fb_id;
        public double conductor_lat;
        public double conductor_lng;
        public String conductor_socket;
        public String conductor_estado;
        public long conductor_puntos;
        public int conductor_vista;
        public int conductor_viajes;
        public int conductor_calificacion_flag;
    }

    public static class AlertaWeb {
        public String tipo;
        public String codigo;
        public double latitud;
        public double longitud;
        public int flag;
    }

    public static class RespEstadoCliente implements Serializable {
        public JsonPack.Servicio Servicio;
        //public Cliente Cliente;
        public Conductor Conductor;
        public Vehiculo Unidad;
        public int CalificacionCliente;
        public int TipoTarifa;//1 es zona 2 es kilometraje

        /*API KEYS*/
        public String foursquare_key;
        public String foursquare_secret;
        public String google_android;
        public String here_key;
        public String here_secret;

        public int getEstado() {
            if (Servicio != null)
                return Servicio.getEstado();
            else
                return 0;
        }

    }

    public static class Vehiculo implements Serializable {


        public String unidad_id;
        public String tiposervicio_id;
        public String unidad_caracteristicas;
        public String unidad_alias;
        public String unidad_status;
        public String unidad_placa;
        public String unidad_marca;
        public String unidad_modelo;
        public String unidad_color;
        public String unidad_fabricacion;
        public String unidad_fechasoat;
        public String unidad_fecharevtec;
        public String unidad_foto;
        public String unidad_asignada;

    }

    public static class Cliente implements Serializable {
        public static String cliente_id;
        public static String cliente_nombre;
        public static String cliente_correo;
        public static String cliente_celular;
        public static String empresa_id;
        public static String tipocliente_id;
        public static String cliente_viajes;
    }


    public static class Solicitud implements Serializable {
        public String idCliente;

        public String tiposervicio;
        public String tipopago;


        public String origen;
        public String origenombre;
        public String direcorigen;
        public String referenciaorigen;
        public double latorig;
        public double lngorig;

        public String tarifa;

        public String destino;
        public String destinonombre;
        public String direcdestino;
        public String referenciadestino;
        public double latdes;
        public double lngdes;

        public String codigoVale;

        public String kilometraje;//si es 1 es kilometraje si es 2 es por zonas

    }

    public static class Servicio implements Serializable {
        public String IdServicio;
        public String IdCliente;
        public String IdConductor;
        public String OrigenId;
        public String OrigenNombre;
        public String OrigenDireccion;
        public String OrigenReferencia;
        public double OrigenLat;
        public double OrigenLng;
        public String DestinoId;
        public String DestinoNombre;
        public String DestinoDireccion;
        public String DestinoReferencia;
        public double DestinoLat;
        public double DestinoLng;
        public String TipoPago;
        public String TipoServicio;
        public String Tarifa;
        public String Estado;
        public String FechaRecojo;
        public String HoraInicio;
        public String codigoVale;


        public Solicitud getSolicitud() {


            Solicitud s = new Solicitud();
            s.idCliente = this.IdCliente;

            s.origen = this.OrigenId;
            s.latorig = this.OrigenLat;
            s.lngorig = this.OrigenLng;
            s.origenombre = this.OrigenNombre;
            s.direcorigen = this.OrigenDireccion;
            s.referenciaorigen = this.OrigenReferencia;
            s.codigoVale=this.codigoVale;

            if (this.DestinoId == null) {
                s.destino = "0";
            } else {
                s.destino = this.DestinoId;
            }

            s.latdes = this.DestinoLat;
            s.lngdes = this.DestinoLng;
            s.destinonombre = this.DestinoNombre;
            s.direcdestino = this.DestinoDireccion;
            s.referenciadestino = this.DestinoReferencia;

            s.tipopago = this.TipoPago;
            s.tiposervicio = this.TipoServicio;
            s.tarifa = this.Tarifa;


            return s;
        }

        public int getEstado() {
            if (Estado != null)
                return Integer.parseInt(Estado);
            else
                return 0;
        }
    }


    public static class AceptaTaxi {
        public String nombre;
        public String celular;
        public float calificacion;
        public float tarifa;
        public String origen;
        public String destino;
        public double latitud;
        public double longitud;
        public String socketCliente;
    }

    public static class SolicitudResponse {
        public String Servicio;
        public ArrayList<Integer> idsCond;
    }

    public static class SelectionarConductor {
        public String idservicio;
        public ArrayList<Integer> idarray;
        public String idconductor;
        public String tipo;
        public String codigoVale;
    }

    public static class LlegoRecojo {
        public String IdConductor;
        public String IdServicio;
    }

    public static class UpdateClienteSocket {
        public String idCliente;
        public String socketCliente;
        public String token;//0 para android
        public String tipo;//2 para android
    }

    public static class ApiKeys {
        /*API KEYS*/
        public String foursquare_key;
        public String foursquare_secret;
        public String google_android;
        public String here_key;
        public String here_secret;
    }

    public static class UpdateGeoConductor {
        public String idConductor;
        public double latitud;
        public double longitud;
    }

    public static class respUpdateGeoConductor {
        public String idConductor;
        public double latitud;
        public double longitud;
        public String socket;
        public int stateConductor;
    }


    public static class RespRefreshConductor {
        public double LatCondutor;
        public double LngConductor;

        public LatLng getLL() {
            return new LatLng(LatCondutor, LngConductor);
        }

    }

    public static class RespConductorLlego {
        public String FechaRecojo;
    }

    public static class RespInicioServicio {
        public String FechaInicio;
    }

    public static class RespCancelarServicio {
        public String servicio_id;
        public String conductor_id;
    }

    public static class RespMiSeguridad {
        public boolean status;
        public String mensaje;
        public String id;
        public String nombre;
        public String correo;
        public String estado;
    }


    public static class CancelarServicio {
        public String ServicioId;
    }

    public static class ZonaSQL{
        public String lugares_id,lugar_descripcion,lugar_lat,lugar_lon,lugar_coordpolygono,lugar_fecharegistro,lugar_fechaupdate,lugar_estado,lugar_mod,lugar_update;
        public ArrayList<String> coordenadas;

    }

    public static class CoordinatesArray{
        public ArrayList<Coordenada> coordenadas;
    }

    public static class SynchronizeZonesResponse{
        public boolean status;
        public ArrayList<ZonaSQL> list;
        public String end_id, fecha;
    }

    public static class ResponseListaVales{
        public boolean status;
        public ArrayList<ValeElectronico> data;
    }

    public static class ValeElectronico{

        public String
                vale_id,
                vale_codigo,
                vale_tipo,
                empresa_id,
                vale_fechainicio,
                vale_monto,
                vale_creador,
                rol_id,
                vale_descripcion,
                vale_fechafinal,
                vale_estado,
                cliente_id;

    }

    public static class PaymentInfo{
        public VisaNetPaymentInfo info;
        public String idServicio;
    }




    public static class CreditCard{
        public String numeroTar,fechaVenc,cvv,nombreTarjetaHabiente, apeTarjetaHabiente,email;
    }
}
