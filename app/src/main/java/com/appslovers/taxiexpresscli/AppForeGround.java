package com.appslovers.taxiexpresscli;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.MyDataBase;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.util.Tools;
import com.appslovers.taxiexpresscli.data.Urls;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 20/05/16.
 */
public class AppForeGround extends MultiDexApplication {


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    //public MyHttpRequest MHTTPR;
    Picasso Pic;
    private MyDataBase db;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v("FINGER_POINT", Tools.getCertificateSHA1Fingerprint(getApplicationContext()) + ";" + getApplicationContext().getPackageName());
        Log.v("HASH_FACEBOOK", Tools.keyhashfacebook(getApplicationContext()));


        // MHTTPR=new MyHttpRequest();
        //registerActivityLifecycleCallbacks(MHTTPR);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            final Thread.UncaughtExceptionHandler oldHandler =
                    Thread.getDefaultUncaughtExceptionHandler();

            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                AppLogService.extractLogToFileAndWeb(true, getApplicationContext());
                if (oldHandler != null)
                    oldHandler.uncaughtException(thread, e); //Delegates to Android's error handling
                else
                    System.exit(2); //Prevents the service/app from freezing

                //android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);

            }
        });

        FirebaseInstanceId fii = FirebaseInstanceId.getInstance();
        if (fii != null) {

            SPUser sp = new SPUser(getApplicationContext());
            sp.setToken(fii.getToken());
            actualziarToken();
        }
    }

    public int flag_horaanticipada = 2;
    public int flag_califico = 0;
    public int flag_tipotarifa;

    public MyDataBase getDb() {
        if (db == null)
            db = new MyDataBase(getApplicationContext());
        return db;
    }

    private ArrayList<JsonPack.Zona> Zonas;

    public ArrayList<JsonPack.Zona> getZonas() {
        if (Zonas == null)
            Zonas = getDb().getZonas();

        return Zonas;
    }

    public Picasso getPicasso() {
        if (Pic == null)
            Pic = Picasso.with(getApplicationContext());
        return Pic;
    }


    public void actualziarToken() {
        SPUser sp = new SPUser(getApplicationContext());
        MyRequest MR = new MyRequest(Urls.updateTokenAndroid, MyRequest.HttpRequestType.POST) {
            @Override
            public void onSucces(ResponseWork rw, boolean isActive) {

            }

            @Override
            public void onFailedRequest(ResponseWork rw, boolean isActive) {

            }
        };

        if (sp.getResponseLogin() != null) {
            if (sp.getToken() != null) {
                Log.i("token", ">>>>>>>>>>>enviando>>>>>>>");

                MR.putParams("token", "" + sp.getToken())
                        .putParams("cliente_id", "" + sp.getResponseLogin().cliente_id)
                        .send();
            } else {
                Log.e("token", "es null");
            }

        } else {
            Log.e("response logitn", "es null");
        }
    }

    public void recuperarContrasena(String email) {

        new MyRequest<JsonPack.ResponseGeneric>(Urls.ws_recuperar_password, MyRequest.HttpRequestType.POST) {

            @Override
            public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseGeneric object) {


                if (object != null) {

                    /*
                    if(object.status) {


                        //Toast.makeText(getActivity(),object.mensaje,Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        //Toast.makeText(getActivity(),object.mensaje,Toast.LENGTH_SHORT).show();
                    }*/
                }

            }


        }
                .putParams("correo", email)
                .send();
    }


}
