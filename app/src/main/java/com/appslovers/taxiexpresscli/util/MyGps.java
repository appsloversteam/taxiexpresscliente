package com.appslovers.taxiexpresscli.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.appslovers.taxiexpresscli.data.ISetActivityResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by javierquiroz on 28/12/15.
 */
public class MyGps<A extends Activity & ISetActivityResult> implements LocationListener {


    //& MyGps.IOnActivityResult

    public enum TypeGPS {

        GOOGLEAPI,GPSDEP;
    }


    public interface  IMyGps
    {
        void onLocation(Location l);
        void onOldLocation(Location l);
        void onEstaApagado();
        void onFueApagado();
    }






    //-----------------------------
    //--- OLD API
    public Location lastKnownLocation;
    boolean flagSwitch= true;
    LocationManager locationManager;
    IMyGps IMGPS;

    //------------------------------
    //------ NEW API

        public static final int REQUEST_CHECK_SETTINGS_GPS = 12345;
        GoogleApiClient mGoogleApiClient;
        LocationRequest mLocationRequest;
        LocationSettingsRequest mLocationSettingsRequest;

    //------------------------------


    Activity myContext;
    Context CONTEXT;
    TypeGPS T;




    public MyGps(A c)
    {
        MyGpsConstructor(c,null, TypeGPS.GOOGLEAPI,null);
    }

    public MyGps(Context c, IMyGps imgps)
    {

        MyGpsConstructor(null,imgps, TypeGPS.GOOGLEAPI,c);
    }

    public MyGps(A c, IMyGps imgps)
    {

        MyGpsConstructor(c,imgps, TypeGPS.GOOGLEAPI,null);
    }

    public MyGps(IMyGps imgps)
    {

        MyGpsConstructor(null,imgps, TypeGPS.GOOGLEAPI,null);
    }

    public MyGps(IMyGps imgps, TypeGPS tipo)
    {

        MyGpsConstructor(null,imgps,tipo,null);
    }

    public MyGps(A c, IMyGps imgps, TypeGPS tipo)
    {
        MyGpsConstructor(c,imgps,tipo,null);

    }


    public void MyGpsConstructor(A c , IMyGps imgps,TypeGPS tipo,Context cc)
    {
        CONTEXT=cc;
        T=tipo;

        IMGPS=imgps;//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);


        if(c!=null)
        {
            myContext=c;
            c.setMyInterface(this);
            locationManager= (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        }
        else
        {
            locationManager= (LocationManager) CONTEXT.getSystemService(Context.LOCATION_SERVICE);
        }



        //------ OLD API

        //-----------------------
        //------- NEW API

        if(T.equals(TypeGPS.GOOGLEAPI))
        {
            long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
            long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

            mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


            if(c!=null)
            {
                CONTEXT=c.getApplicationContext();
            }

            mGoogleApiClient = new GoogleApiClient.Builder(CONTEXT)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                            Log.d(MyGps.class.getSimpleName(), "onConnected");

                            if(IMGPS!=null) {
                                Location l= LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                                if(l!=null)
                                    IMGPS.onOldLocation(l);
                            }


                            requestGoogleApi();

                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                            Log.d(MyGps.class.getSimpleName(), "onConectionSuspendes>"+i);

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d(MyGps.class.getSimpleName(), "onConnectionFailed>");

                        }
                    })
                    .addApi(LocationServices.API)
                    .build();
        }


    }


    public void  requestGoogleApi()
    {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                mGoogleApiClient,
                                mLocationRequest,
                                OLCHANGED
                        ).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status2) {

                            }
                        });

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:


                        if(myContext!=null) {
                            try {
                                status.startResolutionForResult(myContext, REQUEST_CHECK_SETTINGS_GPS);
                            } catch (IntentSender.SendIntentException e) {
                                Log.e(e.getClass().getSimpleName(), ">" + e.getMessage());
                            }
                        }
                        else
                        {
                            Log.e(MyGps.class.getSimpleName(),"GPS Activity es null");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:


                        break;
                }

            }
        });

    }






    public void setIMyGps(IMyGps imgps)
    {
        IMGPS=imgps;
    }

    public void setSwitch(boolean b)
    {
        flagSwitch=b;
    }

    public void turnOn(boolean withGps) {


        try {
            locationManager.addGpsStatusListener(GPSSL);
        } catch (SecurityException e) {

            Log.e(SecurityException.class.getSimpleName(),">"+e.getMessage());
        }

        if(T.equals(TypeGPS.GOOGLEAPI))
        {
            //if(!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();

              /*
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });*/

        }
        else if(T.equals(TypeGPS.GPSDEP))
        {
            String locationProvider = LocationManager.NETWORK_PROVIDER;

            if (withGps)
                locationProvider = LocationManager.GPS_PROVIDER;

            try {
                locationManager.requestLocationUpdates(locationProvider, 0, 0, this);
            } catch (SecurityException e)
            {
                Log.e(SecurityException.class.getSimpleName(),">"+e.getMessage());
            }


            try {
                lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
            }catch (SecurityException e)
            {
                Log.e(SecurityException.class.getSimpleName(),">"+e.getMessage());
            }
            if(lastKnownLocation!=null)
            {
                if(flagSwitch)
                {
                    if(IMGPS!=null) {

                        if(lastKnownLocation!=null)
                        IMGPS.onOldLocation(lastKnownLocation);
                    }
                }

            }

        }




    }


    com.google.android.gms.location.LocationListener OLCHANGED=new com.google.android.gms.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {


            if(IMGPS!=null) {
                if(location!=null) {

                    lastKnownLocation=location;
                    IMGPS.onLocation(location);
                }
            }

        }
    };
    public void turnOff()
    {

        locationManager.removeGpsStatusListener(GPSSL);

        if(T.equals(TypeGPS.GOOGLEAPI))
        {
            //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, OLCHANGED);
            mGoogleApiClient.disconnect();
        }
        else if(T.equals(TypeGPS.GPSDEP))
        {
            try {
                locationManager.removeUpdates(this);
            }catch (SecurityException e)
            {
                Log.e(SecurityException.class.getSimpleName(),">"+e.getMessage());
            }

        }



    }
    @Override
    public void onLocationChanged(Location location) {

        if(location!=null) {

            lastKnownLocation=location;
            if(flagSwitch)
            {
                if(IMGPS!=null) {
                    if(location!=null)
                    IMGPS.onLocation(location);
                }
            }

        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

        Log.d("onStatusChanged", ">" + s + " , " + i);
    }

    @Override
    public void onProviderEnabled(String s) {

        Log.d("onProviderEnable", ">" + s);
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.d("onProviderDisabled", ">" + s); //caundo se apaga avisa al principio
        IMGPS.onEstaApagado();
    }

    /*
    LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    Criteria crit = new Criteria();
    crit.setAccuracy(Criteria.ACCURACY_COARSE);
    String provider = lm.getBestProvider(crit, true);
    Location loc = lm.getLastKnownLocation(provider);*/

    android.location.GpsStatus.Listener GPSSL=new android.location.GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            switch (event) {
                case GpsStatus.GPS_EVENT_STARTED:

                    //startForeground(CODE_FORE_GEOSERVICIO,Notificaciones.buildNotificationGeoService(ServicioGeoLocalizacion.this));
                   Log.d(MyGps.class.getSimpleName(), "GPS_STARTED");

                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:

                    Log.d(MyGps.class.getSimpleName(), "GPS_STARTED");

                    break;
                case GpsStatus.GPS_EVENT_STOPPED:

                    Log.d(MyGps.class.getSimpleName(), "GPS_STOPED"); //caundo se apaga
                    IMGPS.onFueApagado();

                    break;

                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:

                    Log.d(MyGps.class.getSimpleName(), "GPS_EVENT_SATELITES_STATUS");
                    //mLocMan.getGpsStatus(null).;
                    break;
            }
        }
    };


    public void onGetResult(int requestCode, int resultCode, Intent data) {


        switch (requestCode) {

            case MyGps.REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        requestGoogleApi();

                        break;
                    case Activity.RESULT_CANCELED:

                        break;
                }
                break;
        }
    }

}
