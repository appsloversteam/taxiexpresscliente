package com.appslovers.taxiexpresscli.util;

import com.appslovers.taxiexpresscli.data.ISocket;
import com.appslovers.taxiexpresscli.data.JsonPack;

/**
 * Created by javierquiroz on 30/06/16.
 */
public class SocketIoBridge implements ISocket {

    public static class AuxiliarBridge
    {
        public void onNuevasSolicitudes(int cantidad)
        {

        }

    }

    @Override
    public void ServicioCaducado(String idservicio) {

    }

    @Override
    public void ServicioCancelado(JsonPack.CancelarServicio s) {

    }


    @Override
    public void nuevaSolicitudConductor(JsonPack.NuevaSolicitud ns) {

    }

    @Override
    public void connect(String s) {

    }

    @Override
    public void reconnect(String s) {

    }

    @Override
    public void disconnect(String s) {

    }

    @Override
    public void connect_error(String s) {

    }

    @Override
    public void connect_tiemout(String s) {

    }

    @Override
    public void ConductorSeleccionado(JsonPack.Servicio s) {

    }

    @Override
    public void SolicitudAceptar(JsonPack.ConductorLista s) {

    }


    @Override
    public void ConductorLLego(String solicitud) {

    }

    @Override
    public void InicioServicio(String solicitud) {

    }

    @Override
    public void FinalizarServicioCliente(JsonPack.RespFinalizarServ s) {

    }

    @Override
    public void enviarSolicitudPago(String s) {

    }

    @Override
    public void RefreshCliente(String s) {

    }

    @Override
    public void ConductorAsignadoaReserva(String s) {

    }

    @Override
    public void SolicitudApis(JsonPack.ApiKeys s) {

    }
}