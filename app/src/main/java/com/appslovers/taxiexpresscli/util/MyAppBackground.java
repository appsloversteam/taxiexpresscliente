package com.appslovers.taxiexpresscli.util;

import android.app.Activity;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.body.RootBody;

/**
 * Created by javierquiroz on 30/06/16.
 */
public abstract class MyAppBackground {
    public AppBackground ABG;
    Activity A;

    public MyAppBackground() {

    }

    public MyAppBackground(Activity a) {
        A = a;
    }

    public abstract void todo(AppBackground app, int idop, Object obj);

    public void noConect() {

    }

    public void ActivityError() {

    }

    public MyAppBackground removeListener() {

        SB = null;
        if (A != null) {
            //Log.i("myappback","addlistener 2");
            if (A instanceof RootBody) {
                //Log.i("myappback","addlistener 3");
                RootBody rb = (RootBody) A;
                if (rb.AppConexion != null) {

                    //Log.i("myappback","addlistener 4");
                    if (rb.AppConexion.getApp() != null) {
                        //Log.d("addlistenr", "seteado");


                        ABG = rb.AppConexion.getApp();

                        rb.AppConexion.getApp().setSocketIoBridge(null);

                    } else {
                        //Log.e(this.getClass().getSimpleName(), "ActivityError");
                        noConect();
                    }
                } else {
                    //Log.e(this.getClass().getSimpleName(),"ActivityError");
                    noConect();
                }
            } else {
                //Log.e(this.getClass().getSimpleName(),"ActivityError");
                ActivityError();
            }
        } else {
            //Log.e(this.getClass().getSimpleName(),"Activity es null");
        }
        return this;
    }


    public SocketIoBridge.AuxiliarBridge AXB;

    public MyAppBackground addListner(SocketIoBridge.AuxiliarBridge axb) {
        //Log.i("myappback","addlistener 1");
        AXB = axb;


        if (A != null) {
            //Log.i("myappback","addlistener 2");
            if (A instanceof RootBody) {
                //Log.i("myappback","addlistener 3");
                RootBody rb = (RootBody) A;
                if (rb.AppConexion != null) {

                    //Log.i("myappback","addlistener 4");
                    if (rb.AppConexion.getApp() != null) {
                        //Log.d("addlistenr", "seteado");


                        ABG = rb.AppConexion.getApp();

                        rb.AppConexion.getApp().setAuxiliarBridge(AXB);

                    } else {
                        //Log.e(this.getClass().getSimpleName(), "ActivityError");
                        noConect();
                    }
                } else {
                    //Log.e(this.getClass().getSimpleName(),"ActivityError");
                    noConect();
                }
            } else {
                //Log.e(this.getClass().getSimpleName(),"ActivityError");
                ActivityError();
            }
        } else {
            //Log.e(this.getClass().getSimpleName(),"Activity es null");
        }
        //Log.i("myappback","addlistener 10");


        return this;
    }

    public SocketIoBridge SB;

    public MyAppBackground addListner(SocketIoBridge sb) {
        //Log.i("myappback","addlistener 1");
        SB = sb;


        if (A != null) {
            //Log.i("myappback","addlistener 2");
            if (A instanceof RootBody) {
                //Log.i("myappback","addlistener 3");
                RootBody rb = (RootBody) A;
                if (rb.AppConexion != null) {

                    //Log.i("myappback","addlistener 4");
                    if (rb.AppConexion.getApp() != null) {
                        //Log.d("addlistenr", "seteado");


                        ABG = rb.AppConexion.getApp();

                        rb.AppConexion.getApp().setSocketIoBridge(SB);

                    } else {
                        //Log.e(this.getClass().getSimpleName(), "ActivityError");
                        noConect();
                    }
                } else {
                    //Log.e(this.getClass().getSimpleName(),"ActivityError");
                    noConect();
                }
            } else {
                //Log.e(this.getClass().getSimpleName(),"ActivityError");
                ActivityError();
            }
        } else {
            //Log.e(this.getClass().getSimpleName(),"Activity es null");
        }
        //Log.i("myappback","addlistener 10");


        return this;
    }

    public void doit(Activity a, int idop) {
        //Log.i("myappback","addlistener 1");
        if (a instanceof RootBody) {
            //Log.i("myappback","addlistener 2");
            RootBody rb = (RootBody) a;
            if (rb.AppConexion != null) {
                //Log.i("myappback","addlistener 3");
                if (rb.AppConexion.getApp() != null) {
                    //Log.i("myappback","addlistener 4");
                    ABG = rb.AppConexion.getApp();
                    rb.AppConexion.getApp().setSocketIoBridge(SB);


                    todo(rb.AppConexion.getApp(), idop, null);
                } else {
                    //Log.e(this.getClass().getSimpleName(), "ActivityError");
                    noConect();
                }
            } else {
                //Log.e(this.getClass().getSimpleName(),"ActivityError");
                noConect();
            }
        } else {
            //Log.e(this.getClass().getSimpleName(),"ActivityError");
            ActivityError();
        }
    }

    public void doit(Activity a, int idop, Object o) {
        if (a instanceof RootBody) {
            RootBody rb = (RootBody) a;
            if (rb.AppConexion != null) {
                if (rb.AppConexion.getApp() != null) {
                    ABG = rb.AppConexion.getApp();
                    rb.AppConexion.getApp().setSocketIoBridge(SB);


                    todo(rb.AppConexion.getApp(), idop, o);
                } else {
                    //Log.e(this.getClass().getSimpleName(), "ActivityError");
                    noConect();
                }
            } else {
                //Log.e(this.getClass().getSimpleName(),"ActivityError");
                noConect();
            }
        } else {
            //Log.e(this.getClass().getSimpleName(),"ActivityError");
            ActivityError();
        }
    }


}