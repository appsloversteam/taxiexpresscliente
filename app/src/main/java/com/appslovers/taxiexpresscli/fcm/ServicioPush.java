package com.appslovers.taxiexpresscli.fcm;

import android.app.NotificationManager;
import android.content.Intent;
import android.util.Log;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.data.InnerActions;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.Notificaciones;
import com.appslovers.taxiexpresscli.util.Tools;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Map;

/**
 * Created by javierquiroz on 28/09/16.
 */

public class ServicioPush extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        NotificationManager NM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (remoteMessage != null) {

            Log.d("recibido", "From: " + remoteMessage.getFrom());
            if (remoteMessage.getNotification() != null) {
                Log.d("recibido", "Notification Message Body: " + remoteMessage.getNotification().getBody());
            } else {
                Log.d("ZZZZ", "viene de notificaicon :D 1");
                Map<String, String> map = remoteMessage.getData();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    Log.d("data", ">" + entry.getKey() + "/" + entry.getValue());
                }

                if (map.containsKey("op")) {
                    Intent servici = new Intent(getApplicationContext(), AppBackground.class);

                    String op = map.get("op");
                    switch (op) {
                        case "ConductorLLego":
                            JsonPack.RespConductorLlego rcl = new JsonPack.RespConductorLlego();
                            rcl.FechaRecojo = map.get("FechaRecojo");
                            String json1 = new Gson().toJson(rcl);

                            servici.setAction(InnerActions.push_conductorllego.toString());
                            servici.putExtra("json", json1);
                            break;
                        case "InicioServicio":
                            JsonPack.RespInicioServicio ris = new JsonPack.RespInicioServicio();
                            ris.FechaInicio = map.get("FechaInicio");
                            String json2 = new Gson().toJson(ris);

                            servici.setAction(InnerActions.push_incioservicio.toString());
                            servici.putExtra("json", json2);
                            break;
                        case "FinalizarServicioCliente":

                            JsonPack.RespFinalizarServ rfs = new JsonPack.RespFinalizarServ();
                            rfs.FechaFin = map.get("FechaFin");
                            rfs.Espera = Tools.getFloat(map.get("Espera"));
                            rfs.Paradas = Tools.getFloat(map.get("Paradas"));
                            rfs.Parqueos = Tools.getFloat(map.get("Parqueos"));
                            rfs.Peajes = Tools.getFloat(map.get("Peajes"));
                            rfs.Tarifa = Tools.getFloat(map.get("Tarifa"));

                            servici.setAction(InnerActions.push_finalizarserviciocliente.toString());
                            servici.putExtra("json", rfs);

                            break;
                    }
                    startService(servici);

                } else {
                    NM.notify(44, Notificaciones.getNotificacionReservaEnCamino(getApplication()));
                }
            }
        } else {
            NM.notify(44, Notificaciones.getNotificacionReservaEnCamino(getApplication()));
            Log.d("ZZZZ", "viene de notificaicon :D 2 ");
            Map<String, String> map = remoteMessage.getData();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                Log.d("data", ">" + entry.getKey() + "/" + entry.getValue());

            }
        }

    }

}
