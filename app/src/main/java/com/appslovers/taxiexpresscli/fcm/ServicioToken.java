package com.appslovers.taxiexpresscli.fcm;

import android.util.Log;

import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by javierquiroz on 28/09/16.
 */

public class ServicioToken extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("nuevo token", "Refreshed token: " + refreshedToken);
        Log.d("nuevo token","size "+refreshedToken.length());

        MyRequest MR=new MyRequest(Urls.updateTokenAndroid, MyRequest.HttpRequestType.POST)
        {
            @Override
            public void onSucces(ResponseWork rw, boolean isActive) {

            }

            @Override
            public void onFailedRequest(ResponseWork rw, boolean isActive) {

            }
        };


        SPUser sp=new SPUser(getApplication());
        sp.setToken(refreshedToken);

        if(sp.getResponseLogin()!=null)
        {
            MR.putParams("token",""+refreshedToken)
                    .putParams("cliente_id",""+sp.getResponseLogin().cliente_id)
                    .send();
        }



    }
}
