package com.appslovers.taxiexpresscli.body;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.dialogs.DialogCancelar;
import com.appslovers.taxiexpresscli.util.ConfirmDialog;
import com.appslovers.taxiexpresscli.util.Cronometro;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.dialogs.DialogFinalizarServicio;
import com.appslovers.taxiexpresscli.util.MyAppBackground;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.RootSplash;
import com.appslovers.taxiexpresscli.util.SocketIoBridge;
import com.appslovers.taxiexpresscli.util.Tools;
import com.appslovers.taxiexpresscli.util.TouchableMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;

import pe.com.visanet.lib.VisaNetConfigurationContext;
import pe.com.visanet.lib.VisaNetPaymentActivity;
import pe.com.visanet.lib.VisaNetPaymentInfo;

/**
 * Created by javierquiroz on 26/07/16.
 */
public class FragServicio extends TouchableMapFragment implements View.OnClickListener {

    View ViewRoot;
    LinearLayout llContenedor;

    ImageView ivFoto;
    TextView tvNombre, tvPlaca, tvColor, tvMarcaModelo;
    TextView tvDistancia, tvTiempo, tvTarifa, tvLabelTarifa;
    Button btnLlamar, btnCancelar;
    RatingBar rbCalificacion;
    ImageView ivCar0, ivCar1, ivCar2, ivCar3, ivCar4, ivCar5, ivCar6, ivCar7, ivCar8;

    LinearLayout llEnCamino, llContador, llInfoTarifa;
    TextView tvContador;
    TextView tvDTarifa, tvDTarifaFinal;

    final int REQUEST_CODE_PAYMENT=0000;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragservicio, null);
        llContenedor = (LinearLayout) ViewRoot.findViewById(R.id.contenedor);
        llContenedor.addView(super.onCreateView(inflater, container, savedInstanceState), 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        ivFoto = (ImageView) ViewRoot.findViewById(R.id.ivFoto);
        tvNombre = (TextView) ViewRoot.findViewById(R.id.tvNombre);
        tvPlaca = (TextView) ViewRoot.findViewById(R.id.tvPlaca);
        tvColor = (TextView) ViewRoot.findViewById(R.id.tvColor);
        tvMarcaModelo = (TextView) ViewRoot.findViewById(R.id.tvMarcaModelo);

        btnLlamar = (Button) ViewRoot.findViewById(R.id.btnLlamar);
        btnLlamar.setOnClickListener(this);

        rbCalificacion = (RatingBar) ViewRoot.findViewById(R.id.rbCalificacion);

        tvDistancia = (TextView) ViewRoot.findViewById(R.id.tvDistancia);
        tvTiempo = (TextView) ViewRoot.findViewById(R.id.tvTiempo);

        tvTarifa = (TextView) ViewRoot.findViewById(R.id.tvTarifa);
        tvLabelTarifa = (TextView) ViewRoot.findViewById(R.id.tvLabelTarifa);


        ivCar0 = (ImageView) ViewRoot.findViewById(R.id.ivCar0);
        ivCar1 = (ImageView) ViewRoot.findViewById(R.id.ivCar1);
        ivCar2 = (ImageView) ViewRoot.findViewById(R.id.ivCar2);
        ivCar3 = (ImageView) ViewRoot.findViewById(R.id.ivCar3);
        ivCar4 = (ImageView) ViewRoot.findViewById(R.id.ivCar4);
        ivCar5 = (ImageView) ViewRoot.findViewById(R.id.ivCar5);
        ivCar6 = (ImageView) ViewRoot.findViewById(R.id.ivCar6);
        ivCar7 = (ImageView) ViewRoot.findViewById(R.id.ivCar7);
        ivCar8 = (ImageView) ViewRoot.findViewById(R.id.ivCar8);


        llEnCamino = (LinearLayout) ViewRoot.findViewById(R.id.llEnCamino);
        llContador = (LinearLayout) ViewRoot.findViewById(R.id.llContador);
        llInfoTarifa = (LinearLayout) ViewRoot.findViewById(R.id.llInfoTarifa);

        tvContador = (TextView) ViewRoot.findViewById(R.id.tvContador);

        btnCancelar = (Button) ViewRoot.findViewById(R.id.btnCancelar);
        ViewRoot.findViewById(R.id.ivCerrarContador).setOnClickListener(this);
        ViewRoot.findViewById(R.id.ivCerrarEnCamino).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnCancelar).setOnClickListener(this);
        ViewRoot.findViewById(R.id.ivCerrarinfotarifar).setOnClickListener(this);

        tvDTarifa = (TextView) ViewRoot.findViewById(R.id.tvDtarifa);
        tvDTarifaFinal = (TextView) ViewRoot.findViewById(R.id.tvDtarifafinal);
        return ViewRoot;
    }


    public void mostrarInforTarifa() {
        llInfoTarifa.setVisibility(View.VISIBLE);
    }

    boolean flag_mantenerse = true;
    public MyAppBackground MAB;

    Marker MCliente, MConductor, MEnCamino;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        ((RootBody) getActivity()).flInfoTarifa.setVisibility(View.VISIBLE);
        ((RootBody) getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_rojo4r);
        ((RootBody) getActivity()).Emergencia.setEnabled(true);

        MAB = new MyAppBackground(getActivity()) {
            @Override
            public void todo(AppBackground app, int idop, Object o) {
                switch (idop) {
                    case 0:

                        if (app.getConductorActual() != null) {
                            if (app.getServicioActual().Estado.equals("1")) {
                                llEnCamino.setVisibility(View.VISIBLE);
                                btnLlamar.setVisibility(View.VISIBLE);
                            } else if (app.getServicioActual().Estado.equals("2")) {
                                btnCancelar.setVisibility(View.GONE);
                                btnLlamar.setVisibility(View.VISIBLE);
                                llContador.setVisibility(View.VISIBLE);

                                CRONO.setICrono(miICron, MAB.ABG.getServicioActual().FechaRecojo, MAB.ABG.getServicioActual().HoraInicio);
                                CRONO.encender(true);
                            } else if (app.getServicioActual().Estado.equals("3")) {
                                btnCancelar.setVisibility(View.GONE);
                                btnLlamar.setVisibility(View.GONE);

                                CRONO.setICrono(miICron, MAB.ABG.getServicioActual().FechaRecojo, MAB.ABG.getServicioActual().HoraInicio);
                                CRONO.getDiferenciaTiempo();
                                llContador.setVisibility(View.GONE);
                            } else if (app.getServicioActual().Estado.equals("6")) {
                                btnCancelar.setVisibility(View.GONE);
                                flag_mantenerse = false;
                            }

                            if (flag_mantenerse) {
                                tvDTarifa.setText("S/. " + Tools.formatFloat(Tools.getFloat(app.getServicioActual().Tarifa)));
                                tvDTarifaFinal.setText("S/. " + Tools.formatFloat(Tools.getFloat(app.getServicioActual().Tarifa)));

                                ((AppForeGround) getActivity().getApplication()).getPicasso().load(Urls.servidorarchivos + app.getConductorActual().conductor_foto).into(ivFoto);
                                tvNombre.setText(app.getConductorActual().conductor_nombre + " " + app.getConductorActual().conductor_apellido);
                                rbCalificacion.setRating(app.getConductorActual().conductor_calificacion);
                                tvPlaca.setText(app.getUnidadActual().unidad_placa);

                                tvColor.setText(app.getUnidadActual().unidad_color);
                                tvMarcaModelo.setText(app.getUnidadActual().unidad_marca + " " + app.getUnidadActual().unidad_modelo);
                                if (app.getUnidadActual().unidad_caracteristicas != null && app.getUnidadActual().unidad_caracteristicas.length() > 0) {
                                    String[] cars = app.getUnidadActual().unidad_caracteristicas.split(",");
                                    for (String s : cars) {
                                        switch (s) {
                                            case "1":
                                                ivCar0.setImageResource(R.drawable.opc_aire_on);
                                                break;
                                            case "2":
                                                ivCar1.setImageResource(R.drawable.opc_aeropuerto_on);
                                                break;
                                            case "3":
                                                ivCar2.setImageResource(R.drawable.opc_bateria_on);
                                                break;
                                            case "4":
                                                ivCar3.setImageResource(R.drawable.opc_mascota_on);
                                                break;
                                            case "5":
                                                ivCar4.setImageResource(R.drawable.opc_pago_on);
                                                break;
                                            case "6":
                                                ivCar5.setImageResource(R.drawable.opc_wifi_on);
                                                break;
                                            case "7":
                                                ivCar6.setImageResource(R.drawable.opc_menor_on);
                                                break;
                                            case "8":
                                                ivCar7.setImageResource(R.drawable.opc_mayor_on);
                                                break;
                                            case "9":
                                                ivCar8.setImageResource(R.drawable.opc_playa_on);
                                                break;
                                        }
                                    }

                                }

                                getMapAsync(new OnMapReadyCallback() {

                                    @Override
                                    public void onMapReady(GoogleMap googleMap) {

                                        GM = googleMap;
                                        LatLng llCli = new LatLng(MAB.ABG.getServicioActual().OrigenLat, MAB.ABG.getServicioActual().OrigenLng);
                                        LatLng llCon = new LatLng(MAB.ABG.getConductorActual().conductor_lat, MAB.ABG.getConductorActual().conductor_lng);

                                        if (MAB.ABG.getServicioActual().Estado.equals("3")) {
                                            GM.clear();
                                            MEnCamino = GM.addMarker(new MarkerOptions()
                                                    .position(llCon)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cliente_conductor))
                                                    .title("Yendo al destino"));


                                        } else //if(MAB.ABG.getServicioActual().Estado.equals("1") && MAB.ABG.getServicioActual().Estado.equals("2") )
                                        {
                                            GM.clear();
                                            MCliente = GM.addMarker(new MarkerOptions()
                                                    .position(llCli)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cliente))
                                                    .title("Cliente"));


                                            MConductor = GM.addMarker(new MarkerOptions()
                                                    .position(llCon)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_conductor))
                                                    .title("Conductor"));
                                        }


                                        actualizarPosicion(llCli, llCon);
                                    }
                                });

                            } else {
                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                        .commit();
                                MAB.ABG.NodeJsTurnOff();
                            }

                        }
                        break;
                    case 1://cancelar

                        app.EnviarCancelarServicio();
                        app.NodeJsTurnOff();
                        getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                        getActivity().finish();
                        break;

                    case 2:

                        if (android.util.Patterns.PHONE.matcher(MAB.ABG.getConductorActual().conductor_celular).matches()) {
                            //String uri = "tel:" +MAB.ABG.getServicioActual().celularr;
                            //String uri = "tel:" + "945928933";
                            String uri = "tel:" + MAB.ABG.getConductorActual().conductor_celular;
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(uri));
                            startActivity(intent);
                        }
                        break;


                }
            }

        };


        getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap googleMap) {
                GM = googleMap;
            }
        });
    }


    public void actualizarPosicion(LatLng llCli, LatLng llCon) {

        if (MEnCamino != null) {
            if (llCon != null) {
                MEnCamino.setPosition(llCon);
                GM.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(llCon, AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));
            }
        } else {
            if (llCon != null)
                MConductor.setPosition(llCon);

            if (llCli == null) {
                if (MCliente != null)
                    llCli = MCliente.getPosition();
            }

            if (llCli != null && llCon != null) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(llCli);
                builder.include(llCon);
                GM.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics())));
            }
        }
    }

    Cronometro CRONO = new Cronometro();

    Cronometro.ICronometro miICron = new Cronometro.ICronometro() {
        @Override
        public void onChange(String texto, boolean color) {

            if (color) {
                tvContador.setTextColor(Color.RED);
            }
            tvContador.setText(texto);
        }
    };


    GoogleMap GM;
    boolean propia_cancelacion = true;

    @Override
    public void onStart() {

        super.onStart();
        //Toast.makeText(getActivity(),"onStart",Toast.LENGTH_SHORT).show();
        MAB.doit(getActivity(), 0);

        /*
        SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha="2016-8-2 10:19:4";
        Date d=null;
        try {
            d=yyyyMMddHHmmss.parse(fecha);
        }catch (ParseException e)
        {
            Log.e("parsefecha",">"+e.getMessage());
        }
        if(d!=null)
        {
            Calendar cal=Calendar.getInstance();
            cal.setTime(d);
            Log.d("c mes",">"+cal.get(Calendar.MONTH));
            Log.d("d mes", ">" + d.getMonth());

            Log.d("c seg",">"+cal.get(Calendar.SECOND));
            Log.d("d seg",">"+d.getSeconds());
        }*/

        MAB.addListner(new SocketIoBridge() {
            @Override
            public void enviarSolicitudPago(String s){
                Log.e("FragServicio", "enviarSolicitudPago: "+ s );
                JsonPack.SolicitudPago solicitud=new Gson().fromJson(s, JsonPack.SolicitudPago.class);
                showVisaForm(solicitud);
            }

            @Override
            public void ServicioCancelado(JsonPack.CancelarServicio s) {

                if (propia_cancelacion) {

                    if (CRONO != null)
                        CRONO.encender(false);

                    new ConfirmDialog(getActivity(), "El Conductor canceló el servicio.", getString(R.string.app_empresa), "Aceptar", "") {
                        @Override
                        public void onPositive() {
                            MAB.ABG.NodeJsTurnOff();
                            getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                            getActivity().finish();
                        }

                    }.show();
                }

            }


            @Override
            public void ConductorLLego(String solicitud) {
                btnCancelar.setVisibility(View.GONE);
                Log.d("conductorllego", ">" + solicitud);
                Log.d("hora del sistema", ">" + new Date(System.currentTimeMillis()).toLocaleString());
                JsonPack.RespConductorLlego rcl = null;
                try {
                    rcl = new Gson().fromJson(solicitud, JsonPack.RespConductorLlego.class);
                } catch (JsonParseException e) {
                    Log.e("parseex", ">" + e.getMessage());
                }

                llEnCamino.setVisibility(View.GONE);
                llContador.setVisibility(View.VISIBLE);


                CRONO.setICrono(miICron, MAB.ABG.getServicioActual().FechaRecojo, MAB.ABG.getServicioActual().HoraInicio);
                MAB.ABG.getServicioActual().FechaRecojo = rcl.FechaRecojo;

                CRONO.encender(true);
            }

            @Override
            public void RefreshCliente(String s) {
                LatLng newPos = null;
                try {
                    newPos = new Gson().fromJson(s, JsonPack.RespRefreshConductor.class).getLL();

                } catch (JsonParseException e) {
                    Log.e("jsonex", ">" + e.getMessage());
                }

                if (newPos != null) {
                    actualizarPosicion(null, newPos);
                    MAB.ABG.ConductorActual.conductor_lat = newPos.latitude;
                    MAB.ABG.ConductorActual.conductor_lng = newPos.longitude;

                    if (MAB.ABG.getServicioActual().Estado.equals("1"))//conductor yendo a recoger al cliente
                    {
                        float[] result = new float[4];
                        Location.distanceBetween(MAB.ABG.getServicioActual().OrigenLat, MAB.ABG.getServicioActual().OrigenLng, newPos.latitude, newPos.longitude, result);
                        if (result != null) {
                            if (result.length > 0) {

                                if (result[0] > 1000.0f) {
                                    tvDistancia.setText(String.format("%.1f", result[0] / 1000.0f) + " Km");
                                } else if (result[0] < 1000.0f) {
                                    tvDistancia.setText(String.format("%.1f", result[0]) + " m");
                                }

                                if (mr_last_distance == 0.0f) {
                                    mr_last_distance = result[0];
                                    mr_distance_contador = VECES_ANTES_CONSULTAR;
                                } else {
                                    float dif = Math.abs(mr_last_distance - result[0]);
                                    if (dif > DISTANCIA_CAMBIO) {
                                        mr_last_distance = result[0];
                                        mr_distance_contador = VECES_ANTES_CONSULTAR;
                                    } else if (dif > DISTANCIA_NOESTAENELMISMOSITIO) {
                                        mr_distance_contador++;
                                    }
                                }

                                if (mr_distance_contador >= VECES_ANTES_CONSULTAR) {
                                    mr_distance_contador = 0;

                                    MR_DISTANCE.CancelarRequest();
                                    MR_DISTANCE.putParams("origins", newPos.latitude + "," + newPos.longitude)
                                            .putParams("destinations", MAB.ABG.getServicioActual().OrigenLat + "," + MAB.ABG.getServicioActual().OrigenLng)
                                            .putParams("mode", "driver")
                                            .putParams("language", "es-PE")
                                            .putParams("key", Urls.GOOGLE_KEY).send();
                                }

                            }
                        }
                    } else if (MAB.ABG.getServicioActual().Estado.equals("3")) // conductor llevando a destino
                    {
                        if (MAB.ABG.getServicioActual().DestinoLat != 0 || MAB.ABG.getServicioActual().DestinoLng != 0) {
                            float[] result = new float[4];
                            Location.distanceBetween(MAB.ABG.getServicioActual().DestinoLat, MAB.ABG.getServicioActual().DestinoLng, newPos.latitude, newPos.longitude, result);
                            if (result != null) {
                                if (result.length > 0) {

                                    if (result[0] > 1000.0f) {
                                        tvDistancia.setText(String.format("%.1f", result[0] / 1000.0f) + " Km");
                                    } else if (result[0] < 1000.0f) {
                                        tvDistancia.setText(String.format("%.1f", result[0]) + " m");
                                    }

                                    if (mr_last_distance == 0.0f) {
                                        mr_last_distance = result[0];
                                        mr_distance_contador = VECES_ANTES_CONSULTAR;
                                    } else {
                                        float dif = Math.abs(mr_last_distance - result[0]);
                                        if (dif > DISTANCIA_CAMBIO) {
                                            mr_last_distance = result[0];
                                            mr_distance_contador = VECES_ANTES_CONSULTAR;
                                        } else if (dif > DISTANCIA_NOESTAENELMISMOSITIO) {
                                            mr_distance_contador++;
                                        }
                                    }

                                    if (mr_distance_contador >= VECES_ANTES_CONSULTAR) {
                                        mr_distance_contador = 0;

                                        MR_DISTANCE.CancelarRequest();
                                        MR_DISTANCE.putParams("origins", newPos.latitude + "," + newPos.longitude)
                                                .putParams("destinations", MAB.ABG.getServicioActual().DestinoLat + "," + MAB.ABG.getServicioActual().DestinoLng)
                                                .putParams("mode", "driver")
                                                .putParams("language", "es-PE")
                                                .putParams("key", Urls.GOOGLE_KEY).send();
                                    }
                                }
                            }
                        }

                    }


                }

            }

            @Override
            public void InicioServicio(String solicitud) {


                CRONO.setICrono(miICron, MAB.ABG.getServicioActual().FechaRecojo, MAB.ABG.getServicioActual().HoraInicio);
                CRONO.encender(false);
                if (MAB.ABG.getServicioActual().HoraInicio == null)
                    MAB.ABG.getServicioActual().HoraInicio = CRONO.TFinal;


                llContador.setVisibility(View.GONE);
                llEnCamino.setVisibility(View.GONE);

                btnLlamar.setVisibility(View.GONE);
                btnCancelar.setVisibility(View.GONE);
                GM.clear();


                if (MEnCamino == null) {
                    MEnCamino = GM.addMarker(new MarkerOptions()
                            .position(MConductor.getPosition())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cliente_conductor))
                            .title("Yendo al destino"));
                } else {
                    MEnCamino.setPosition(MConductor.getPosition());
                }
            }

            @Override
            public void FinalizarServicioCliente(JsonPack.RespFinalizarServ s) {

                llEnCamino.setVisibility(View.GONE);
                btnCancelar.setVisibility(View.GONE);

                //if (s.TipoPago.equalsIgnoreCase("Tarjeta")){

                //    return;
                //}


                if (FragServicio.this != null) {
                    if (DFS == null) {
                        DFS = new DialogFinalizarServicio();
                        DFS.FS = s;
                        DFS.setTargetFragment(FragServicio.this, 12345);
                    }
                    if (!DFS.isAdded())
                        DFS.show(getFragmentManager(), "dfdsf");

                /*
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                        .commit();
                MAB.ABG.NodeJsTurnOff();*/
                }
            }
        });
    }

    final float DISTANCIA_NOESTAENELMISMOSITIO = 10f;
    final float DISTANCIA_CAMBIO = 50f;
    final int VECES_ANTES_CONSULTAR = 3;
    int mr_distance_contador = 0;
    float mr_last_distance = 0.0f;
    MyRequest<JsonPack.GoogleDistanceMatrixResult> MR_DISTANCE = new MyRequest<JsonPack.GoogleDistanceMatrixResult>(Urls.google_distancematrix, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleDistanceMatrixResult object) {

            mr_distance_contador = 0;
            if (object != null) {
                if (object.rows != null) {
                    if (object.rows.size() > 0) {
                        if (object.rows.get(0).elements != null) {
                            if (object.rows.get(0).elements.size() > 0) {
                                tvTiempo.setText(object.rows.get(0).elements.get(0).duration.text);
                            }
                        }
                    }
                }
            }
        }
    };

    private void showVisaForm(JsonPack.SolicitudPago solicitud){
        SPUser user=new SPUser(getActivity());

        String externalTransactionId = "pe.taxiexpress.id";
        String transactionId = String.format("%09d",Integer.parseInt(MAB.ABG.getServicioActual().IdServicio));
        if (Integer.parseInt(MAB.ABG.getServicioActual().Tarifa)==0)
            MAB.ABG.getServicioActual().Tarifa=solicitud.tarifa;
        double amount = Double.parseDouble(MAB.ABG.getServicioActual().Tarifa) + solicitud.costosAdicionales.Espera + solicitud.costosAdicionales.Paradas+ solicitud.costosAdicionales.Parqueos+solicitud.costosAdicionales.Peajes;

        VisaNetConfigurationContext ctx = new VisaNetConfigurationContext();
        ctx.setMerchantId("440303901");
        ctx.setAccessKeyId("AKIAJKPMKU6LQCZTYF2A");
        ctx.setSecretAccess("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");
        ctx.setEndPointURL("https://api.vnforapps.com/api.tokenization/api/v2");


        HashMap<String, String> data = new HashMap<String, String>();
        ctx.setData(data);

        ctx.setCustomerFirstName(user.getResponseLogin().nombre);
        ctx.setCustomerLastName(user.getResponseLogin().apellido);
        ctx.setCustomerEmail(user.getResponseLogin().email);
        ctx.setCurrency(VisaNetConfigurationContext.VisaNetCurrency.PEN);
        ctx.setLanguage(VisaNetConfigurationContext.VisaNetLanguage.SPANISH);
        ctx.setAmount(amount);

        ctx.setTransactionId(transactionId);
        ctx.setExternalTransactionId(externalTransactionId);

        ctx.getMerchantDefinedData().put("field3", "movil");
        ctx.getMerchantDefinedData().put("field91", "movil");
        ctx.getMerchantDefinedData().put("field92", "Taxi Express");

        // iniciar actividad VisaNet
        Intent intent = new Intent(getActivity(), VisaNetPaymentActivity.class);
        intent.putExtra(VisaNetConfigurationContext.VISANET_CONTEXT, ctx);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 12345:

                if (resultCode == 200) {
                    if (getActivity() != null) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                                .commit();
                    }

                    MAB.ABG.NodeJsTurnOff();
                }

                break;

            case 8912:

                if (resultCode == 200) {

                    MAB.doit(getActivity(), 1);
                }
                break;



        }
    }




    DialogFinalizarServicio DFS;

    @Override
    public void onStop() {
        super.onStop();

        MR_DISTANCE.CancelarRequest();
        MAB.removeListener();
        if (CRONO != null)
            CRONO.encender(false);
        super.onPause();
    }

    DialogCancelar DCAN;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ivCerrarinfotarifar:
                llInfoTarifa.setVisibility(View.GONE);

                break;
            case R.id.ivCerrarContador:

                llContador.setVisibility(View.GONE);
                break;

            case R.id.ivCerrarEnCamino:

                llEnCamino.setVisibility(View.GONE);
                break;

            case R.id.btnLlamar:
                MAB.doit(getActivity(), 2);
                break;

            case R.id.btnCancelar:

                if (DCAN == null)
                    DCAN = new DialogCancelar();

                DCAN.setTargetFragment(FragServicio.this, 8912);
                DCAN.show(getFragmentManager(), "sdfgdag");


                //MAB.doit(getActivity(),1);

                break;

        }
    }
}
