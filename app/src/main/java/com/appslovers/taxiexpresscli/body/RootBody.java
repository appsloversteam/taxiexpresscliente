package com.appslovers.taxiexpresscli.body;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.AppBinder;
import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.AppLogService;
import com.appslovers.taxiexpresscli.dialogs.DialogTerminos;
import com.appslovers.taxiexpresscli.util.ConfirmDialog;
import com.appslovers.taxiexpresscli.data.ISetActivityResult;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyGps;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.dialogs.DialogAlerta;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.RootSplash;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.JsonObject;

import java.util.HashMap;

import pe.com.visanet.lib.VisaNetConfigurationContext;
import pe.com.visanet.lib.VisaNetPaymentActivity;
import pe.com.visanet.lib.VisaNetPaymentInfo;

public class RootBody extends AppCompatActivity implements View.OnClickListener, ISetActivityResult {


    private static final int REQUEST_CODE_PAYMENT = 65536;
    public AppBinder AppConexion;
    boolean flag_fragbienvenido = true;

    ServiceConnection SC = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            AppConexion = ((AppBinder) iBinder);
            AppConexion.setRoot(RootBody.this);
            Log.d("Service Conection", " onServiceConnected---------------------->");
            actualizarVista();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            //solo se llama cuando el servicio se cae :(
            //Log.d("Service Conection", " onServiceDisconnected---------------------->");
            AppConexion.setApp(null);
            AppConexion.setRoot(null);
        }
    };


    public DrawerLayout drawer;
    public TextView tvTitulo;

    public FrameLayout flRutasFrec;
    public FrameLayout flReservasAdd;
    public FrameLayout flInfoTarifa;
    //public FrameLayout flSolicitudes;
    //public TextView tvNroSolicitudes;
    public Button Emergencia;
    public ImageView btnMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rootbody);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        findViewById(R.id.btnMenu).setOnClickListener(this);

        flRutasFrec = (FrameLayout) findViewById(R.id.flRutasFrec);
        flReservasAdd = (FrameLayout) findViewById(R.id.flReservasAdd);
        flInfoTarifa = (FrameLayout) findViewById(R.id.flInfoTarifa);

        findViewById(R.id.btnMenuRutasFrecuentes).setOnClickListener(this);
        findViewById(R.id.btnReservasAdd).setOnClickListener(this);
        findViewById(R.id.btnInfoTarifa).setOnClickListener(this);

        findViewById(R.id.btnSolicitar).setOnClickListener(this);
        findViewById(R.id.btnReservas).setOnClickListener(this);
        findViewById(R.id.btnTarifario).setOnClickListener(this);
        findViewById(R.id.btnHistorial).setOnClickListener(this);
        findViewById(R.id.btnServicio).setOnClickListener(this);
        findViewById(R.id.btnBeneficio).setOnClickListener(this);
        findViewById(R.id.btnCerrar).setOnClickListener(this);
        findViewById(R.id.btnExpreso).setOnClickListener(this);
        findViewById(R.id.btnContactenos).setOnClickListener(this);
        findViewById(R.id.btnMiSeguridad).setOnClickListener(this);
        findViewById(R.id.btnRutasFrecuentes).setOnClickListener(this);
        findViewById(R.id.btnVales).setOnClickListener(this);
        findViewById(R.id.btnTarjetas).setOnClickListener(this);

        findViewById(R.id.ivLogoTop).setOnLongClickListener(OLCL);

        Emergencia = (Button) findViewById(R.id.btnEmergencia);
        Emergencia.setOnClickListener(this);

        tvTitulo = (TextView) findViewById(R.id.tvTitulo);
        tvTitulo.setText(getString(R.string.app_empresa));
        btnMenu=(ImageView) findViewById(R.id.imageView6);

    }

    int contador = 0;
    View.OnLongClickListener OLCL = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (contador >= 3) {
                startService(new Intent(getApplicationContext(), AppLogService.class));
            } else {
                contador++;
            }
            return false;
        }
    };


    public void mostarTopBarSolicitud() {

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    public void actualizarVista() {
        AppConexion.getApp().NodeJsTurnOn();
        if (AppConexion.getApp().isNodeJsConnected()) {
            if (AppConexion.getApp().getServicioActual() != null && (AppConexion.getApp().getServicioActual().getEstado() >= 1 && AppConexion.getApp().getServicioActual().getEstado() <= 5)) {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);

                //if(!(f instanceof FragServicio)){ // solo funciona si es andorid 5 en adelante

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, new FragServicio(), FragServicio.class.getSimpleName())
                        .commit();
                //}
            } else if (AppConexion.getApp().getServicioActual() != null && AppConexion.getApp().getServicioActual().getEstado() == 6) {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                if (!(f instanceof FragCalificacion)) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                            .commit();
                    AppConexion.getApp().NodeJsTurnOff();
                }
            } else {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                if (!(f instanceof Frag_3_Solicitud)) {
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fl_root_inside, new Frag_3_Solicitud(), Frag_3_Solicitud.class.getSimpleName())
                            .commit();
                }
            }
        } else {
            if (((AppForeGround) getApplication()).flag_califico == 1) {

                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                if (!(f instanceof FragCalificacion)) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                            .commit();
                    AppConexion.getApp().NodeJsTurnOff();
                }
            } else {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                if (!(f instanceof Frag_3_Solicitud)) {
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fl_root_inside, new Frag_3_Solicitud(), Frag_3_Solicitud.class.getSimpleName())
                            .commit();
                }
            }
        }
    }

    public MyGps GPS;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        GPS = new MyGps(this);
        //bindService(new Intent(this, AppBackground.class), SC, Context.BIND_AUTO_CREATE);
        startService(new Intent(getApplicationContext(), AppBackground.class));
    }


    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, AppBackground.class), SC, 0);

    }

    @Override
    protected void onStop() {
        unbindService(SC);
        super.onStop();
    }


    @Override
    public void onBackPressed() {

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
        if (f instanceof Frag_3_Solicitud) {
            Frag_3_Solicitud fs = (Frag_3_Solicitud) f;
            int index = fs.Paginador.getCurrentItem();

            Log.v("index on back", ">" + index);
            if (index == 1) {
                ((Frag_3_Solicitud) f).Paginador.setCurrentItem(0);
            } else if (index == 2) {

                ((FragSolicitudBuscando) ((Frag_3_Solicitud) f).frags.get(2)).onBack();
                ((Frag_3_Solicitud) f).Paginador.setCurrentItem(fs.getRetrocederAlFrag());
            } else {
                super.onBackPressed();
            }

        } else if (
                (f instanceof Frag_8_Beneficios)
                        || (f instanceof Frag_2_Reservas)
                        || (f instanceof Frag_4_Tarifario)
                        || (f instanceof Frag_6_ServiciosHistorial)
                        || (f instanceof Frag_10_ServicioEmergencia)
                        || (f instanceof Frag_1_Expreso)
                        || (f instanceof FragValesDescuentos)
                ) {
            actualizarVista();
        } else {
            super.onBackPressed();
        }


    }

    public void retroceder() {
        super.onBackPressed();
    }


    public JsonPack.RutaFrec RUTAFRECUENTETEMP;

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.btnVales:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragValesDescuentos(), FragValesDescuentos.class.getSimpleName())
                        //.addToBackStack("dsfsdfsdadf")
                        .commit();
                drawer.closeDrawers();
                break;


            case R.id.btnInfoTarifa:

                Fragment ff = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                if (ff instanceof FragServicio) {

                    ((FragServicio) ff).mostrarInforTarifa();
                }

                break;

            case R.id.btnSolicitar:

                if (AppConexion != null) {
                    if (AppConexion.getApp() != null) {
                        if (AppConexion.getApp().getServicioActual() == null) {
                            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                            if (!(f instanceof Frag_3_Solicitud)) {


                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fl_root_inside, new Frag_3_Solicitud(), Frag_3_Solicitud.class.getSimpleName())
                                        .commit();


                            }
                        } else if (AppConexion.getApp().getServicioActual().getEstado() > 3) {
                            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fl_root_inside);
                            if (!(f instanceof Frag_3_Solicitud)) {


                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fl_root_inside, new Frag_3_Solicitud(), Frag_3_Solicitud.class.getSimpleName())
                                        .commit();


                            }
                        }
                    }
                }


                drawer.closeDrawers();

                break;

            case R.id.btnTarjetas:{

            }

            case R.id.btnMiSeguridad:

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, new Frag_5_MiSeguridad(), Frag_5_MiSeguridad.class.getSimpleName())
                        .addToBackStack("miseguridad")
                        .commit();

                drawer.closeDrawers();

                break;

            case R.id.btnContactenos:

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, new Frag_9_Contactenos(), Frag_9_Contactenos.class.getSimpleName())
                        .addToBackStack("contactenos")
                        .commit();

                drawer.closeDrawers();

                break;

            case R.id.btnExpreso:

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, new Frag_1_Expreso(), Frag_1_Expreso.class.getSimpleName())
                        .addToBackStack("expr")
                        .commit();

                drawer.closeDrawers();

                break;
            case R.id.btnRutasFrecuentes:

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, Frag_7_RutasFrecuentes.getFrag(1), Frag_7_RutasFrecuentes.class.getSimpleName())
                        .addToBackStack("rutfrec1")
                        .commit();

                drawer.closeDrawers();
                break;

            case R.id.btnMenuRutasFrecuentes:
                Frag_7_RutasFrecuentes frf = Frag_7_RutasFrecuentes.getFrag(2);
                frf.setTargetFragment(getSupportFragmentManager().findFragmentById(R.id.fl_root_inside), 12);

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fl_root_inside, frf, Frag_7_RutasFrecuentes.class.getSimpleName())
                        .addToBackStack("rutfrec2")
                        .commit();

                break;

            case R.id.btnReservas:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_2_Reservas(), Frag_2_Reservas.class.getSimpleName())
                        //.addToBackStack("dsfsdf")
                        .commit();
                drawer.closeDrawers();

                break;

            case R.id.btnReservasAdd:

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragAddReserva(), FragAddReserva.class.getSimpleName())
                        .addToBackStack("addres")
                        .commit();
                break;

            case R.id.btnMenu:
                drawer.openDrawer(GravityCompat.START);

                break;
            case R.id.btnBeneficio:


                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_8_Beneficios(), Frag_8_Beneficios.class.getSimpleName())
                        //.addToBackStack("dsfsdfsdadf")
                        .commit();
                drawer.closeDrawers();
                break;

            case R.id.btnServicio:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_10_ServicioEmergencia(), Frag_10_ServicioEmergencia.class.getSimpleName())
                        //.addToBackStack("dsfsdfddf")
                        .commit();
                drawer.closeDrawers();

                break;
            case R.id.btnHistorial:

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_6_ServiciosHistorial(), Frag_6_ServiciosHistorial.class.getSimpleName())
                        //.addToBackStack("dsfsdfdf")
                        .commit();
                drawer.closeDrawers();
                break;

            case R.id.btnTarifario:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new Frag_4_Tarifario(), Frag_4_Tarifario.class.getSimpleName())
                        //.addToBackStack("dsfsdf")
                        .commit();
                drawer.closeDrawers();

                break;


            case R.id.btnEmergencia:

                DialogAlerta md = new DialogAlerta();
                md.setTargetFragment(getSupportFragmentManager().findFragmentById(R.id.fl_root_inside), 1);
                md.show(getSupportFragmentManager(), "sms");
                break;

            case R.id.btnIniciarSesion:


                break;

            case R.id.btnCerrar:


                new ConfirmDialog(getWindow().getContext(), "¿Desea cerrar sesión ?", getString(R.string.app_empresa), "Cerrar Sesión") {
                    @Override
                    public void onPositive() {

                        if (AppConexion != null) {
                            AppConexion.getApp().NodeJsTurnOff();
                            AppConexion.getApp().detenerServicio();
                        }
                        dismisss();

                        SPUser.logout(getApplicationContext());
                        startActivity(new Intent(getApplicationContext(), RootSplash.class));
                        finish();
                    }
                }.show();

                break;
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        //AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        //AppEventsLogger.deactivateApp(this);
        super.onPause();
    }

    MyGps mygps;

    @Override
    public void setMyInterface(MyGps mgps) {
        mygps = mgps;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //switch (requestCode){
            //case REQUEST_CODE_PAYMENT:
            //{
                if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                    if (data != null) {
                        VisaNetPaymentInfo info = (VisaNetPaymentInfo) data.getSerializableExtra(VisaNetConfigurationContext.VISANET_CONTEXT);
                        //Toast.makeText(getApplicationContext(), "Información de Pago recibida de VISANET", Toast.LENGTH_LONG).show();
                        if (AppConexion.getApp()!=null)
                            if (AppConexion.getApp().ServicioActual!=null)
                                showPopUpResultVisa(info);
                    }
                }
                else if(resultCode == Activity.RESULT_CANCELED) {
                    Log.i("pancake", "El usuario canceló.");
                }
            //}
        //}


        if (mygps != null)
            mygps.onGetResult(requestCode, resultCode, data);
    }

    private void showPopUpResultVisa(final VisaNetPaymentInfo info) {

        try{

            // if(progressDialog.isShowing())
            //     progressDialog.dismiss();

            AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
            helpBuilder.setTitle("Resultado de la Operación");

            LayoutInflater inflater = getLayoutInflater();
            View viewPopupResultVisa  = inflater.inflate(R.layout.popupresult_visa, null);
            helpBuilder.setView(viewPopupResultVisa);

            //Setting info from Visa in Popup.
            TextView textView_NumOrden = (TextView)viewPopupResultVisa.findViewById(R.id.textView_NumOrden);
            TextView textView_ResultOper = (TextView)viewPopupResultVisa.findViewById(R.id.textView_ResultOper);
            TextView textView_MotivoDeneg = (TextView)viewPopupResultVisa.findViewById(R.id.textView_MotivoDeneg);
            TextView textView_NumTarjet = (TextView)viewPopupResultVisa.findViewById(R.id.textView_NumTarjeta);
            TextView textView_ImpAutizado = (TextView)viewPopupResultVisa.findViewById(R.id.textView_ImporteAutizado);
            TextView textView_NombreHabiente= (TextView)viewPopupResultVisa.findViewById(R.id.textView_NombreHabiente);
            TextView textView_FechaHora2= (TextView)viewPopupResultVisa.findViewById(R.id.textView_FechaHora2);
            TextView textView_DescripcionProducto        = (TextView)viewPopupResultVisa.findViewById(R.id.textView_DescripcionProducto);
            TextView textView_TerminosyCondiciones=(TextView)viewPopupResultVisa.findViewById(R.id.textView95);
            textView_TerminosyCondiciones.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogTerminos md = new DialogTerminos();
                    md.show(getSupportFragmentManager(), "terminos");
                }
            });
            //TextView textView_DescripcionTitulo = (TextView)viewPopupResultVisa.findViewById(R.id.TextView_ImprimirTitle);

            textView_NumOrden.setText(" " + info.getData().get("NUMORDEN"));
            textView_ResultOper.setText( info.getData().get("RESPUESTA").equals("1") ? " Autorizada" : " Denegada"  );
            textView_MotivoDeneg.setText(" " +info.getData().get("DSC_COD_ACCION"));
            textView_NumTarjet.setText(" " +info.getData().get("PAN"));
            textView_ImpAutizado.setText(" " + "S/." + info.getData().get("IMP_AUTORIZADO"));
            textView_NombreHabiente.setText(" " + info.getFirstName() + " " + info.getLastName());
            textView_FechaHora2.setText(" " + info.getData().get("FECHAYHORA_TX"));
            textView_DescripcionProducto.setText("MOVILIDAD TAXI EXPRESS");

            if(!info.getData().get("RESPUESTA").equals("1")) //si NO DEVUELVE EXITO no muestra titulo del diálogo
            {
                //textView_DescripcionTitulo.setVisibility(View.GONE);
                helpBuilder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AppConexion.getApp().finalizarConTarjeta(info,AppConexion.getApp().getServicioActual().IdServicio);
                    }
                });
            }
            else
            {
                helpBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        AppConexion.getApp().finalizarConTarjeta(info,AppConexion.getApp().getServicioActual().IdServicio);
                    }
                });
            }

            AlertDialog helpDialog = helpBuilder.create();
            helpDialog.show();

            Button pbutton = helpDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setBackgroundColor(Color.rgb(253,191, 45));

        }catch(Exception ex){
            final AlertDialog.Builder errorNotificationDialog = new AlertDialog.Builder(this);
            errorNotificationDialog.setTitle("Error en el pago");
            errorNotificationDialog.setMessage("Ha ocurrido un error al procesar el pago. Debe cancelar en efectivo.");
            errorNotificationDialog.setCancelable(false);
            errorNotificationDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    AppConexion.getApp().finalizarConTarjeta(null,AppConexion.getApp().getServicioActual().IdServicio);
                }
            });
            errorNotificationDialog.show();
        }

    }
    private void showTicket(int idPedido) {


    }




}
