package com.appslovers.taxiexpresscli.body;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.R;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_1_Expreso extends Fragment implements View.OnClickListener {

    View ViewRoot;
    Button btnSi, btnNo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragserexpreso, null);
        btnSi = (Button) ViewRoot.findViewById(R.id.btnSi);
        btnSi.setOnClickListener(this);
        btnNo = (Button) ViewRoot.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(this);

        return ViewRoot;
    }

    SPUser SPCLI;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).tvTitulo.setText("Servicio Expreso");
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);

        SPCLI = new SPUser(getActivity());
        if (SPCLI.getExpreso()) {
            btnSi.setTextColor(Color.WHITE);
            btnSi.setEnabled(false);

            btnNo.setTextColor(Color.BLACK);
            btnNo.setEnabled(true);
        } else {

            btnSi.setTextColor(Color.BLACK);
            btnSi.setEnabled(true);

            btnNo.setTextColor(Color.WHITE);
            btnNo.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSi:

                SPCLI.setExpreso(true);

                btnSi.setTextColor(Color.WHITE);
                btnSi.setEnabled(false);

                btnNo.setTextColor(Color.BLACK);
                btnNo.setEnabled(true);

                break;

            case R.id.btnNo:

                SPCLI.setExpreso(false);

                btnSi.setTextColor(Color.BLACK);
                btnSi.setEnabled(true);

                btnNo.setTextColor(Color.WHITE);
                btnNo.setEnabled(false);
                break;
        }
    }
}
