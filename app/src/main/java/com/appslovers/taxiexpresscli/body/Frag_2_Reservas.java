package com.appslovers.taxiexpresscli.body;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.data.IMyAdapter;
import com.appslovers.taxiexpresscli.data.IMyViewHolder;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */

public class Frag_2_Reservas extends Fragment implements IMyViewHolder {
    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.Reserva> reservas;
    LinearLayoutManager llm;
    private MyAdapter<Frag_2_Reservas.ReservaViewHolder, JsonPack.Reserva> mAdapter;
    LinearLayout llSinReservas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragreservas, null);
        llSinReservas = (LinearLayout) ViewRoot.findViewById(R.id.llSinReservas);
        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lista);

        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);
        return ViewRoot;
    }

    JsonPack.ResponseLogin rl;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).tvTitulo.setText("Reservas");
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.VISIBLE);
        Log.i("se muestra le boton1", "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");


        rl = new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("cliente_id", rl.cliente_id);
        MHR.send();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.VISIBLE);
        //comentar aqui

        Log.i("se muestra le boton2", "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    MyRequest MHR = new MyRequest(Urls.ws_lista_reservas, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {
            reservas = null;
            try {
                reservas = new Gson().fromJson(rw.ResponseBody, JsonPack.RespReservas.class).result;
                if (reservas != null) {
                    if (reservas.size() > 0)
                        llSinReservas.setVisibility(View.GONE);
                    else {
                        llSinReservas.setVisibility(View.VISIBLE);
                    }
                    mAdapter = new MyAdapter<>(reservas, Frag_2_Reservas.this, R.layout.fragreservas_item);
                    rvLista.setAdapter(mAdapter);
                }
            } catch (JsonParseException e) {
                Log.e("json ex", ">" + e.getMessage());
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new ReservaViewHolder(v);
    }


    public class ReservaViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<ReservaViewHolder> {
        TextView tvFecha, tvHora, tvOrigen, tvDestino, tvCliente, tvTarifa;
        NumberFormat format;

        public ReservaViewHolder(View v) {
            super(v);
            tvFecha = (TextView) v.findViewById(R.id.tvFecha);
            tvHora = (TextView) v.findViewById(R.id.tvHora);
            tvOrigen = (TextView) v.findViewById(R.id.tvOrigen);
            tvDestino = (TextView) v.findViewById(R.id.tvDestino);
            tvCliente = (TextView) v.findViewById(R.id.tvCliente);
            tvTarifa = (TextView) v.findViewById(R.id.tvTarifa);
            format = new DecimalFormat("#0.00");
        }

        @Override
        public void bindView(ReservaViewHolder holder, int position) {
            holder.tvCliente.setText(reservas.get(position).cliente_nombre);
            holder.tvHora.setText(reservas.get(position).Hora);
            holder.tvFecha.setText(reservas.get(position).Fecha);
            holder.tvOrigen.setText(reservas.get(position).dir_origen);
            holder.tvDestino.setText(reservas.get(position).dir_destino);
            try {
                holder.tvTarifa.setText("S/. " + format.format(Float.parseFloat(reservas.get(position).tarifa)));
            } catch (Exception e) {
                holder.tvTarifa.setText("S/. " + reservas.get(position).tarifa);
            }
        }
    }
}
