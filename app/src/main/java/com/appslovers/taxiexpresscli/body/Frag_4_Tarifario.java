package com.appslovers.taxiexpresscli.body;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyFragment;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.dialogs.DialogDirecciones;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.Tools;
import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_4_Tarifario extends MyFragment implements View.OnClickListener {
    View ViewRoot;
    Button tvTipoServicio;
    Button btnOrigen, btnDestino;
    TextView tvTarifa;
    SPUser SPCLI;
    String[] tiposdeservicio;
    ArrayList<JsonPack.TipoServicio> tiposServices;
    String tiposerviciosel = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragtarifario, null);
        tvTarifa = (TextView) ViewRoot.findViewById(R.id.tvTarifa);
        btnOrigen = (Button) ViewRoot.findViewById(R.id.btnOrigen);
        btnOrigen.setOnClickListener(this);
        btnDestino = (Button) ViewRoot.findViewById(R.id.btnTarifa);
        btnDestino.setOnClickListener(this);

        tvTipoServicio = (Button) ViewRoot.findViewById(R.id.btnTipoServicio);
        tvTipoServicio.setOnClickListener(this);

        SPCLI = new SPUser(getActivity());
        tiposServices = new SPUser(getActivity()).getTiposServicio();
        tiposdeservicio = new String[tiposServices.size()];
        tiposerviciosel = "" + tiposServices.get(0).id;

        int i = -1;
        for (JsonPack.TipoServicio t : tiposServices) {
            i++;
            tiposdeservicio[i] = t.nombre;
        }

        tvTipoServicio.setText(tiposServices.get(0).nombre);

        return ViewRoot;
    }

    JsonPack.ResponseLogin Sesion;

    public JsonPack.Zona ZonaOrigen = null;
    public JsonPack.Zona ZonaDestino = null;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).tvTitulo.setText(getString(R.string.tarifario));
        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        Sesion = new SPUser(getActivity()).getResponseLogin();
    }

    MyRequest<JsonPack.ResponseTrarifa> MRT = new MyRequest<JsonPack.ResponseTrarifa>(Urls.ws_consultar_tarifa, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.ResponseTrarifa object) {

            if (object != null) {
                if (object.status) {
                    tvTarifa.setText("S/. " + Tools.formatFloat(object.tarifa));
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.ruta_no_disponible), Toast.LENGTH_SHORT).show();
                    tvTarifa.setText("S/. ");
                }
            } else {
                Toast.makeText(getActivity(), getActivity().getString(R.string.ruta_no_disponible), Toast.LENGTH_SHORT).show();
                tvTarifa.setText("S/. ");
            }
        }
    };

    LatLng Origen;
    LatLng Destino;
    int currentLocZona = 1;

    public void concultarTarifa() {
        Log.e("concultarTarifa", ">");

        int flag_tipotarifa = ((AppForeGround) getActivity().getApplication()).flag_tipotarifa;

        if (flag_tipotarifa == 1) {

            if (idZonaOrigen != null && idZonaDestino != null) {
                if (idZonaOrigen.trim() != "" && idZonaDestino.trim() != "") {
                    JsonPack.ResponseLogin respLogin = SPCLI.getResponseLogin();

                    MRT
                            .putParams("tiposervicio_id", tiposerviciosel)
                            .putParams("empresa_id", respLogin.empresa_id)
                            /*.putParams("latitudOrigen", "" + Origen.latitude)
                            .putParams("longitudOrigen", "" + Origen.longitude)
                            .putParams("latitudDestino", "" + Destino.latitude)
                            .putParams("longitudDestino", "" + Destino.longitude)*/
                            .putParams("origen_id", "" + idZonaOrigen)
                            .putParams("destino_id", "" + idZonaDestino)
                            .send();
                }
            }

        } else if (flag_tipotarifa == 2) {
            if (Origen != null && Destino != null) {
                Log.i("tarifa 2", "origen destino ok");

                MR_DISTANCE.CancelarRequest();
                MR_DISTANCE.putParams("origins", Origen.latitude + "," + Origen.longitude)
                        .putParams("destinations", Destino.latitude + "," + Destino.longitude)
                        .putParams("mode", "driver")
                        .putParams("language", "es-PE")
                        .putParams("key", Urls.GOOGLE_KEY)
                        .send();
            } else {
                Log.i("tarifa 2", "origen destino null");
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    DialogDirecciones dlgDirecciones;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnOrigen:

                if (dlgDirecciones == null)
                    dlgDirecciones = new DialogDirecciones();

                dlgDirecciones.hintBusqueda = "Ingrese dirección de origen";
                dlgDirecciones.setTexoBusqueda(btnOrigen.getText().toString());
                dlgDirecciones.setTargetFragment(Frag_4_Tarifario.this, 1);
                dlgDirecciones.show(getFragmentManager(), "dirs");
                break;

            case R.id.btnTarifa:
                if (dlgDirecciones == null)
                    dlgDirecciones = new DialogDirecciones();

                dlgDirecciones.hintBusqueda = "Ingrese dirección de destino";
                dlgDirecciones.setTexoBusqueda(btnDestino.getText().toString());
                dlgDirecciones.setTargetFragment(Frag_4_Tarifario.this, 2);
                dlgDirecciones.show(getFragmentManager(), "dirs");

                break;
            case R.id.btnTipoServicio:
                new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        .setTitle(getString(R.string.app_empresa))
                        .setItems(tiposdeservicio, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                tvTipoServicio.setText(tiposdeservicio[which]);
                                tiposerviciosel = "" + tiposServices.get(which).id;
                                currentLocZona = 3;
                                concultarTarifa();
                            }
                        }).create().show();
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == 200) {
                    JsonPack.DirLug DL = (JsonPack.DirLug) data.getSerializableExtra("dir");
                    if (DL != null) {
                        btnOrigen.setText(DL.name);
                        ZonaOrigen = null;
                        currentLocZona = 1;
                        Origen = new LatLng(DL.lat, DL.lon);

                        if (getIdOri != null) {
                            if (getIdOri.getStatus() == AsyncTask.Status.RUNNING) {
                                getIdOri.cancel(true);
                            }
                        }

                        getIdOri = new getIdOrigen(new LatLng(DL.lat, DL.lon), Frag_3_Solicitud.ZZ);
                        getIdOri.execute();
                    }
                }
                break;
            case 2:
                if (resultCode == 200) {
                    JsonPack.DirLug DL = (JsonPack.DirLug) data.getSerializableExtra("dir");
                    if (DL != null) {
                        btnDestino.setText(DL.name);
                        ZonaDestino = null;
                        currentLocZona = 2;
                        Destino = new LatLng(DL.lat, DL.lon);

                        if (getIdDes != null) {
                            if (getIdDes.getStatus() == AsyncTask.Status.RUNNING) {
                                getIdDes.cancel(true);
                            }
                        }

                        getIdDes = new getIdDestino(new LatLng(DL.lat, DL.lon), Frag_3_Solicitud.ZZ);
                        getIdDes.execute();
                    }
                }
                break;
        }

        concultarTarifa();
    }

    MyRequest<JsonPack.GoogleDistanceMatrixResult> MR_DISTANCE = new MyRequest<JsonPack.GoogleDistanceMatrixResult>(Urls.google_distancematrix, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleDistanceMatrixResult object) {
            if (object != null) {
                if (object.rows != null) {
                    if (object.rows.size() > 0) {
                        if (object.rows.get(0).elements != null) {
                            if (object.rows.get(0).elements.size() > 0) {
                                if (object.rows.get(0).elements.get(0) != null && object.rows.get(0).elements.get(0).distance != null & object.rows.get(0).elements.get(0).duration != null) {
                                    MRT.CancelarRequest();
                                    MRT
                                            .putParams("tiposervicio_id", tiposerviciosel)
                                            .putParams("origen_id", "0")
                                            .putParams("destino_id", "0")
                                            .putParams("empresa_id", SPCLI.getResponseLogin().empresa_id)
                                            .putParams("distancia", object.rows.get(0).elements.get(0).distance.text.replace(" km", ""))//"3.4 km"
                                            .putParams("tiempo", object.rows.get(0).elements.get(0).duration.text.replace(" min", ""))//"13 min"
                                            .send();
                                } else {
                                    MRT.CancelarRequest();
                                    MRT
                                            .putParams("tiposervicio_id", tiposerviciosel)
                                            .putParams("origen_id", "0")
                                            .putParams("destino_id", "0")
                                            .putParams("empresa_id", SPCLI.getResponseLogin().empresa_id)
                                            .putParams("distancia", "0.0")//"3.4 km"
                                            .putParams("tiempo", "0")//"13 min"
                                            .send();
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    String idZonaOrigen = null;
    String idZonaDestino = null;

    private getIdOrigen getIdOri;
    private getIdDestino getIdDes;

    private class getIdOrigen extends AsyncTask<Void, Void, Void> {
        private LatLng point;
        private ArrayList<JsonPack.Zona> ZZ;

        public getIdOrigen(LatLng point, ArrayList<JsonPack.Zona> ZZ) {
            this.point = point;
            this.ZZ = ZZ;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Boolean hasId = false;

            for (JsonPack.Zona zona : ZZ) {
                ArrayList<LatLng> LatLngs = new ArrayList<>();
                ArrayList<JsonPack.Coordenada> coordenadas = zona.coordenadas;

                if (coordenadas.size() > 0) {
                    for (JsonPack.Coordenada coord : coordenadas) {
                        LatLngs.add(new LatLng(coord.latitud, coord.longitud));
                    }

                    if (LatLngs.size() > 0) {
                        hasId = new JsonPack.Coordenada().isInsidePolygon(LatLngs, point);

                        if (hasId) {
                            idZonaOrigen = zona.id;
                            Log.e("IdOrigen", "" + idZonaOrigen + ", Zona: " + zona.nombre.toUpperCase());
                            concultarTarifa();
                            break;
                        }
                    }
                }
                if (hasId) {
                    break;
                }
            }
            return null;
        }
    }


    private class getIdDestino extends AsyncTask<Void, Void, Void> {
        private LatLng point;
        private ArrayList<JsonPack.Zona> ZZ;

        public getIdDestino(LatLng point, ArrayList<JsonPack.Zona> ZZ) {
            this.point = point;
            this.ZZ = ZZ;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Boolean hasId = false;

            for (JsonPack.Zona zona : ZZ) {
                ArrayList<LatLng> LatLngs = new ArrayList<>();
                ArrayList<JsonPack.Coordenada> coordenadas = zona.coordenadas;

                if (coordenadas.size() > 0) {
                    for (JsonPack.Coordenada coord : coordenadas) {
                        LatLngs.add(new LatLng(coord.latitud, coord.longitud));
                    }

                    if (LatLngs.size() > 0) {
                        hasId = new JsonPack.Coordenada().isInsidePolygon(LatLngs, point);

                        if (hasId) {
                            idZonaDestino = zona.id;
                            Log.e("IdDestino", "" + idZonaDestino + ", Zona: " + zona.nombre.toUpperCase());
                            concultarTarifa();
                            break;
                        }
                    }
                }
                if (hasId) {
                    break;
                }
            }
            return null;
        }
    }
}

