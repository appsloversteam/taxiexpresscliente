package com.appslovers.taxiexpresscli.body;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appslovers.taxiexpresscli.data.IMyAdapter;
import com.appslovers.taxiexpresscli.data.IMyViewHolder;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 1/06/16.
 */

public class Frag_6_ServiciosHistorial extends Fragment implements IMyViewHolder {

    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.ServicioHist> Servicios;
    LinearLayoutManager llm;
    MyAdapter<Frag_6_ServiciosHistorial.HistorialViewHolder, JsonPack.ServicioHist> mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragservicioshistorial, null);
        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lista);
        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);
        return ViewRoot;
    }

    JsonPack.ResponseLogin rl;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        ((RootBody) getActivity()).tvTitulo.setText("Historial de Servicios");

        rl = new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("cliente_id", rl.cliente_id);
        MHR.send();
    }

    MyRequest MHR = new MyRequest(Urls.ws_historial_servicios, MyRequest.HttpRequestType.GET, "ISO-8859-1") {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            Servicios = null;
            try {
                JsonPack.ResponseHistorial RR = new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseHistorial.class);
                if (RR != null) {
                    if (RR.result != null && RR.result.size() > 0) {
                        Servicios = RR.result;
                        mAdapter = new MyAdapter<>(Servicios, Frag_6_ServiciosHistorial.this, R.layout.fraghistorial_item);
                        rvLista.setAdapter(mAdapter);
                    }
                }
            } catch (JsonParseException e) {
                Log.e("json ex", ">" + e.getMessage());
            }
        }
    };

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new HistorialViewHolder(v);
    }

    public class HistorialViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<HistorialViewHolder> {

        TextView tvFecha, tvHora, tvOrigen, tvDestino, tvCosto;

        public HistorialViewHolder(View v) {
            super(v);
            tvFecha = (TextView) v.findViewById(R.id.tvFecha);
            tvHora = (TextView) v.findViewById(R.id.tvHora);
            tvOrigen = (TextView) v.findViewById(R.id.tvOrigen);
            tvDestino = (TextView) v.findViewById(R.id.tvDestino);
            tvCosto = (TextView) v.findViewById(R.id.tvCosto);
        }
        
        @Override
        public void bindView(HistorialViewHolder holder, int position) {
            holder.tvDestino.setText(Servicios.get(position).direccion_destino);
            holder.tvOrigen.setText(Servicios.get(position).direccion_origen);
            holder.tvCosto.setText("S/. " + Servicios.get(position).tarifa);
            holder.tvFecha.setText(Servicios.get(position).fecha);
            holder.tvHora.setText(Servicios.get(position).hora);
        }
    }
}
