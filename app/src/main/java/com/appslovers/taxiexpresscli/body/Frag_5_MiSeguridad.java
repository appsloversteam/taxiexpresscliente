package com.appslovers.taxiexpresscli.body;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_5_MiSeguridad extends Fragment implements View.OnClickListener {
    View ViewRoot;
    Button btnSi, btnNo;
    EditText etCorreo, etNombre;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragmiseguridad, null);
        etCorreo = (EditText) ViewRoot.findViewById(R.id.etCorreo);
        etNombre = (EditText) ViewRoot.findViewById(R.id.etNombre);
        btnSi = (Button) ViewRoot.findViewById(R.id.btnSi);
        btnSi.setOnClickListener(this);
        btnNo = (Button) ViewRoot.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(this);

        return ViewRoot;
    }

    String temptitulo = "";
    SPUser SPCLI;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        temptitulo = ((RootBody) getActivity()).tvTitulo.getText().toString();
        ((RootBody) getActivity()).tvTitulo.setText("Mi Seguridad");
        SPCLI = new SPUser(getActivity());
    }

    @Override
    public void onDetach() {
        ((RootBody) getActivity()).tvTitulo.setText(temptitulo);
        super.onDetach();
    }

    MyRequest<JsonPack.RespMiSeguridad> MR_GET_MISEGURIDAD = new MyRequest<JsonPack.RespMiSeguridad>(Urls.ws_obtener_datos_seguridad, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespMiSeguridad object) {

            if (object.status) {
                etNombre.setText(object.nombre);
                etCorreo.setText(object.correo);
                estado = object.estado;

                if (estado.equals("1")) {
                    btnSi.setTextColor(Color.WHITE);
                    btnSi.setEnabled(false);

                    btnNo.setTextColor(Color.BLACK);
                    btnNo.setEnabled(true);

                } else if (estado.equals("0")) {
                    btnSi.setTextColor(Color.BLACK);
                    btnSi.setEnabled(true);

                    btnNo.setTextColor(Color.WHITE);
                    btnNo.setEnabled(false);
                }
            }
        }
    };

    MyRequest MR_SAVE_MISEGURIDAD = new MyRequest(Urls.ws_guardar_datos_seguridad, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, Object object) {

        }
    };

    @Override
    public void onStart() {
        super.onStart();

        MR_GET_MISEGURIDAD
                .putParams("id", SPCLI.getResponseLogin().cliente_id)
                .send();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (estado != null) {
            if (SPCLI.getResponseLogin() != null) {
                MR_SAVE_MISEGURIDAD
                        .putParams("nombre", etNombre.getText().toString())
                        .putParams("correo", etCorreo.getText().toString())
                        .putParams("status", estado)
                        .putParams("id", SPCLI.getResponseLogin().cliente_id)
                        .send();
            }
        }
    }

    String estado;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSi:
                estado = "1";
                btnSi.setTextColor(Color.WHITE);
                btnSi.setEnabled(false);
                btnNo.setTextColor(Color.BLACK);
                btnNo.setEnabled(true);
                break;
            case R.id.btnNo:
                estado = "0";
                btnSi.setTextColor(Color.BLACK);
                btnSi.setEnabled(true);
                btnNo.setTextColor(Color.WHITE);
                btnNo.setEnabled(false);
                break;
        }

    }
}
