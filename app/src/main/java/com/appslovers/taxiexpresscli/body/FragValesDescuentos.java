package com.appslovers.taxiexpresscli.body;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.data.IMyAdapter;
import com.appslovers.taxiexpresscli.data.IMyViewHolder;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.dialogs.DialogCanjeRealizado;
import com.appslovers.taxiexpresscli.dialogs.DialogConfirmarCanje;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by yfarina on 3/16/17.
 */

public class FragValesDescuentos extends Fragment implements IMyViewHolder {
    public static FragCanjes nuevo(int puntos) {
        FragCanjes fc = new FragCanjes();
        fc.Puntos = puntos;
        return fc;
    }

    View ViewRoot;
    RecyclerView rvLista;
    ArrayList<JsonPack.ValeElectronico> promociones;
    LinearLayoutManager llm;
    MyAdapter<FragValesDescuentos.ValeViewHolder, JsonPack.ValeElectronico> mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.frag_vales_list, null);
        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lstLista);
        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);

        return ViewRoot;
    }


    public int Puntos;
    JsonPack.ResponseLogin rl;
    DialogConfirmarCanje DCC;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).tvTitulo.setText("Vales");
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);

        rl = new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("cliente_id", rl.cliente_id).putParams("tipo","1");
        MHR.send();

        DCC = new DialogConfirmarCanje();
        DCC.setTargetFragment(FragValesDescuentos.this, 333);
    }

    ProgressDialog pdProcesando;
    JsonPack.Promocion SelPro;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //Toast.makeText(getContext(),requestCode+" - "+resultCode,Toast.LENGTH_SHORT).show();
        /*switch (requestCode) {
            case 333:

                if (resultCode == 200) {
                    if (SelPro != null) {
                        if (pdProcesando == null)
                            pdProcesando = ProgressDialog.show(getActivity(), getString(R.string.app_empresa), "Procesando canje...", true, false);
                        else {
                            pdProcesando.setMessage("Procesando canje...");
                            pdProcesando.show();
                        }

                        MHR_canjear.putParams("cliente_id", rl.cliente_id);
                        MHR_canjear.putParams("promo_id", SelPro.promo_id);
                        MHR_canjear.send();
                    }

                }
                break;
        }*/
    }

    MyRequest MHR = new MyRequest(Urls.w_vales, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            promociones = null;
            try {

                //promociones=new Gson().fromJson(rw.ResponseBody,  new TypeToken<ArrayList<JsonPack.Promocion>>() {}.getType());
                JsonPack.ResponseListaVales rp = new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseListaVales.class);
                if (rp != null && rp.data != null) {
                    promociones = rp.data;

                    mAdapter = new MyAdapter<>(promociones, FragValesDescuentos.this, R.layout.frag_vales_item);

                    rvLista.setAdapter(mAdapter);

                    ViewRoot.findViewById(R.id.sinValesTv).setVisibility(View.GONE);
                }

            } catch (JsonParseException e) {
                Log.e("json ex", ">" + e.getMessage());
            }
        }
    };


    MyRequest MHR_canjear = new MyRequest(Urls.ws_canjear_beneficios, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            if (pdProcesando != null)
                pdProcesando.cancel();

            SelPro = null;

            JsonPack.ResponseGeneric rg;
            try {

                rg = new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseGeneric.class);
                if (rg.status) {

                    MHR_puntos.putParams("cliente_id", rl.cliente_id).send();
                    new DialogCanjeRealizado().show(getFragmentManager(), "realizado");
                } else {
                    Toast.makeText(getActivity(), "Error en el canje Realizado", Toast.LENGTH_SHORT).show();
                }
            } catch (JsonParseException e) {
                Log.e("json ex", ">" + e.getMessage());
                Toast.makeText(getActivity(), "Error en el canje Realizado", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailedRequest(ResponseWork rw, boolean isActive) {

            if (pdProcesando != null)
                pdProcesando.cancel();

            SelPro = null;
            Toast.makeText(getContext(), "Error en el canje Realizado", Toast.LENGTH_SHORT).show();
        }
    };

    MyRequest<JsonPack.RespPuntos> MHR_puntos = new MyRequest<JsonPack.RespPuntos>(Urls.ws_obtener_puntos, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespPuntos object) {
            if (object != null) {

                Puntos = object.puntos;
                //mAdapter.notifyDataSetChanged();
                MHR.putParams("cliente_id", rl.cliente_id);
                MHR.send();
            }
        }
    };

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new FragValesDescuentos.ValeViewHolder(v);
    }

    public class ValeViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<FragValesDescuentos.ValeViewHolder>, View.OnClickListener {


        TextView tvMontoVale, tvCaduca, tvDescripcion, tvCodigo;
        Button btnSeleccionar;

        public ValeViewHolder(View v) {
            super(v);
            tvMontoVale=(TextView)v.findViewById(R.id.tvMontoVale);
            tvCaduca=(TextView)v.findViewById(R.id.tvCaduca);
            tvDescripcion=(TextView)v.findViewById(R.id.tvDescripcion);
            tvCodigo=(TextView)v.findViewById(R.id.tvCodigoVale);
            btnSeleccionar=(Button)v.findViewById(R.id.button3);
            btnSeleccionar.setOnClickListener(this);

        }


        @Override
        public void bindView(FragValesDescuentos.ValeViewHolder holder, int position) {
            JsonPack.ValeElectronico vale=promociones.get(position);

            DecimalFormat df=new DecimalFormat("###,###.##");
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);

            tvMontoVale.setText("S/. "+df.format(Float.parseFloat(vale.vale_monto)));
            tvDescripcion.setText(vale.vale_descripcion);
            tvCaduca.setText(vale.vale_fechafinal);
            tvCodigo.setText(vale.vale_codigo);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button3:
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        }
    }


}