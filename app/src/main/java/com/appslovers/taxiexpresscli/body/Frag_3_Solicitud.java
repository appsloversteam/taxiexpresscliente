package com.appslovers.taxiexpresscli.body;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.dialogs.DialogDirsFavRes;
import com.appslovers.taxiexpresscli.dialogs.DialogExpreso;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.MapStateListener;
import com.appslovers.taxiexpresscli.util.NonSwipeableViewPager;
import com.appslovers.taxiexpresscli.util.TouchableMapFragment;
import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 13/07/16.
 */
public class Frag_3_Solicitud extends TouchableMapFragment implements View.OnClickListener {

    View ViewRoot;
    LinearLayout llContenedor;
    public ScreenSlidePagerAdapter mPagerAdapter;
    public NonSwipeableViewPager Paginador;
    ArrayList<IPageFragment> frags;
    InputMethodManager IMM;
    public ImageView ivMiUbicacion;

    DialogDirsFavRes DDIR;
    JsonPack.ResponseLogin Sesion;
    public JsonPack.Servicio Servicio = new JsonPack.Servicio();
    public static ArrayList<JsonPack.Zona> ZZ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        IMM = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        ViewRoot = inflater.inflate(R.layout.fragsolicitud, null);
        Sesion = new SPUser(getActivity()).getResponseLogin();

        ivMiUbicacion = (ImageView) ViewRoot.findViewById(R.id.ivMiUbicacion);

        llContenedor = (LinearLayout) ViewRoot.findViewById(R.id.contenedor);
        llContenedor.addView(super.onCreateView(inflater, container, savedInstanceState), 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Paginador = (NonSwipeableViewPager) ViewRoot.findViewById(R.id.pager);
        Paginador.setOffscreenPageLimit(3);

        this.ZZ = ((AppForeGround) getActivity().getApplication()).getDb().getZonas();

        frags = new ArrayList<>();
        frags.add(FragSolicitudInicio.getInstance(this, this));
        frags.add(FragSolicitudDestino.getInstance(this, this));
        frags.add(FragSolicitudBuscando.getInstance(this, this));

        Paginador.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                Log.v("onPageSelect", ">" + position);
                frags.get(position).onThisCurrent();

                if (beforeselectedid == -1) {
                    beforeselectedid = position;
                } else {
                    if (position != beforeselectedid) {
                        Log.e("unSelect", "OK ------------------>" + beforeselectedid);
                        frags.get(beforeselectedid).unSelected();
                        beforeselectedid = position;
                    } else {
                        Log.e("unSelect", "false ------------------");
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mPagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager(), frags);
        Paginador.setAdapter(mPagerAdapter);

        return ViewRoot;
    }

    public int beforeselectedid = -1;

    public static final int reqCodeOrigen = 1;
    public static final int reqCodeDestino = 2;

    public void MostrarDialogDirecciones(String s, String h, int reqCod, Fragment f) {
        if (DDIR == null)
            DDIR = new DialogDirsFavRes();

        DDIR.setTexoBusqueda(s);
        DDIR.setTargetFragment(f, reqCod);
        DDIR.show(getFragmentManager(), "dirs");
    }

    DialogExpreso DEXP;

    public void MostrarConfimacion(String s, int reqCod, Fragment f) {

        if (DEXP == null) {
            DEXP = new DialogExpreso();
            DEXP.setTargetFragment(f, reqCod);
        }
        
        if (!DEXP.isAdded()) {
            if (DEXP.getDialog() == null) {
                DEXP.show(getFragmentManager(), s);
            } else if (!DEXP.getDialog().isShowing()) {
                DEXP.show(getFragmentManager(), s);
            }
        }
    }


    GoogleMap MAPA;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            ((RootBody) getActivity()).tvTitulo.setText(getString(R.string.app_empresa));
            ((RootBody) getActivity()).flRutasFrec.setVisibility(View.VISIBLE);
            ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
            ((RootBody) getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_grisconborde4r);
            ((RootBody) getActivity()).Emergencia.setEnabled(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case 12:
                if (getActivity() != null) {
                    if (((RootBody) getActivity()).tvTitulo != null) {
                        ((RootBody) getActivity()).tvTitulo.setText(getString(R.string.app_empresa));
                        if (resultCode == 200) {
                            if (((RootBody) getActivity()).RUTAFRECUENTETEMP != null) {
                                Paginador.setCurrentItem(1);
                            }
                        }
                    }
                }
                break;
        }
    }


    public int retrocedera = 1;

    public int getRetrocederAlFrag() {
        return retrocedera;
    }

    public void EmpezarSolicitud() {
        Paginador.setCurrentItem(2);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnExpreso:
                retrocedera = 0;
                ((FragSolicitudInicio) frags.get(0)).saveInfo();
                Paginador.setCurrentItem(2);
                break;
            case R.id.btnDestino:
                ((FragSolicitudInicio) frags.get(0)).saveInfo();
                retrocedera = 1;
                Paginador.setCurrentItem(1);
                break;
        }
    }

    public static class ScreenSlidePagerAdapter extends FragmentPagerAdapter//FragmentStatePagerAdapter3
    {
        ArrayList<IPageFragment> fragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, ArrayList<IPageFragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position).getFragment();
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

    IMapListen IML;
    MapStateListener MSL;

    public void setListenerMapa(IMapListen iml) {

        IML = iml;
        if (MAPA != null) {
            if (MSL == null) {
                MSL = new MapStateListener(MAPA, this, getActivity()) {
                    @Override
                    public void onMapTouched() {

                        if (IML != null) IML.onMapTouched();
                    }

                    @Override
                    public void onMapReleased() {
                        if (IML != null) IML.onMapReleased();
                    }

                    @Override
                    public void onMapUnsettled() {
                        if (IML != null) IML.onMapUnsettled();
                    }

                    @Override
                    public void onMapSettled() {
                        if (IML != null) IML.onMapSettled();
                    }
                };
            }

        }


    }

    public interface IMapListen {
        void onMapTouched();

        void onMapReleased();

        void onMapUnsettled();

        void onMapSettled();
    }



    public interface IPageFragment {

        void unSelected();

        void onThisCurrent();

        Fragment getFragment();
    }
}
