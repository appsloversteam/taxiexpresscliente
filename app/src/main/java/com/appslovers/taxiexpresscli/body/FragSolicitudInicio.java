package com.appslovers.taxiexpresscli.body;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyGps;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.Tools;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 21/07/16.
 */
public class FragSolicitudInicio extends Fragment implements Frag_3_Solicitud.IPageFragment, View.OnClickListener {
    @Override
    public Fragment getFragment() {
        return this;
    }

    View.OnClickListener OCL;
    Frag_3_Solicitud FSOL;

    private GestureDetector mDetector;

    public static FragSolicitudInicio getInstance(View.OnClickListener ocl, Frag_3_Solicitud fsol) {
        FragSolicitudInicio f = new FragSolicitudInicio();
        f.OCL = ocl;
        f.FSOL = fsol;
        return f;
    }

    boolean flagMyZoom = false;
    EditText etReferenciaOrigen;
    Button btnDireccion1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragsolicitud_inicio, container, false);
        Log.i("Inicio", "onCreateView");
        etReferenciaOrigen = (EditText) rootView.findViewById(R.id.etReferenciaOrigen);
        rootView.findViewById(R.id.btnDestino).setOnClickListener(OCL);
        //rootView.findViewById(R.id.btnExpreso).setOnClickListener(OCL);
        rootView.findViewById(R.id.btnExpreso).setOnClickListener(this);
        btnDireccion1 = (Button) rootView.findViewById(R.id.btnDireccion1);
        btnDireccion1.setOnClickListener(this);

        ((RootBody) getActivity()).GPS.setIMyGps(new MyGps.IMyGps() {
            @Override
            public void onLocation(Location l) {
                FSOL.MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(l.getLatitude(), l.getLongitude()), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));

                if (getActivity() != null) {
                    if (((RootBody) getActivity()).GPS != null) {
                        ((RootBody) getActivity()).GPS.turnOff();
                    }
                }
            }

            @Override
            public void onOldLocation(Location l) {
            }

            @Override
            public void onEstaApagado() {
            }

            @Override
            public void onFueApagado() {
            }
        });


        //mDetector = new GestureDetector(getActivity(), new Frag_3_Solicitud.MyGestureDetector()
        mDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                Log.d("onSingleTapUp", ">");
                ((RootBody) getActivity()).GPS.turnOn(true);
                return true;
            }


            @Override
            public boolean onDoubleTap(MotionEvent e) {
                float zoom = FSOL.MAPA.getCameraPosition().zoom;
                if (flagMyZoom) {
                    if (zoom < FSOL.MAPA.getMaxZoomLevel()) {
                        zoom += 0.5;
                        FSOL.MAPA.moveCamera(CameraUpdateFactory.zoomTo(zoom));
                    }
                } else {
                    if (zoom > FSOL.MAPA.getMinZoomLevel()) {
                        zoom -= 0.5;
                        FSOL.MAPA.moveCamera(CameraUpdateFactory.zoomTo(zoom));
                    }
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                flagMyZoom = !flagMyZoom;
                super.onLongPress(e);
            }

        });

        rootView.findViewById(R.id.btnMyLocation).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mDetector.onTouchEvent(event);
            }
        });


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("Inicio", "onViewCreated");

        if (getString(R.string.app_empresa).equals("AWB")) {
            ((RootBody) getActivity()).tvTitulo.setText(getString(R.string.app_empresa) + " inicio");
        }
        //onThisCurrent();

        FSOL.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                FSOL.MAPA = googleMap;
                ((RootBody) getActivity()).GPS.turnOn(true);

                FSOL.setListenerMapa(LISTEN);
            }
        });
    }

    Frag_3_Solicitud.IMapListen LISTEN = new Frag_3_Solicitud.IMapListen() {
        @Override
        public void onMapTouched() {
            Log.d("onMapTouched", "--------------->>>>>>>>>>");
            FSOL.ivMiUbicacion.getDrawable().setAlpha(100);
            FSOL.ivMiUbicacion.refreshDrawableState();
        }

        @Override
        public void onMapReleased() {
            Log.d("onMapEeleased", "--------------->>>>>>>>>>");
        }

        @Override
        public void onMapUnsettled() {
            Log.d("onMapunsettled", "--------------->>>>>>>>>>");
        }

        @Override
        public void onMapSettled() {
            FSOL.Servicio.OrigenLat = FSOL.MAPA.getCameraPosition().target.latitude;
            FSOL.Servicio.OrigenLng = FSOL.MAPA.getCameraPosition().target.longitude;
            consultarDireccion(FSOL.MAPA.getCameraPosition().target);
            buscarIdZona(new LatLng(FSOL.MAPA.getCameraPosition().target.latitude, FSOL.MAPA.getCameraPosition().target.longitude));

            Log.d("onMapSettled", "--------------->>>>>>>>>>");
        }
    };

    @Override
    public void unSelected() {

        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.VISIBLE);
    }


    @Override
    public void onThisCurrent() {
        if (getString(R.string.app_empresa).equals("AWB")) {
            ((RootBody) getActivity()).tvTitulo.setText(getString(R.string.app_empresa) + " inicio");
        }
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.VISIBLE);

        FSOL.setListenerMapa(LISTEN);
        if (FSOL.Servicio.OrigenLat != 0 && FSOL.Servicio.OrigenLng != 0) {
            flagQueryDir = false;
            FSOL.MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(FSOL.Servicio.OrigenLat, FSOL.Servicio.OrigenLng), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));
        }
        FSOL.IMM.hideSoftInputFromWindow(etReferenciaOrigen.getWindowToken(), 0);


    }

    boolean flagQueryDir = true;
    MyRequest<JsonPack.GoogleGeoCode> MyRequestJsonPackGoogleGeoCode;

    public void consultarDireccion(LatLng target) {

        if (flagQueryDir) {
            FSOL.ivMiUbicacion.setImageResource(R.drawable.ic_cliente_standby);
            FSOL.ivMiUbicacion.getDrawable().setAlpha(255);
            FSOL.ivMiUbicacion.refreshDrawableState();


            if (MyRequestJsonPackGoogleGeoCode == null) {
                MyRequestJsonPackGoogleGeoCode = new MyRequest<JsonPack.GoogleGeoCode>(null, Urls.google_inversegeocode, MyRequest.HttpRequestType.GET) {
                    @Override
                    public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleGeoCode object) {

                        if (object.results.size() > 0) {
                            if (object.results.get(0).address_components != null) {

                                String snum = "";
                                String snom = "";
                                String sdistrict="";
                                for (JsonPack.GoogleGeoCodeAddressComponent ac : object.results.get(0).address_components) {
                                    if (ac.types != null) {
                                        if (ac.types.size() > 0) {
                                            if (ac.types.get(0).contains("street_number")) {
                                                snum = ac.short_name;
                                            }
                                            if (ac.types.get(0).contains("route")) {
                                                snom = ac.long_name;
                                            }
                                            if (ac.types.get(0).contains("locality")) {
                                                sdistrict = ac.long_name;
                                            }
                                        }
                                    }

                                }
                                btnDireccion1.setText((snom + " " + snum + " " + sdistrict).trim());
                            }
                        }

                        FSOL.ivMiUbicacion.setImageResource(R.drawable.ic_cliente);
                        FSOL.ivMiUbicacion.getDrawable().setAlpha(255);
                        FSOL.ivMiUbicacion.refreshDrawableState();
                        concultarTarifa();
                    }

                    @Override
                    public void onFailedRequest(ResponseWork rw, boolean isActive) {

                        FSOL.ivMiUbicacion.setImageResource(R.drawable.ic_cliente);
                        FSOL.ivMiUbicacion.getDrawable().setAlpha(255);
                        FSOL.ivMiUbicacion.refreshDrawableState();
                        concultarTarifa();
                    }
                };
            }
            MyRequestJsonPackGoogleGeoCode.CancelarRequest();
            MyRequestJsonPackGoogleGeoCode.putParams("latlng", target.latitude + "," + target.longitude)
                    .putParams("sensor", "false")
                    .putParams("key", Urls.GOOGLE_KEY)
                    .setShowLogs(false)
                    .send();
        } else {
            flagQueryDir = true;
            concultarTarifa();
        }
    }

    JsonPack.ResponseLogin RL;

    public void saveInfo() {
        //corregir
        if (RL == null) {
            RL = new SPUser(getActivity()).getResponseLogin();
        }
        FSOL.Servicio.IdCliente = RL.cliente_id;
        FSOL.Servicio.OrigenReferencia = etReferenciaOrigen.getText().toString();
        FSOL.Servicio.OrigenDireccion = btnDireccion1.getText().toString();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        Log.i("onACtivity", ">" + requestCode + " , " + resultCode);
        switch (requestCode) {
            case Frag_3_Solicitud.reqCodeOrigen:

                if (resultCode == 200) {
                    JsonPack.DirLug DL = (JsonPack.DirLug) data.getSerializableExtra("dir");
                    if (DL.direccion==null){
                        DL.direccion="";
                    }
                    flagQueryDir = false;
                    FSOL.MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(DL.lat, DL.lon), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));
                    btnDireccion1.setText(DL.name + DL.direccion);

                    buscarIdZona(new LatLng(DL.lat, DL.lon));
                }
                break;

            case 4321:
                if (resultCode == 200) {
                    pasaraExpreso();
                }

                break;
        }
    }

    private void buscarIdZona(LatLng point) {
        if (getId != null) {
            if (getId.getStatus() == AsyncTask.Status.RUNNING) {
                getId.cancel(true);
            }
        }

        getId = new getIdOrigen(point, Frag_3_Solicitud.ZZ);
        getId.execute();
    }

    public void concultarTarifa() {
        if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 1) {

            if (FSOL.Servicio.OrigenId != null && FSOL.Servicio.DestinoId != null) {
                if (FSOL.Servicio.OrigenId.trim() != "" && FSOL.Servicio.DestinoId.trim() != "") {
                    MRT
                            .putParams("tiposervicio_id", "" + FSOL.Servicio.TipoServicio)
                            .putParams("empresa_id", FSOL.Sesion.empresa_id)
                            .putParams("origen_id", "" + FSOL.Servicio.OrigenId)
                            .putParams("destino_id", "" + FSOL.Servicio.DestinoId)
                            .send();
                }
            }
        } else if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 2) {
            if (FSOL.Servicio.OrigenLat != 0.0d && FSOL.Servicio.OrigenLng != 0.0d && FSOL.Servicio.DestinoLat != 0.0d && FSOL.Servicio.DestinoLng != 0.0d) {
                MR_DISTANCE.CancelarRequest();
                MR_DISTANCE.putParams("origins", FSOL.Servicio.OrigenLat + "," + FSOL.Servicio.OrigenLng)
                        .putParams("destinations", FSOL.Servicio.DestinoLat + "," + FSOL.Servicio.DestinoLng)
                        .putParams("mode", "driver")
                        .putParams("language", "es-PE")
                        .putParams("key", Urls.GOOGLE_KEY)
                        .send();
            }
        }

    }

    MyRequest<JsonPack.ResponseTrarifa> MRT = new MyRequest<JsonPack.ResponseTrarifa>(Urls.ws_consultar_tarifa, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.ResponseTrarifa object) {

            if (object.status) {
                FSOL.Servicio.Tarifa = "" + object.tarifa;
            }
        }
    };
    MyRequest<JsonPack.GoogleDistanceMatrixResult> MR_DISTANCE = new MyRequest<JsonPack.GoogleDistanceMatrixResult>(Urls.google_distancematrix, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleDistanceMatrixResult object) {

            if (object != null) {
                if (object.rows != null) {
                    if (object.rows.size() > 0) {
                        if (object.rows.get(0).elements != null) {
                            if (object.rows.get(0).elements.size() > 0) {
                                if (object.rows.get(0).elements.get(0) != null && object.rows.get(0).elements.get(0).distance != null & object.rows.get(0).elements.get(0).duration != null) {
                                    MRT.CancelarRequest();
                                    MRT
                                            .putParams("tiposervicio_id", "" + FSOL.Servicio.TipoServicio)
                                            //.putParams("origen_id", FSOL.Servicio.OrigenId)
                                            //.putParams("destino_id", FSOL.Servicio.DestinoId)
                                            .putParams("empresa_id", FSOL.Sesion.empresa_id)
                                            .putParams("distancia", object.rows.get(0).elements.get(0).distance.text.replace(" km", ""))//"3.4 km"
                                            .putParams("tiempo", object.rows.get(0).elements.get(0).duration.text.replace(" min", ""))//"13 min"
                                            .send();

                                } else {
                                    MRT.CancelarRequest();
                                    MRT
                                            .putParams("tiposervicio_id", "" + FSOL.Servicio.TipoServicio)
                                            //.putParams("origen_id", FSOL.Servicio.OrigenId)
                                            //.putParams("destino_id", FSOL.Servicio.DestinoId)
                                            .putParams("empresa_id", FSOL.Sesion.empresa_id)
                                            .putParams("distancia", "0.0")
                                            .putParams("tiempo", "0")
                                            .send();
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    public String[] TiposPago=new String[]{"Efectivo", "Vale", "Tarjeta VISA"};

    @Override
    public void onStop() {
        super.onStop();
    }

    ArrayList<JsonPack.ValeElectronico> promociones;

    MyRequest MHR = new MyRequest(Urls.w_vales, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            promociones = null;
            try {
                //promociones=new Gson().fromJson(rw.ResponseBody,  new TypeToken<ArrayList<JsonPack.Promocion>>() {}.getType());
                JsonPack.ResponseListaVales rp = new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseListaVales.class);
                if (rp != null && rp.data != null) {
                    if (rp.status){
                        //tvTipoPago.setText(getTipoPago("2"));
                        promociones = rp.data;
                        for (int i=0; i<promociones.size(); i++){
                            new SPUser(getActivity()).setCodigoVale(promociones.get(i).vale_codigo);
                            //tvTipoPago.setText(getTipoPago("2"));
                            FSOL.Servicio.TipoPago = "2";
                            FSOL.Paginador.setCurrentItem(2);
                        }
                    }

                }

            } catch (JsonParseException e) {
                Toast.makeText(getActivity(), "No se encontraron vales disponibles", Toast.LENGTH_SHORT).show();
                //tvTipoPago.setText(getTipoPago("1"));
                Log.e("json ex", ">" + e.getMessage());
            }
        }
    };





    public void pasaraExpreso() {
        FSOL.retrocedera = 0;
        saveInfo();

        new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setTitle(getString(R.string.app_empresa))
                .setItems(TiposPago, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch(which){
                            case 0:
                                FSOL.Servicio.TipoPago = "" + (which + 1);
                                FSOL.Paginador.setCurrentItem(2);
                                break;
                            case 1:
                                MHR.putParams("cliente_id",FSOL.Servicio.IdCliente).putParams("tipo","2").send();
                                break;
                            case 2:
                                //Toast.makeText(getActivity(), "El pago con tarjeta de crédito estará pronto disponible", Toast.LENGTH_SHORT).show();
                                //tvTipoPago.setText(getTipoPago("" + (which + 1)));
                                FSOL.Servicio.TipoPago = "" + (which + 1);
                                FSOL.Paginador.setCurrentItem(2);
                        }

                        //tvTipoPago.setText(getTipoPago("" + (which + 1)));
                        //FSOL.Servicio.TipoPago = "" + (which + 1);


                        concultarTarifa();


                    }
                }).create().show();





    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnDireccion1:
                FSOL.MostrarDialogDirecciones(btnDireccion1.getText().toString(), "Ingrese dirección de origen", Frag_3_Solicitud.reqCodeOrigen, FragSolicitudInicio.this);
                break;

            case R.id.btnExpreso:

                if (FSOL.Servicio.OrigenId != null) {
                    if (FSOL.Servicio.OrigenId != "0") {
                        FSOL.Servicio.DestinoId = "";
                        FSOL.Servicio.DestinoDireccion = "";
                        FSOL.Servicio.DestinoLat = 0.0d;
                        FSOL.Servicio.DestinoLng = 0.0d;
                        FSOL.Servicio.DestinoReferencia = "";
                        FSOL.Servicio.DestinoNombre = "";

                        if (new SPUser(getActivity()).getExpreso()) {
                            FSOL.MostrarConfimacion("fdsfsdfdsf", 4321, this);
                        } else {
                            pasaraExpreso();
                        }
                    } else {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.zona_desconocida), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.zona_desconocida), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private getIdOrigen getId;

    private class getIdOrigen extends AsyncTask<Void, Void, Void> {
        private LatLng point;
        private ArrayList<JsonPack.Zona> ZZ;

        public getIdOrigen(LatLng point, ArrayList<JsonPack.Zona> ZZ) {
            this.point = point;
            this.ZZ = ZZ;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Boolean hasId = false;
            FSOL.Servicio.OrigenId = "0";
            String nombre_zona = "";

            for (JsonPack.Zona zona : ZZ) {
                ArrayList<LatLng> LatLngs = new ArrayList<>();
                ArrayList<JsonPack.Coordenada> coordenadas = zona.coordenadas;

                if (coordenadas.size() > 0) {
                    for (JsonPack.Coordenada coord : coordenadas) {
                        LatLngs.add(new LatLng(coord.latitud, coord.longitud));
                    }

                    if (LatLngs.size() > 0) {
                        hasId = new JsonPack.Coordenada().isInsidePolygon(LatLngs, point);

                        if (hasId) {
                            FSOL.Servicio.OrigenId = zona.id;
                            nombre_zona = zona.nombre;
                            break;
                        }
                    }
                }
                if (hasId) {
                    break;
                }
            }

            if (FSOL.Servicio.OrigenId.equals("0")) {
                Log.e("IdOrigen", "" + FSOL.Servicio.OrigenId + ", Zona no encontrada");
            } else {
                Log.e("IdOrigen", "" + FSOL.Servicio.OrigenId + ", Zona: " + nombre_zona.toUpperCase());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //consultarDireccion(FSOL.MAPA.getCameraPosition().target);
            concultarTarifa();
        }
    }
}