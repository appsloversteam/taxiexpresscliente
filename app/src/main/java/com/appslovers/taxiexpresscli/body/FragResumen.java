package com.appslovers.taxiexpresscli.body;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appslovers.taxiexpresscli.R;

/**
 * Created by javierquiroz on 31/05/16.
 */
public class FragResumen extends Fragment implements View.OnClickListener{

    View ViewRoot;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot=inflater.inflate(R.layout.fragresumen,null);
        ViewRoot.findViewById(R.id.btnAprobacion).setOnClickListener(this);

        return ViewRoot;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody)getActivity()).tvTitulo.setText("Resumen de servicio");
        ((RootBody)getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody)getActivity()).flReservasAdd.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAprobacion:

                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_root_inside, new FragCalificacion(), FragCalificacion.class.getSimpleName())
                        .addToBackStack("vb")
                        .commit();
                break;
        }
    }
}
