package com.appslovers.taxiexpresscli.body;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.appslovers.taxiexpresscli.data.IMyAdapter;
import com.appslovers.taxiexpresscli.data.IMyViewHolder;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.util.MyFragment;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 21/07/16.
 */
public class Frag_7_RutasFrecuentes extends MyFragment implements IMyViewHolder {

    public static Frag_7_RutasFrecuentes getFrag(int tipo) {
        Frag_7_RutasFrecuentes f = new Frag_7_RutasFrecuentes();
        f.tipo = tipo;
        return f;
    }

    RecyclerView rvLista;
    ArrayList<JsonPack.RutaFrec> Rutas;
    LinearLayoutManager llm;
    MyAdapter<Frag_7_RutasFrecuentes.RutasViewHolder, JsonPack.RutaFrec> mAdapter;
    TextView tvRutas;
    public int tipo = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragrutasfrec, null);
        tvRutas = (TextView) ViewRoot.findViewById(R.id.tvRutas);
        rvLista = (RecyclerView) ViewRoot.findViewById(R.id.lista);
        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);
        return ViewRoot;// super.onCreateView(inflater, container, savedInstanceState);
    }

    JsonPack.ResponseLogin rl;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).tvTitulo.setText("Rutas Frecuentes");
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);

        rl = new SPUser(getActivity()).getResponseLogin();
        MHR.putParams("cliente_id", rl.cliente_id);
        MHR.send();
    }

    boolean flag_borrar = true;
    MyRequest MHR_BORRAR = new MyRequest(Urls.ws_eliminar_ruta_frecuente, MyRequest.HttpRequestType.GET) {

        @Override
        public void onSuccesForeground(ResponseWork rw) {
            flag_borrar = true;
        }

        @Override
        public void onFailedRequestForeground(ResponseWork rw) {
            flag_borrar = true;
        }
    };

    MyRequest<JsonPack.RespRutas> MHR = new MyRequest<JsonPack.RespRutas>(Urls.ws_mostrar_ruta_frecuente, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespRutas object) {
            if (object.result != null) {
                if (object.result.size() > 0) {
                    Rutas = object.result;
                    mAdapter = new MyAdapter<>(Rutas, Frag_7_RutasFrecuentes.this, R.layout.fragrutas_item);
                    rvLista.setAdapter(mAdapter);
                } else {
                    tvRutas.setVisibility(View.VISIBLE);
                }
            } else {
                tvRutas.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new RutasViewHolder(v);
    }

    @Override
    public void onStop() {
        if (tipo == 2) {
            ((RootBody) getActivity()).flRutasFrec.setVisibility(View.VISIBLE);
        }
        super.onStop();
    }

    public class RutasViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<RutasViewHolder>, View.OnClickListener {
        RadioButton rbEscoger;
        Button btnBorrar;
        TextView tvNombre, tvOrigenDestino, tvDirOrigen, tvDirDestino;

        public RutasViewHolder(View v) {
            super(v);
            btnBorrar = (Button) v.findViewById(R.id.btnBorrar);
            rbEscoger = (RadioButton) v.findViewById(R.id.rbEscoger);
            btnBorrar.setOnClickListener(this);
            rbEscoger.setOnClickListener(this);
            tvNombre = (TextView) v.findViewById(R.id.tvNombre);
            tvOrigenDestino = (TextView) v.findViewById(R.id.tvOrigenDestino);
            tvDirOrigen = (TextView) v.findViewById(R.id.tvDirOrigen);
            tvDirDestino = (TextView) v.findViewById(R.id.tvDirDestino);
        }

        @Override
        public void bindView(RutasViewHolder holder, int position) {
            if (tipo == 1) {
                btnBorrar.setVisibility(View.VISIBLE);
                rbEscoger.setVisibility(View.GONE);
            } else if (tipo == 2) {
                btnBorrar.setVisibility(View.GONE);
                rbEscoger.setVisibility(View.VISIBLE);
            }

            JsonPack.RutaFrec frec = Rutas.get(position);

            holder.tvNombre.setText(frec.ruta_nombre);
            holder.tvOrigenDestino.setText(frec.nombre_origen + " - " + frec.nombre_destino);
            holder.tvDirOrigen.setText(frec.origen);
            holder.tvDirDestino.setText(frec.destino);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnBorrar:
                    LayoutInflater inflater = getLayoutInflater(getArguments());
                    final View dialoglayout = inflater.inflate(R.layout.dialog_confirmeliminarrutafrec, null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setView(dialoglayout);
                    final AlertDialog dialog = builder.show();
                    Button btnSi = (Button) dialoglayout.findViewById(R.id.btnSi);
                    Button btnNo = (Button) dialoglayout.findViewById(R.id.btnNo);
                    btnSi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (flag_borrar) {
                                flag_borrar = false;
                                MHR_BORRAR.putParams("id", Rutas.get(getLayoutPosition()).rutasid).send();

                                Rutas.remove(getLayoutPosition());
                                mAdapter.notifyDataSetChanged();
                            }
                            dialog.dismiss();
                        }
                    });
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                    break;

                case R.id.rbEscoger:
                    ((RootBody) getActivity()).RUTAFRECUENTETEMP = Rutas.get(getLayoutPosition());
                    getActivity().onBackPressed();
                    if (getTargetFragment() != null)
                        getTargetFragment().onActivityResult(getTargetRequestCode(), 200, new Intent());
                    break;
            }
        }
    }

    @Override
    public void onDetach() {
        if (getTargetFragment() != null)
            getTargetFragment().onActivityResult(getTargetRequestCode(), 100, new Intent());
        super.onDetach();
    }
}
