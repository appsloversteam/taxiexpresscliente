package com.appslovers.taxiexpresscli.body;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.dialogs.DialogResumen;
import com.appslovers.taxiexpresscli.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.maps.android.PolyUtil;

import org.json.JSONObject;

import java.util.ArrayList;

import pe.com.visanet.lib.VisaNetConfigurationContext;
import pe.com.visanet.lib.VisaNetPaymentActivity;

/**
 * Created by javierquiroz on 21/07/16.
 */
public class FragSolicitudDestino extends Fragment implements Frag_3_Solicitud.IPageFragment, View.OnClickListener {
    public View.OnClickListener OCL;
    Frag_3_Solicitud FSOL;

    @Override
    public Fragment getFragment() {
        return this;
    }

    public static FragSolicitudDestino getInstance(View.OnClickListener ocl, Frag_3_Solicitud fsol) {
        FragSolicitudDestino f = new FragSolicitudDestino();
        f.OCL = ocl;
        f.FSOL = fsol;
        return f;
    }

    public static String[] TiposPago = {"Efectivo", "Vale", "Tarjeta VISA"};

    public static String
    getTipoPago(String idpago) {
        if (idpago.equals("1")) {
            return TiposPago[0];
        } else if (idpago.equals("2")) {
            return TiposPago[1];
        } else if (idpago.equals("3")) {
            return TiposPago[2];
        }else {
            return "";
        }
    }

    EditText etReferenciaDestino;
    Button btnDireccion2;

    TextView tvTipoPago;
    TextView tvTipoServicio;
    String[] tiposdeservicio;
    ArrayList<JsonPack.TipoServicio> tiposServices;
    Button btnResumen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragsolicitud_destino, container, false);
        Log.i("Destino", "onCreatView");

        tiposServices = new SPUser(getActivity()).getTiposServicio();
        tiposdeservicio = new String[tiposServices.size()];
        int i = -1;
        for (JsonPack.TipoServicio t : tiposServices) {
            i++;
            tiposdeservicio[i] = t.nombre;
        }
        FSOL.Servicio.TipoServicio = "" + tiposServices.get(0).id;
        Log.i("SolicituDestino", ">Asignando el primero a tipo de servicio");
        tvTipoServicio = (TextView) rootView.findViewById(R.id.tvServicio);
        tvTipoServicio.setText(tiposdeservicio[0]);


        tvTipoPago = (TextView) rootView.findViewById(R.id.tvPago);
        tvTipoPago.setText("Elegir Pago");
        FSOL.Servicio.TipoPago = "1";

        rootView.findViewById(R.id.btnResumen).setOnClickListener(OCL);
        etReferenciaDestino = (EditText) rootView.findViewById(R.id.etReferenciaDestino);

        btnDireccion2 = (Button) rootView.findViewById(R.id.btnDireccionDestino);
        btnDireccion2.setOnClickListener(this);
        rootView.findViewById(R.id.btnTipoServicio).setOnClickListener(this);
        rootView.findViewById(R.id.btnTipoPago).setOnClickListener(this);
        btnResumen = (Button) rootView.findViewById(R.id.btnResumen);
        btnResumen.setOnClickListener(this);

        tvTipoServicio.setText(tiposdeservicio[0]);

        return rootView;
    }

    DialogResumen DRES;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnResumen:

                if(! btnDireccion2.getText().toString().equalsIgnoreCase("Ingresar dirección y urbanización")){
                    if (!flagDisplayResumenafterTarifa) {
                        //si no viene de rutas frecuentes
                        FSOL.Servicio.DestinoReferencia = etReferenciaDestino.getText().toString();
                        FSOL.Servicio.DestinoDireccion = btnDireccion2.getText().toString();
                    } else {
                        //si viene de ahi volver false
                        flagDisplayResumenafterTarifa = false;
                    }


                    if (DRES == null) {
                        DRES = new DialogResumen();
                        DRES.setTargetFragment(FragSolicitudDestino.this, 123);
                    }

                    DRES.setServicio(FSOL.Servicio);
                    if (!DRES.isAdded())
                        DRES.show(getFragmentManager(), "aasasdf");
                }

                break;

            case R.id.btnTipoPago:

                new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        .setTitle(getString(R.string.app_empresa))
                        .setItems(TiposPago, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                switch(which){
                                    case 0:
                                        FSOL.Servicio.TipoPago = "" + (which + 1);
                                        break;
                                    case 1:
                                        MHR.putParams("cliente_id",FSOL.Servicio.IdCliente).putParams("tipo","2").send();
                                        break;
                                    case 2:
                                        //Toast.makeText(getActivity(), "El pago con tarjeta de crédito estará pronto disponible", Toast.LENGTH_SHORT).show();
                                        tvTipoPago.setText(getTipoPago("" + (which + 1)));
                                        FSOL.Servicio.TipoPago = "" + (which + 1);
                                }

                                //tvTipoPago.setText(getTipoPago("" + (which + 1)));
                                //FSOL.Servicio.TipoPago = "" + (which + 1);


                                concultarTarifa();


                            }
                        }).create().show();
                break;

            case R.id.btnTipoServicio:

                new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        .setTitle(getString(R.string.app_empresa))
                        .setItems(tiposdeservicio, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                tvTipoServicio.setText(tiposdeservicio[which]);
                                FSOL.Servicio.TipoServicio = "" + tiposServices.get(which).id;
                                concultarTarifa();
                            }
                        }).create().show();
                break;
            case R.id.btnDireccionDestino:
                FSOL.MostrarDialogDirecciones(btnDireccion2.getText().toString(), "Ingrese dirección de destino", Frag_3_Solicitud.reqCodeDestino, FragSolicitudDestino.this);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("Destino", "onViewCreated");
    }

    Frag_3_Solicitud.IMapListen LISTEN = new Frag_3_Solicitud.IMapListen() {
        @Override
        public void onMapTouched() {
            Log.d("onMapTouched", "--------------->>>>>>>>>>");
            FSOL.ivMiUbicacion.getDrawable().setAlpha(100);
            FSOL.ivMiUbicacion.refreshDrawableState();
        }

        @Override
        public void onMapReleased() {
            Log.d("onMapEeleased", "--------------->>>>>>>>>>");
        }

        @Override
        public void onMapUnsettled() {
            Log.d("onMapunsettled", "--------------->>>>>>>>>>");
        }

        @Override
        public void onMapSettled() {

            FSOL.Servicio.DestinoLat = FSOL.MAPA.getCameraPosition().target.latitude;
            FSOL.Servicio.DestinoLng = FSOL.MAPA.getCameraPosition().target.longitude;
            consultarDireccion(FSOL.MAPA.getCameraPosition().target);
            buscarIdZona(new LatLng(FSOL.MAPA.getCameraPosition().target.latitude, FSOL.MAPA.getCameraPosition().target.longitude));
            Log.d("onMapSettled", "--------------->>>>>>>>>>");
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Destino", "onStart");
    }

    @Override
    public void unSelected() {

    }

    @Override
    public void onThisCurrent() {

        if (getString(R.string.app_empresa).equals("AWB")) {
            ((RootBody) getActivity()).tvTitulo.setText(getString(R.string.app_empresa) + " destino");
        }
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);

        FSOL.IMM.hideSoftInputFromWindow(etReferenciaDestino.getWindowToken(), 0);


        FSOL.setListenerMapa(LISTEN);
        if (FSOL.Servicio.DestinoLat != 0 && FSOL.Servicio.DestinoLng != 0) {

            flagQueryDir = false;

            FSOL.MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(FSOL.Servicio.DestinoLat, FSOL.Servicio.DestinoLng), 15, 0, 0)));
        }

        if (((RootBody) getActivity()).RUTAFRECUENTETEMP != null) {
            JsonPack.RutaFrec rf = ((RootBody) getActivity()).RUTAFRECUENTETEMP;

            FSOL.Servicio.IdCliente = rf.cliente_id;
            FSOL.Servicio.TipoServicio = rf.tiposervicio;
            FSOL.Servicio.TipoPago = "1";
            FSOL.Servicio.OrigenNombre = rf.nombre_origen;
            FSOL.Servicio.OrigenDireccion = rf.origen;
            FSOL.Servicio.OrigenId = rf.id_origen;
            FSOL.Servicio.DestinoNombre = rf.nombre_destino;
            FSOL.Servicio.DestinoId = rf.id_destino;
            FSOL.Servicio.DestinoDireccion = rf.destino;
            FSOL.Servicio.OrigenLat = rf.latitud_origen;
            FSOL.Servicio.OrigenLng = rf.longitud_origen;
            FSOL.Servicio.DestinoLat = rf.latitud_destino;
            FSOL.Servicio.DestinoLng = rf.longitud_destino;
            FSOL.Servicio.OrigenReferencia = rf.ref_origen;
            FSOL.Servicio.DestinoReferencia = rf.ref_destino;

            flagDisplayResumenafterTarifa = true;
            concultarTarifa();

            ((RootBody) getActivity()).RUTAFRECUENTETEMP = null;
        }
    }

    boolean flagDisplayResumenafterTarifa = false;

    boolean flagQueryDir = true;
    MyRequest<JsonPack.GoogleGeoCode> MyRequestJsonPackGoogleGeoCode;

    public void consultarDireccion(LatLng target) {

        if (flagQueryDir) {
            FSOL.ivMiUbicacion.setImageResource(R.drawable.ic_cliente_standby);
            FSOL.ivMiUbicacion.getDrawable().setAlpha(255);
            FSOL.ivMiUbicacion.refreshDrawableState();

            if (MyRequestJsonPackGoogleGeoCode == null) {
                MyRequestJsonPackGoogleGeoCode = new MyRequest<JsonPack.GoogleGeoCode>(null, Urls.google_inversegeocode, MyRequest.HttpRequestType.GET) {
                    @Override
                    public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleGeoCode object) {

                        if (object.results.size() > 0) {
                            if (object.results.get(0).address_components != null) {

                                String snum = "";
                                String snom = "";
                                String sdistrict="";
                                for (JsonPack.GoogleGeoCodeAddressComponent ac : object.results.get(0).address_components) {
                                    if (ac.types != null) {
                                        if (ac.types.size() > 0) {
                                            if (ac.types.get(0).contains("street_number")) {
                                                snum = ac.short_name;
                                            }
                                            if (ac.types.get(0).contains("route")) {
                                                snom = ac.long_name;
                                            }
                                            if (ac.types.get(0).contains("locality")) {
                                                sdistrict = ac.long_name;
                                            }
                                        }
                                    }

                                }

                                btnDireccion2.setText((snom + " " + snum + " " + sdistrict).trim());
                            }
                        }
                        FSOL.ivMiUbicacion.setImageResource(R.drawable.ic_cliente);
                        FSOL.ivMiUbicacion.getDrawable().setAlpha(255);
                        FSOL.ivMiUbicacion.refreshDrawableState();
                        concultarTarifa();
                    }

                    @Override
                    public void onFailedRequest(ResponseWork rw, boolean isActive) {

                        FSOL.ivMiUbicacion.setImageResource(R.drawable.ic_cliente);
                        FSOL.ivMiUbicacion.getDrawable().setAlpha(255);
                        FSOL.ivMiUbicacion.refreshDrawableState();
                        concultarTarifa();
                    }
                };
            }
            MyRequestJsonPackGoogleGeoCode.CancelarRequest();
            MyRequestJsonPackGoogleGeoCode.putParams("latlng", target.latitude + "," + target.longitude)
                    .putParams("sensor", "false")
                    .putParams("key", Urls.GOOGLE_KEY)
                    .setShowLogs(false)
                    .send();
        } else {
            flagQueryDir = true;
            concultarTarifa();
        }

    }

    boolean overrideAddress=true;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case Frag_3_Solicitud.reqCodeDestino:

                if (resultCode == 200) {
                    JsonPack.DirLug DL = (JsonPack.DirLug) data.getSerializableExtra("dir");
                    if (DL.direccion==null)
                        DL.direccion="";
                    flagQueryDir = false;
                    FSOL.MAPA.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(DL.lat, DL.lon), AppBackground.ZOOM_GOOGLEMAPS, 0, 0)));
                    btnDireccion2.setText(DL.name +" "+DL.direccion);
                    overrideAddress=false;
                }
                break;

            case 123:

                if (FSOL.Servicio.Tarifa != null && FSOL.Servicio.Tarifa.length() > 0) {
                    FSOL.Servicio.codigoVale = new SPUser(getActivity()).getCodigoVale();
                    FSOL.EmpezarSolicitud();
                }else
                    //Toast.makeText(getContext(),"Ruta no disponible",Toast.LENGTH_SHORT).show();

                    break;
        }
    }

    private void buscarIdZona(LatLng point) {
        if (getId != null) {
            if (getId.getStatus() == AsyncTask.Status.RUNNING) {
                getId.cancel(true);
            }
        }

        getId = new getIdOrigen(point, Frag_3_Solicitud.ZZ);
        getId.execute();
    }

    public void concultarTarifa() {

        if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 1) {
            if (FSOL.Servicio.OrigenId != null && FSOL.Servicio.DestinoId != null) {
                if (FSOL.Servicio.OrigenId != "" && FSOL.Servicio.DestinoId != "") {
                    MRT
                            .putParams("tiposervicio_id", "" + FSOL.Servicio.TipoServicio)
                            .putParams("empresa_id", FSOL.Sesion.empresa_id)
                            .putParams("origen_id", FSOL.Servicio.OrigenId)
                            .putParams("destino_id", FSOL.Servicio.DestinoId)
                            .putParams("activo","2")
                            .putParams("reserva","2")
                            .send();
                }
            }
        } else if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 2) {
            if (FSOL.Servicio.OrigenLat != 0.0d && FSOL.Servicio.OrigenLng != 0.0d && FSOL.Servicio.DestinoLat != 0.0d && FSOL.Servicio.DestinoLng != 0.0d) {
                MR_DISTANCE.CancelarRequest();
                MR_DISTANCE.putParams("origins", FSOL.Servicio.OrigenLat + "," + FSOL.Servicio.OrigenLng)
                        .putParams("destinations", FSOL.Servicio.DestinoLat + "," + FSOL.Servicio.DestinoLng)
                        .putParams("mode", "driver")
                        .putParams("language", "es-PE")
                        .putParams("key", Urls.GOOGLE_KEY).send();
            }
        }
    }

    ArrayList<JsonPack.ValeElectronico> promociones;

    MyRequest MHR = new MyRequest(Urls.w_vales, MyRequest.HttpRequestType.GET) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {

            promociones = null;
            try {
                //promociones=new Gson().fromJson(rw.ResponseBody,  new TypeToken<ArrayList<JsonPack.Promocion>>() {}.getType());
                JsonPack.ResponseListaVales rp = new Gson().fromJson(rw.ResponseBody, JsonPack.ResponseListaVales.class);
                if (rp != null && rp.data != null) {
                    if (rp.status){
                        tvTipoPago.setText(getTipoPago("2"));
                        promociones = rp.data;
                        for (int i=0; i<promociones.size(); i++){
                            new SPUser(getActivity()).setCodigoVale(promociones.get(i).vale_codigo);
                            tvTipoPago.setText(getTipoPago("2"));
                            FSOL.Servicio.TipoPago = "2";
                        }
                    }

                }

            } catch (JsonParseException e) {
                Toast.makeText(getActivity(), "No se encontraron vales disponibles", Toast.LENGTH_SHORT).show();
                tvTipoPago.setText(getTipoPago("1"));
                Log.e("json ex", ">" + e.getMessage());
            }
        }
    };

    MyRequest<JsonPack.GoogleDistanceMatrixResult> MR_DISTANCE = new MyRequest<JsonPack.GoogleDistanceMatrixResult>(Urls.google_distancematrix, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleDistanceMatrixResult object) {


            if (object != null) {
                if (object.rows != null) {
                    if (object.rows.size() > 0) {
                        if (object.rows.get(0).elements != null) {
                            if (object.rows.get(0).elements.size() > 0) {

                                if (object.rows.get(0).elements.get(0) != null && object.rows.get(0).elements.get(0).distance != null & object.rows.get(0).elements.get(0).duration != null) {
                                    MRT.CancelarRequest();
                                    MRT
                                            .putParams("tiposervicio_id", "" + FSOL.Servicio.TipoServicio)
                                            //.putParams("origen_id", FSOL.Servicio.OrigenId)
                                            //.putParams("destino_id", FSOL.Servicio.DestinoId)
                                            .putParams("empresa_id", FSOL.Sesion.empresa_id)
                                            .putParams("distancia", object.rows.get(0).elements.get(0).distance.text.replace(" km", ""))//"3.4 km"
                                            .putParams("tiempo", object.rows.get(0).elements.get(0).duration.text.replace(" min", ""))//"13 min"
                                            .send();
                                    //tvTiempo.setText(object.rows.get(0).elements.get(0).duration.text);
                                } else {
                                    MRT.CancelarRequest();
                                    MRT
                                            .putParams("tiposervicio_id", "" + FSOL.Servicio.TipoServicio)
                                            //.putParams("origen_id", FSOL.Servicio.OrigenId)
                                            //.putParams("destino_id", FSOL.Servicio.DestinoId)
                                            .putParams("empresa_id", FSOL.Sesion.empresa_id)
                                            .putParams("distancia", "0.0")
                                            .putParams("tiempo", "0")
                                            .send();
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    MyRequest<JsonPack.ResponseTrarifa> MRT = new MyRequest<JsonPack.ResponseTrarifa>(Urls.ws_consultar_tarifa, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.ResponseTrarifa object) {

            Log.i("concultar_tarifa", "" + rw.ResponseBody.toString());

            if (object != null) {
                FSOL.Servicio.Tarifa = "" + object.tarifa;

                if (object.status) {
                    if (flagDisplayResumenafterTarifa) {
                        onClick(btnResumen);
                    }

                    btnResumen.setClickable(true);
                    btnResumen.setEnabled(true);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.ruta_no_disponible), Toast.LENGTH_SHORT).show();
                }
            } else {
                FSOL.Servicio.Tarifa = "";
                Toast.makeText(getActivity(), getActivity().getString(R.string.ruta_no_disponible), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private getIdOrigen getId;

    private class getIdOrigen extends AsyncTask<Void, Void, Void> {
        private LatLng point;
        private ArrayList<JsonPack.Zona> ZZ;

        public getIdOrigen(LatLng point, ArrayList<JsonPack.Zona> ZZ) {
            this.point = point;
            this.ZZ = ZZ;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Boolean hasId = false;
            FSOL.Servicio.DestinoId = "0";
            String nombre_zona = "";

            for (JsonPack.Zona zona : ZZ) {
                ArrayList<LatLng> LatLngs = new ArrayList<>();
                ArrayList<JsonPack.Coordenada> coordenadas = zona.coordenadas;

                if (coordenadas.size() > 0) {
                    for (JsonPack.Coordenada coord : coordenadas) {
                        LatLngs.add(new LatLng(coord.latitud, coord.longitud));
                    }

                    if (LatLngs.size() > 0) {
                        hasId = new JsonPack.Coordenada().isInsidePolygon(LatLngs, point);

                        if (hasId) {
                            FSOL.Servicio.DestinoId = zona.id;
                            nombre_zona = zona.nombre;
                            break;
                        }
                    }
                }
                if (hasId) {
                    break;
                }
            }

            if (FSOL.Servicio.DestinoId.equals("0"))
                Log.e("IdDestino", "" + FSOL.Servicio.DestinoId + ", Zona no encontrada");
            else
                Log.e("IdDestino", "" + FSOL.Servicio.DestinoId + ", Zona: " + nombre_zona);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //if (overrideAddress)
            //    consultarDireccion(FSOL.MAPA.getCameraPosition().target);
            concultarTarifa();
        }
    }
}