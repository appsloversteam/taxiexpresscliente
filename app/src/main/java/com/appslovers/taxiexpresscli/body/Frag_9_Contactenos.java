package com.appslovers.taxiexpresscli.body;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.R;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class Frag_9_Contactenos extends Fragment implements View.OnClickListener {
    View ViewRoot;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragcontactenos, null);
        ViewRoot.findViewById(R.id.btnEmail).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnLlamar).setOnClickListener(this);
        return ViewRoot;
    }

    String temptitulo = "";

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((RootBody) getActivity()).mostarTopBarSolicitud();
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        temptitulo = ((RootBody) getActivity()).tvTitulo.getText().toString();
        ((RootBody) getActivity()).tvTitulo.setText("Contáctenos");
    }

    @Override
    public void onDetach() {
        ((RootBody) getActivity()).tvTitulo.setText(temptitulo);
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnEmail:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contacto_email)});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                try {
                    getActivity().startActivity(i);
                } catch (android.content.ActivityNotFoundException ex) {
                    ex.printStackTrace();
                }
                break;
            case R.id.btnLlamar:
                if (android.util.Patterns.PHONE.matcher(getString(R.string.contacto_telcell)).matches()) {
                    String uri = "tel:" + getString(R.string.contacto_telcell);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);

                }
                break;
        }

    }
}
