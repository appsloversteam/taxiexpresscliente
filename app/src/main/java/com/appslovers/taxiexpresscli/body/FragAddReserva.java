package com.appslovers.taxiexpresscli.body;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.BuildConfig;
import com.appslovers.taxiexpresscli.RootSplash;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.ConfirmDialog;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.dialogs.DialogDirsFavRes;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.DatePickerFragment;
import com.appslovers.taxiexpresscli.util.TimePickerFragment;
import com.appslovers.taxiexpresscli.util.Tools;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragAddReserva extends Fragment implements View.OnClickListener {
    View ViewRoot;
    Button btnHora, btnFecha;
    Button btnOrigen, btnDestino;
    EditText etRefOrigen, etRefDestino;
    Button btnTipoSer, btnTipoPag;
    TextView tvTarifa;
    Button btnGuardar;
    JsonPack.NuevaReserva NR;
    TextView tvLabelTarifa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewRoot = inflater.inflate(R.layout.fragaddreserva, null);

        btnHora = (Button) ViewRoot.findViewById(R.id.btnHora);
        btnHora.setOnClickListener(this);
        btnFecha = (Button) ViewRoot.findViewById(R.id.btnFecha);
        btnFecha.setOnClickListener(this);

        btnOrigen = (Button) ViewRoot.findViewById(R.id.btnOrigen);
        btnOrigen.setOnClickListener(this);
        btnDestino = (Button) ViewRoot.findViewById(R.id.btnDestino);
        btnDestino.setOnClickListener(this);

        etRefOrigen = (EditText) ViewRoot.findViewById(R.id.etRefOrigen);
        etRefDestino = (EditText) ViewRoot.findViewById(R.id.etRefDestino);

        btnTipoSer = (Button) ViewRoot.findViewById(R.id.btnTipoSer);
        btnTipoSer.setOnClickListener(this);
        btnTipoPag = (Button) ViewRoot.findViewById(R.id.btnTipoPag);
        btnTipoPag.setOnClickListener(this);

        tvTarifa = (TextView) ViewRoot.findViewById(R.id.tvTarifa);

        btnGuardar = (Button) ViewRoot.findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);

        tvLabelTarifa = (TextView) ViewRoot.findViewById(R.id.tvLabelTarifa);
        if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 2) {
            tvLabelTarifa.setText(getString(R.string.tarifa) + " Aproximada : ");
        } else if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 1) {
            tvLabelTarifa.setText(getString(R.string.tarifa) + " : ");
        }

        return ViewRoot;
    }

    SPUser SPCLI;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((RootBody) getActivity()).tvTitulo.setText("Crear Reserva");
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.VISIBLE);

        NR = new JsonPack.NuevaReserva();

        tiposServices = new SPUser(getActivity()).getTiposServicio();
        tiposdeservicio = new String[tiposServices.size()];
        int i = -1;
        for (JsonPack.TipoServicio t : tiposServices) {
            i++;
            tiposdeservicio[i] = t.nombre;
        }
        NR.tiposervicio_id = "" + tiposServices.get(0).id;
        Log.i("SolicituDestino", ">Asignando el primero a tipo de servicio");

        btnTipoSer.setText(tiposdeservicio[0]);

        btnTipoPag.setText(getTipoPago("1"));
        NR.tipopago_id = "1";

        SPCLI = new SPUser(getActivity());
        NR.cliente_id = SPCLI.getResponseLogin().cliente_id;
    }

    DialogDirsFavRes DDIR;
    DatePickerFragment DPF;
    TimePickerFragment TPF;

    MyRequest MR_GUARDAR = new MyRequest(Urls.ws_agregar_reserva, MyRequest.HttpRequestType.POST) {
        @Override
        public void onSuccesForeground(ResponseWork rw) {
            if (rw.ResponseBody.contains("true")) {
                new ConfirmDialog(getActivity().getWindow().getContext(), "Su reserva esta siendo procesada.", getString(R.string.app_empresa), "Aceptar", "") {
                    @Override
                    public void onPositive() {
                        getActivity().onBackPressed();
                    }
                }.show();
            }
        }
    };

    String temp_fecha = "";
    String temp_hora = "";
    Calendar ReservaFecha = Calendar.getInstance();

    SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat HHmm = new SimpleDateFormat("HH:mm");

    long anticipacionminima = 1000 * 60 * 60 * 2;//2 horas

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnGuardar:
                anticipacionminima = 1000 * 60 * 60 * ((AppForeGround) getActivity().getApplication()).flag_horaanticipada;
                if (ZonaOrigen != null) {
                    NR.id_origen = ZonaOrigen.id;
                    NR.origen = ZonaOrigen.nombre;
                }
                NR.direccion_origen = btnOrigen.getText().toString();
                NR.referencia_origen = etRefOrigen.getText().toString();

                if (ZonaDestino != null) {
                    NR.id_destino = ZonaDestino.id;
                    NR.destino = ZonaDestino.nombre;
                }

                NR.direccion_destino = btnDestino.getText().toString();
                NR.referencia_destino = etRefDestino.getText().toString();

                NR.fecha = yyyyMMdd.format(ReservaFecha.getTime()) + " " + HHmm.format(ReservaFecha.getTime());

                boolean checkfecha = false;
                if (BuildConfig.DEBUG) {
                    checkfecha = true;
                } else//apk para publicacion
                {
                    Calendar sistema = Calendar.getInstance();
                    long dif = ReservaFecha.getTimeInMillis() - sistema.getTimeInMillis();

                    if (dif >= anticipacionminima) {
                        checkfecha = true;
                    }
                }

                if (checkfecha) {
                    MR_GUARDAR
                            .putParams("cliente_id", NR.cliente_id)
                            .putParams("id_origen", NR.id_origen)
                            .putParams("origen", NR.origen)
                            .putParams("direccion_origen", NR.direccion_origen)
                            .putParams("referencia_origen", NR.referencia_origen)
                            .putParams("latitud_origen", "" + NR.latitud_origen)
                            .putParams("longitud_origen", "" + NR.longitud_origen)
                            .putParams("id_destino", NR.id_destino)
                            .putParams("destino", NR.destino)
                            .putParams("direccion_destino", NR.direccion_destino)
                            .putParams("referencia_destino", NR.referencia_destino)
                            .putParams("latitud_destino", "" + NR.latitud_destino)
                            .putParams("longitud_destino", "" + NR.longitud_destino)
                            .putParams("tipopago_id", NR.tipopago_id)
                            .putParams("tiposervicio_id", NR.tiposervicio_id)
                            .putParams("tarifa", NR.tarifa)
                            .putParams("fecha", NR.fecha)
                            .send();
                }
                break;

            case R.id.btnHora:
                if (TPF == null) {
                    TPF = new TimePickerFragment();
                    TPF.ITP = new TimePickerFragment.ITimePicker() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            ReservaFecha.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            ReservaFecha.set(Calendar.MINUTE, minute);
                            btnHora.setText(HHmm.format(ReservaFecha.getTime()));
                        }
                    };
                }

                TPF.show(getFragmentManager(), "time");
                break;
            case R.id.btnFecha:
                if (DPF == null) {
                    DPF = new DatePickerFragment();
                    DPF.IDP = new DatePickerFragment.IDatePicker() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day) {
                            ReservaFecha.set(Calendar.YEAR, year);
                            ReservaFecha.set(Calendar.MONTH, month);
                            ReservaFecha.set(Calendar.DAY_OF_MONTH, day);

                            btnFecha.setText(yyyyMMdd.format(ReservaFecha.getTime()));
                        }
                    };
                }
                DPF.show(getFragmentManager(), "date");
                break;
            case R.id.btnOrigen:
                if (DDIR == null) {
                    DDIR = new DialogDirsFavRes();
                }
                DDIR.setTexoBusqueda(btnOrigen.getText().toString());
                DDIR.setTargetFragment(FragAddReserva.this, 123);
                DDIR.show(getFragmentManager(), "dirori");
                break;
            case R.id.btnDestino:
                if (DDIR == null) {
                    DDIR = new DialogDirsFavRes();
                }
                DDIR.setTexoBusqueda(btnDestino.getText().toString());
                DDIR.setTargetFragment(FragAddReserva.this, 321);
                DDIR.show(getFragmentManager(), "dirdes");
                break;
            case R.id.btnTipoSer:
                new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        .setTitle(getString(R.string.app_empresa))
                        .setItems(tiposdeservicio, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                btnTipoSer.setText(tiposdeservicio[which]);
                                NR.tiposervicio_id = "" + tiposServices.get(which).id;
                            }
                        }).create().show();
                break;
            case R.id.btnTipoPag:
                new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        .setTitle(getString(R.string.app_empresa))
                        .setItems(TiposPago, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                btnTipoPag.setText(getTipoPago("" + (which + 1)));
                                NR.tipopago_id = "" + (which + 1);
                            }
                        }).create().show();
                break;
        }
    }

    int curZona = 0;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 123:
                if (resultCode == 200) {
                    JsonPack.DirLug DL = (JsonPack.DirLug) data.getSerializableExtra("dir");
                    curZona = 123;
                    btnOrigen.setText(DL.name);
                    NR.latitud_origen = DL.lat;
                    NR.longitud_origen = DL.lon;
                    localizarZona(new LatLng(DL.lat, DL.lon));
                }
                break;
            case 321:
                if (resultCode == 200) {
                    JsonPack.DirLug DL = (JsonPack.DirLug) data.getSerializableExtra("dir");

                    curZona = 321;
                    btnDestino.setText(DL.name);
                    NR.latitud_destino = DL.lat;
                    NR.longitud_destino = DL.lon;
                    localizarZona(new LatLng(DL.lat, DL.lon));
                }
                break;

            case 12:
                if (resultCode == 200) {
                    if (((RootBody) getActivity()).RUTAFRECUENTETEMP != null) {
                        JsonPack.RutaFrec rf = ((RootBody) getActivity()).RUTAFRECUENTETEMP;
                        NR.cliente_id = rf.cliente_id;
                        NR.id_origen = rf.id_origen;
                        NR.longitud_origen = rf.longitud_origen;
                        NR.latitud_origen = rf.latitud_origen;
                        NR.direccion_origen = rf.origen;
                        NR.origen = rf.nombre_origen;
                        NR.id_destino = rf.id_destino;
                        NR.longitud_destino = rf.longitud_destino;
                        NR.latitud_destino = rf.latitud_destino;
                        NR.direccion_destino = rf.destino;
                        NR.destino = rf.nombre_destino;

                        NR.tiposervicio_id = rf.tiposervicio;
                        btnTipoSer.setText(tiposdeservicio[Integer.parseInt(rf.tiposervicio)]);
                        btnOrigen.setText(NR.direccion_origen);
                        btnDestino.setText(NR.direccion_destino);
                        concultarTarifa2();
                        ((RootBody) getActivity()).RUTAFRECUENTETEMP = null;
                    }
                }
                break;
        }
    }

    public void concultarTarifa() {
        if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 1) {
            if (ZonaOrigen != null && ZonaDestino != null) {
                String empresa_id= SPCLI.getResponseLogin().empresa_id;

                MRT
                        .putParams("tiposervicio_id", "" + NR.tiposervicio_id)
                        .putParams("origen_id", ZonaOrigen.id)
                        .putParams("destino_id", ZonaDestino.id)
                        .putParams("empresa_id", empresa_id)
                        .send();
            }
        } else if (((AppForeGround) getActivity().getApplication()).flag_tipotarifa == 2) {
            MR_DISTANCE.CancelarRequest();
            MR_DISTANCE.putParams("origins", NR.latitud_origen + "," + NR.longitud_origen)
                    .putParams("destinations", NR.latitud_destino + "," + NR.longitud_destino)
                    .putParams("mode", "driver")
                    .putParams("language", "es-PE")
                    //.putParams("key", "AIzaSyBCycz1v8B0dyhfz7fLO-Nx8x7Dv2fsn60")
                    .putParams("key", Urls.GOOGLE_KEY)
                    .send();
        }
    }

    public void concultarTarifa2() {


        if (NR.id_origen != null && NR.id_destino != null) {
            MRT
                    .putParams("tiposervicio_id", "" + NR.tiposervicio_id)
                    .putParams("origen_id", NR.id_origen)
                    .putParams("destino_id", NR.id_destino)
                    .putParams("empresa_id", SPCLI.getResponseLogin().empresa_id)
                    .send();
        }

    }

    MyRequest<JsonPack.ResponseTrarifa> MRT = new MyRequest<JsonPack.ResponseTrarifa>(Urls.ws_consultar_tarifa, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.ResponseTrarifa object) {

            if (object.status) {
                //FSOL.Servicio.Tarifa=""+object.tarifa;
                tvTarifa.setText(Tools.formatFloat(object.tarifa));
                NR.tarifa = "" + object.tarifa;
                //String.format( "%.2f", object.tarifa);
            } else {
                NR.tarifa = "0.00";
            }
        }
    };


    JsonPack.Zona ZonaOrigen, ZonaDestino;
    AsyncTask<LatLng, Integer, JsonPack.Zona> TareaLocalizar;

    public synchronized void localizarZona(LatLng zonaactual) {
        if (TareaLocalizar != null) {
            TareaLocalizar.cancel(true);
        }
        TareaLocalizar = new AsyncTask<LatLng, Integer, JsonPack.Zona>() {
            @Override
            protected JsonPack.Zona doInBackground(LatLng... params) {
                JsonPack.Zona zona = null;
                for (JsonPack.Zona Z : ((AppForeGround) getActivity().getApplication()).getZonas()) {
                    if (PolyUtil.containsLocation(params[0], Z.getLtns(), false)) {
                        zona = Z;
                        break;
                    }
                }
                return zona;
            }

            @Override
            protected void onPostExecute(JsonPack.Zona zon) {
                if (zon != null) {
                    if (curZona == 123) {
                        //guardar id y nombre de zona
                        ZonaOrigen = zon;
                        if (ZonaDestino != null)
                            concultarTarifa();
                        Log.e("localizarzon", "Zona Origen: " + ZonaOrigen.id + " - " + ZonaOrigen.nombre.toUpperCase());

                    } else if (curZona == 321) {
                        //guardar id y nombre de zona
                        ZonaDestino = zon;
                        if (ZonaOrigen != null)
                            concultarTarifa();
                        Log.e("localizarzon", "Zona Destino: " + ZonaDestino.id + " - " + ZonaDestino.nombre.toUpperCase());
                    }
                } else
                    Log.e("Zona No Encontrada", ">");
            }
        };
        TareaLocalizar.execute(zonaactual);
    }

    String[] tiposdeservicio;
    ArrayList<JsonPack.TipoServicio> tiposServices;
    public static String[] TiposPago = {"Efectivo", "Vale"};

    public static String getTipoPago(String idpago) {
        if (idpago.equals("1")) {
            return TiposPago[0];
        } else if (idpago.equals("2")) {
            return TiposPago[1];
        } else {
            return "";
        }
    }

    MyRequest<JsonPack.GoogleDistanceMatrixResult> MR_DISTANCE = new MyRequest<JsonPack.GoogleDistanceMatrixResult>(Urls.google_distancematrix, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleDistanceMatrixResult object) {
            if (object != null) {
                if (object.rows != null) {
                    if (object.rows.size() > 0) {
                        if (object.rows.get(0).elements != null) {
                            if (object.rows.get(0).elements.size() > 0) {
                                MRT.CancelarRequest();
                                MRT
                                        .putParams("tiposervicio_id", "" + NR.tiposervicio_id)
                                        .putParams("empresa_id", SPCLI.getResponseLogin().empresa_id)
                                        .putParams("distancia", object.rows.get(0).elements.get(0).distance.text.replace(" km", ""))//"3.4 km"
                                        .putParams("tiempo", object.rows.get(0).elements.get(0).duration.text.replace(" min", ""))//"13 min"
                                        .send();
                            }
                        }
                    }
                }
            }
        }
    };
}
