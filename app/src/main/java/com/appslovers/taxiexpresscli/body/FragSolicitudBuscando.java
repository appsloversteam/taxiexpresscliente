package com.appslovers.taxiexpresscli.body;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.IMyAdapter;
import com.appslovers.taxiexpresscli.data.IMyViewHolder;
import com.appslovers.taxiexpresscli.data.InnerActions;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.util.ConfirmDialog;
import com.appslovers.taxiexpresscli.util.MyAdapter;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.SocketIo;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.util.MyAppBackground;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.util.SocketIoBridge;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by javierquiroz on 21/07/16.
 */
public class FragSolicitudBuscando extends Fragment implements Frag_3_Solicitud.IPageFragment, IMyViewHolder, View.OnClickListener {
    public View.OnClickListener OCL;
    Frag_3_Solicitud FSOL;

    @Override
    public Fragment getFragment() {
        return this;
    }

    public static FragSolicitudBuscando getInstance(View.OnClickListener ocl, Frag_3_Solicitud fsol) {
        FragSolicitudBuscando f = new FragSolicitudBuscando();
        f.OCL = ocl;
        f.FSOL = fsol;
        return f;
    }

    RecyclerView rvLista;
    LinearLayout llListaTaxista;
    LinearLayoutManager llm;
    TextView tvContador;
    TextView tvSinConductores;
    Button btnReloj;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("Buscando", "onCreateView");
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragsolicitud_conductores, container, false);
        llListaTaxista = (LinearLayout) rootView.findViewById(R.id.llListaTaxistas);
        tvSinConductores = (TextView) rootView.findViewById(R.id.tvSinConductores);
        tvContador = (TextView) rootView.findViewById(R.id.tvContador);
        tvContador.setOnClickListener(this);

        btnReloj = (Button) rootView.findViewById(R.id.ivReloj);
        btnReloj.setOnClickListener(this);
        rvLista = (RecyclerView) rootView.findViewById(R.id.lista);
        rvLista.setHasFixedSize(true);
        llm = new LinearLayoutManager(getActivity());
        rvLista.setLayoutManager(llm);

        //rootView.findViewById(R.id.btnResumen).setOnClickListener(OCL);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("Buscando", "onViewCreated");

    }

    SimpleDateFormat ss = new SimpleDateFormat("ss");
    long contador = 11 * 1000;
    CountDownTimer CDT = new CountDownTimer(11000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            tvContador.setText(ss.format(new Date(contador -= 1000)));
        }

        @Override
        public void onFinish() {
            //tvContador.setText(ss.format(new Date(contador-=1000)));


            if (conductores != null && conductores.size() > 0) {
                llListaTaxista.setVisibility(View.VISIBLE);
            } else {
                //flag_aceptasolodurante10segundos=false;
                MAB.ABG.NodeJsTurnOff();
                contador = 11 * 1000;

                tvSinConductores.setVisibility(View.VISIBLE);
                tvContador.setText("¿Reintentar?");
                tvContador.setEnabled(true);
                btnReloj.setEnabled(true);
            }
        }
    };


    JsonPack.SolicitudResponse RespuestaSolicitud;

    MyAdapter<FragSolicitudBuscando.TaxistaViewHolder, JsonPack.ConductorLista> mAdapter;
    ArrayList<JsonPack.ConductorLista> conductores;

    //boolean flag_aceptasolodurante10segundos=true;
    @Override
    public void onStart() {
        super.onStart();
        Log.i("Buscando", "onStart");

        MAB.addListner(new SocketIoBridge() {
            @Override
            public void connect(String s) {

                MAB.doit(getActivity(), 4);
                ((RootBody) getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_rojo4r);
                ((RootBody) getActivity()).Emergencia.setEnabled(true);
            }

            @Override
            public void SolicitudAceptar(JsonPack.ConductorLista conductorLista) {

                //if(flag_aceptasolodurante10segundos) {
                if (conductores == null) {
                    conductores = new ArrayList<>();
                    mAdapter = new MyAdapter<>(conductores, FragSolicitudBuscando.this, R.layout.fragtaxistas_item);

                    rvLista.setAdapter(mAdapter);
                }

                conductores.add(conductorLista);
                mAdapter.notifyDataSetChanged();
                //mAdapter.notifyItemInserted(conductores.size()-1);

                //quitar lo de abajo para regrsesar a anteior
                btnReloj.setEnabled(true);
            }

            //}
        });

    }

    @Override
    public void onStop() {

        Log.i("Buscando", "onStop");
        MAB.removeListener();
        super.onStop();
    }


    MyRequest<JsonPack.RespEstadoCliente> getEstadoCliente = new MyRequest<JsonPack.RespEstadoCliente>(null, Urls.estado, MyRequest.HttpRequestType.POST) {

        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.RespEstadoCliente object) {

            Log.i("Calf cond", ">" + object.CalificacionCliente);
            Log.i(" cliente objeto", " " + object.Conductor);
            Log.i(" servicio objeto", " " + object.Servicio);

            Intent servici = new Intent(getActivity(), AppBackground.class);
            servici.setAction(InnerActions.notact_updateestado.toString());
            servici.putExtra("rec", object);
            getActivity().startService(servici);

            if (pdProcesando != null)
                pdProcesando.cancel();

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_root_inside, new FragServicio(), FragServicio.class.getSimpleName())
                    .commit();
        }

    };

    ProgressDialog pdProcesando;
    MyAppBackground MAB = new MyAppBackground(getActivity()) {
        @Override
        public void todo(AppBackground app, int idop, Object o) {

            switch (idop) {

                case 4:

                    tvContador.setText("Solicitando...");
                    tvContador.setEnabled(false);
                    btnReloj.setEnabled(false);
                    FSOL.Servicio.codigoVale=new SPUser(getActivity()).getCodigoVale();
                    MAB.ABG.solicitarServicio(FSOL.Servicio, new SocketIo.CallBack() {
                        @Override
                        public void onCallBack(String s) {
                            Log.d(">callback serv", ">" + s);

                            RespuestaSolicitud = new Gson().fromJson(s, JsonPack.SolicitudResponse.class);

                            if (RespuestaSolicitud.idsCond != null && RespuestaSolicitud.idsCond.size() > 0) {
                                Log.d("primerconductor", ">" + RespuestaSolicitud.idsCond.get(0));
                                tvContador.setText("10");
                                CDT.start();
                            } else {
                                MAB.ABG.NodeJsTurnOff();
                                Log.e("primercoductor", ">");
                                contador = 11 * 1000;
                                tvSinConductores.setVisibility(View.VISIBLE);
                                tvContador.setText("¿Reintentar?");
                                tvContador.setEnabled(true);
                                btnReloj.setEnabled(true);
                            }
                        }
                    });

                    break;
                case 0:
                    tvSinConductores.setVisibility(View.GONE);
                    app.NodeJsTurnOn();
                    break;

                case 1:
                    Log.d("Apagndo", "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    app.NodeJsTurnOff();
                    ((RootBody) getActivity()).Emergencia.setBackgroundResource(R.drawable.bg_touch_grisconborde4r);
                    ((RootBody) getActivity()).Emergencia.setEnabled(false);
                    break;

                case 2:
                    final JsonPack.ConductorLista con = (JsonPack.ConductorLista) o;

                    llListaTaxista.setEnabled(false);

                    if (pdProcesando == null)
                        pdProcesando = ProgressDialog.show(getActivity(), getString(R.string.app_empresa), "Escogiendo al conductor...", true, false);
                    else {
                        pdProcesando.setMessage("Escogiendo al conductor...");
                        pdProcesando.show();
                    }

                    MAB.ABG.SelecionarConductor(RespuestaSolicitud, con, new SocketIo.CallBack() {

                        @Override
                        public void onCallBack(String s) {
                            SPUser sp = new SPUser(getActivity());
                            if (s.equals("OK")) {
                                if (pdProcesando != null)
                                    pdProcesando.cancel();

                                pdProcesando.setMessage("Iniciando Servicio");
                                pdProcesando.show();
                                getEstadoCliente.putParams("codigo", sp.getResponseLogin().cliente_id).putParams("flag", "2").send();

                            } else {
                                if (pdProcesando != null)
                                    pdProcesando.cancel();

                                new ConfirmDialog(getActivity(), "El conductor ya no esta disponible. Desea volver a intentar?", getString(R.string.app_empresa), "Reintentar", "Cancelar") {

                                    @Override
                                    public void onPositive() {

                                        //conductores.remove(con);
                                        conductores.clear();
                                        mAdapter.notifyDataSetChanged();

                                        llListaTaxista.setVisibility(View.GONE);
                                        contador = 11 * 1000;

                                        MAB.doit(getActivity(), 4);
                                        //quitar conductor de la lista
                                    }

                                    @Override
                                    public void onNegative() {
                                        //conductores.remove(con);
                                        conductores.clear();
                                        mAdapter.notifyDataSetChanged();

                                        if (conductores.size() == 0) {
                                            getActivity().onBackPressed();
                                        }

                                    }
                                }.show();


                            }
                        }
                    });
                    break;

                case 3://para consultar el estado c:


                    break;
            }

        }
    };

    public void onBack() {
        contador = 10 * 1000;
        CDT.cancel();
    }

    @Override
    public void unSelected() {

        llListaTaxista.setVisibility(View.GONE);
        if (conductores != null)
            conductores.clear();

        tvContador.setText("Solicitando...");
        contador = 10 * 1000;
        MAB.doit(getActivity(), 1);
        CDT.cancel();
    }



    @Override
    public void onThisCurrent() {
        if (((RootBody) getActivity()).flRutasFrec!=null)
            ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        MAB.doit(getActivity(), 1);
        MAB.doit(getActivity(), 0);
    }

    @Override
    public RecyclerView.ViewHolder getInstancia(View v) {
        return new TaxistaViewHolder(v);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ivReloj:

                if (conductores != null && conductores.size() > 0) {
                    llListaTaxista.setVisibility(View.VISIBLE);
                    CDT.cancel();
                } else {
                    btnReloj.setEnabled(false);
                    MAB.doit(getActivity(), 0);

                }
                break;

            case R.id.tvContador:
                MAB.doit(getActivity(), 0);
                break;
        }
    }

    public class TaxistaViewHolder extends RecyclerView.ViewHolder implements IMyAdapter<TaxistaViewHolder>, View.OnClickListener {

        ImageView ivFoto;
        TextView tvNombre, tvPlaca, tvColor, tvMarcaModelo;
        TextView tvDistancia, tvTiempo, tvLabelTiempo, tvTarifa, tvLabelTarifa;
        Button btnLlamar;
        RatingBar rbCalificacion;
        ImageView ivCar0, ivCar1, ivCar2, ivCar3, ivCar4, ivCar5, ivCar6, ivCar7, ivCar8;

        public TaxistaViewHolder(View v) {
            super(v);

            ivFoto = (ImageView) v.findViewById(R.id.ivFoto);
            tvNombre = (TextView) v.findViewById(R.id.tvNombre);
            tvPlaca = (TextView) v.findViewById(R.id.tvPlaca);
            tvColor = (TextView) v.findViewById(R.id.tvColor);
            tvMarcaModelo = (TextView) v.findViewById(R.id.tvMarcaModelo);

            btnLlamar = (Button) v.findViewById(R.id.btnLlamar);
            rbCalificacion = (RatingBar) v.findViewById(R.id.rbCalificacion);

            tvDistancia = (TextView) v.findViewById(R.id.tvDistancia);
            tvTiempo = (TextView) v.findViewById(R.id.tvTiempo);
            tvLabelTiempo = (TextView) v.findViewById(R.id.tvLabelTiempo);
            tvTiempo.setVisibility(View.GONE);
            tvLabelTiempo.setVisibility(View.GONE);


            tvTarifa = (TextView) v.findViewById(R.id.tvTarifa);
            tvLabelTarifa = (TextView) v.findViewById(R.id.tvLabelTarifa);


            ivCar0 = (ImageView) v.findViewById(R.id.ivCar0);
            ivCar1 = (ImageView) v.findViewById(R.id.ivCar1);
            ivCar2 = (ImageView) v.findViewById(R.id.ivCar2);
            ivCar3 = (ImageView) v.findViewById(R.id.ivCar3);
            ivCar4 = (ImageView) v.findViewById(R.id.ivCar4);
            ivCar5 = (ImageView) v.findViewById(R.id.ivCar5);
            ivCar6 = (ImageView) v.findViewById(R.id.ivCar6);
            ivCar7 = (ImageView) v.findViewById(R.id.ivCar7);
            ivCar8 = (ImageView) v.findViewById(R.id.ivCar8);
            v.setOnClickListener(this);

        }


        @Override
        public void bindView(TaxistaViewHolder holder, int position) {

            ((AppForeGround) getActivity().getApplication()).getPicasso().load(Urls.servidorarchivos + conductores.get(position).Foto).into(holder.ivFoto);
            holder.tvNombre.setText(conductores.get(position).NombreConductor + " " + conductores.get(position).ApelliConductor);
            holder.rbCalificacion.setRating(conductores.get(position).Calificacion);
            holder.tvPlaca.setText(conductores.get(position).Placa);
            holder.tvColor.setText(conductores.get(position).Color);
            holder.tvMarcaModelo.setText(conductores.get(position).Marca + " " + conductores.get(position).Modelo);

            if (conductores.get(position).Caracteristicas != null && conductores.get(position).Caracteristicas.length() > 0) {
                String[] cars = conductores.get(position).Caracteristicas.split(",");
                for (String s : cars) {
                    switch (s) {
                        case "1":
                            ivCar0.setImageResource(R.drawable.opc_aire_on);
                            break;
                        case "2":
                            ivCar1.setImageResource(R.drawable.opc_aeropuerto_on);
                            break;
                        case "3":
                            ivCar2.setImageResource(R.drawable.opc_bateria_on);
                            break;
                        case "4":
                            ivCar3.setImageResource(R.drawable.opc_mascota_on);
                            break;
                        case "5":
                            ivCar4.setImageResource(R.drawable.opc_pago_on);
                            break;
                        case "6":
                            ivCar5.setImageResource(R.drawable.opc_wifi_on);
                            break;
                        case "7":
                            ivCar6.setImageResource(R.drawable.opc_menor_on);
                            break;
                        case "8":
                            ivCar7.setImageResource(R.drawable.opc_mayor_on);
                            break;
                        case "9":
                            ivCar8.setImageResource(R.drawable.opc_playa_on);
                            break;
                    }
                }

            }

            float[] result = new float[4];
            Location.distanceBetween(FSOL.Servicio.OrigenLat, FSOL.Servicio.OrigenLng, conductores.get(position).Latitud, conductores.get(position).Longitud, result);
            if (result != null) {
                if (result.length > 0) {

                    if (result[0] > 1000.0f) {
                        tvDistancia.setText(String.format("%.1f", result[0] / 1000.0f) + " Km");
                    } else if (result[0] < 1000.0f) {
                        tvDistancia.setText(String.format("%.1f", result[0]) + " m");
                    }
                }
            }
        }


        @Override
        public void onClick(View v) {

            MAB.doit(getActivity(), 2, conductores.get(getLayoutPosition()));

        }
    }

    MyRequest<JsonPack.GoogleDistanceMatrixResult> MR_DISTANCE = new MyRequest<JsonPack.GoogleDistanceMatrixResult>(Urls.google_distancematrix, MyRequest.HttpRequestType.GET) {
        @Override
        public void onParseSucces(ResponseWork rw, boolean isActive, JsonPack.GoogleDistanceMatrixResult object) {

        }
    };
}
