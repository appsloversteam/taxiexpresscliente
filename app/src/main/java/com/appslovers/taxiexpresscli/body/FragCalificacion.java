package com.appslovers.taxiexpresscli.body;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.dialogs.DialogPorquecalifico;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.dialogs.DialogRutaFrecuente;
import com.appslovers.taxiexpresscli.util.MyAppBackground;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.AppBackground;
import com.appslovers.taxiexpresscli.RootSplash;

/**
 * Created by javierquiroz on 1/06/16.
 */
public class FragCalificacion extends Fragment implements View.OnClickListener {

    View ViewRoot;
    Button btnTerminar, btnAddRuta;
    RatingBar rbCalificacion;
    DialogPorquecalifico DPC;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragcalfiicacion, null);
        btnAddRuta = (Button) ViewRoot.findViewById(R.id.btnAddRuta);
        btnAddRuta.setOnClickListener(this);
        rbCalificacion = (RatingBar) ViewRoot.findViewById(R.id.rbCalificacion);
        rbCalificacion.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rating <= 2.0) {
                    if (DPC == null)
                        DPC = new DialogPorquecalifico();

                    if (!DPC.isAdded())
                        DPC.show(getFragmentManager(), "dfdf");
                }
                btnTerminar.setEnabled(true);//setBackgroundResource(R.drawable.bg_touch_rojo4r);
                btnTerminar.setTextColor(Color.WHITE);
            }
        });
        btnTerminar = (Button) ViewRoot.findViewById(R.id.btnTerminar);
        btnTerminar.setOnClickListener(this);

        return ViewRoot;

    }

    MyAppBackground MAB;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((RootBody) getActivity()).tvTitulo.setText("Calificacíon");
        ((RootBody) getActivity()).flRutasFrec.setVisibility(View.GONE);
        ((RootBody) getActivity()).flReservasAdd.setVisibility(View.GONE);
        MAB = new MyAppBackground(getActivity()) {
            @Override
            public void todo(AppBackground app, int idop, Object o) {

                switch (idop) {
                    case 0://calificar , finalziar node y reiniciar app
                        String mensaje = "";
                        if (DPC != null)
                            mensaje = DPC.MENSAJE;

                        app.CalificarCliente(rbCalificacion.getRating(), mensaje);
                        app.NodeJsTurnOff();
                        getActivity().startActivity(new Intent(getActivity(), RootSplash.class));
                        getActivity().finish();
                        break;

                    case 1:
                        btnAddRuta.setEnabled(false);
                        MR_ADDRUTA
                                .putParams("nombre", DRF.Nombre)
                                .putParams("cliente_id", app.getServicioActual().IdCliente)
                                .putParams("latitud_origen", "" + app.getServicioActual().OrigenLat)
                                .putParams("longitud_origen", "" + app.getServicioActual().OrigenLng)
                                .putParams("latitud_destino", "" + app.getServicioActual().DestinoLat)
                                .putParams("longitud_destino", "" + app.getServicioActual().DestinoLng)
                                .putParams("id_origen", "" + app.getServicioActual().OrigenId)
                                .putParams("id_destino", "" + app.getServicioActual().DestinoId)
                                .putParams("tiposervicio_id", "" + app.getServicioActual().TipoServicio)
                                .putParams("direccion_origen", "" + app.getServicioActual().OrigenDireccion)
                                .putParams("direccion_destino", "" + app.getServicioActual().DestinoDireccion)
                                .putParams("referencia_origen", "" + app.getServicioActual().OrigenReferencia)
                                .putParams("referencia_destino", "" + app.getServicioActual().DestinoReferencia)
                                .putParams("nombre_origen", "" + app.getServicioActual().OrigenNombre)
                                .putParams("nombre_destino", "" + app.getServicioActual().DestinoNombre)
                                .send();
                        break;
                }
            }
        };
    }


    MyRequest MR_ADDRUTA = new MyRequest(Urls.ws_agregar_ruta_frecuente, MyRequest.HttpRequestType.POST) {
        @Override
        public void onSucces(ResponseWork rw, boolean isActive) {

            if (rw.ResponseBody != null) {
                if (rw.ResponseBody.contains("true")) {
                    //Toast.makeText(getContext(),"RUTA GUARDAD CON ÉXITO",Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    DialogRutaFrecuente DRF;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTerminar:
                MAB.doit(getActivity(), 0);
                break;

            case R.id.btnAddRuta:
                if (DRF == null) {
                    DRF = new DialogRutaFrecuente();
                    DRF.setTargetFragment(FragCalificacion.this, 123);
                }
                DRF.show(getFragmentManager(), "addrut");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 123:
                MAB.doit(getActivity(), 1);
                break;
        }
    }
}
