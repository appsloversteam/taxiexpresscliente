package com.appslovers.taxiexpresscli.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.appslovers.taxiexpresscli.AppForeGround;
import com.appslovers.taxiexpresscli.data.JsonPack;
import com.appslovers.taxiexpresscli.data.SPUser;
import com.appslovers.taxiexpresscli.util.MyRequest;
import com.appslovers.taxiexpresscli.data.Urls;
import com.appslovers.taxiexpresscli.R;
import com.appslovers.taxiexpresscli.RootSplash;
import com.appslovers.taxiexpresscli.util.MyFragment;

import java.util.ArrayList;

/**
 * Created by javierquiroz on 23/05/16.
 */
public class FragStartSession extends MyFragment implements View.OnClickListener {

    AutoCompleteTextView etUser;
    EditText etPassword;
    JsonPack.MisSesiones MSesiones;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewRoot = inflater.inflate(R.layout.fragstartsession, null);
        ViewRoot.findViewById(R.id.tvOlvidoContrasena).setOnClickListener(this);
        ViewRoot.findViewById(R.id.btnIngresar).setOnClickListener(this);
        etUser = (AutoCompleteTextView) ViewRoot.findViewById(R.id.etUser);
        etPassword = (EditText) ViewRoot.findViewById(R.id.etPassword);

        MSesiones = new SPUser(getActivity()).getUsuarios();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, MSesiones.getUsurarios());
        etUser.setAdapter(adapter);

        return ViewRoot;
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.tvOlvidoContrasena:

                getActivity()
                        .getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_container_login, new FragForgetPassword(), FragForgetPassword.class.getSimpleName())
                        .addToBackStack("b")
                        .commit();

                break;

            case R.id.btnIngresar:
                new MyRequest<JsonPack.ResponseLogin>(this, Urls.ws_login, MyRequest.HttpRequestType.POST) {

                    @Override
                    public void onParseSuccesForeground(ResponseWork rw, JsonPack.ResponseLogin object) {
                        if (object != null) {
                            if (object.status) {
                                SPUser sp = new SPUser(getActivity());
                                sp.setResponseLogin(object);
                                if (MSesiones.usuarios != null) {
                                    if (!MSesiones.usuarios.contains(etUser.getText().toString())) {
                                        MSesiones.usuarios.add(etUser.getText().toString());
                                    }
                                } else {
                                    MSesiones.usuarios = new ArrayList<String>();
                                    MSesiones.usuarios.add(etUser.getText().toString());
                                }

                                /*SETEANDO API KEYS*/
                                Urls.FOURSQUARE_CLIENT_ID = object.foursquare_key;
                                Urls.FOURSQUARE_CLIENT_SECRET = object.foursquare_secret;
                                Urls.GOOGLE_KEY = object.google_android;
                                Urls.HERE_APP_ID = object.here_key;
                                Urls.HERE_APP_CODE = object.here_secret;

                                sp.setUsuarios(MSesiones);

                                ((AppForeGround) getActivity().getApplication()).actualziarToken();

                                startActivity(new Intent(getActivity(), RootSplash.class));
                                getActivity().finish();
                            } else {
                                Toast.makeText(getActivity(), object.mensaje, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }
                        .putParams("correo", etUser.getText().toString())
                        .putParams("password", etPassword.getText().toString())
                        .putParams("tipo","1")
                        .putParams("token","0")
                        .send();
                break;
        }

    }
}
